package com.rzt.cft.admin.aspect;

import com.google.gson.Gson;
import com.rzt.cft.constant.Constants;
import com.rzt.cft.entity.sys.SysOperationLog;
import com.rzt.cft.entity.sys.SysUser;
import com.rzt.cft.service.sys.ISysOperationLogService;
import com.rzt.cft.utils.HttpContextUtils;
import com.rzt.cft.utils.IPUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 系统日志，切面处理类
 *
 * @author
 */
@Aspect
@Component
@Slf4j
public class SysLogAspect {

	@Autowired
	private ISysOperationLogService operationLogService;

	@Pointcut("@annotation(com.rzt.cft.admin.aspect.SysLog)")
	public void logPointCut() {

	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		//执行方法
		Object result = point.proceed();
		//执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;

		//保存日志
		saveSysLog(point, time);

		return result;
	}

	private void saveSysLog(ProceedingJoinPoint joinPoint, long time) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		SysOperationLog sysLog = new SysOperationLog();
		SysLog syslog = method.getAnnotation(SysLog.class);
		if(syslog != null){
			//注解上的描述
			sysLog.setMethodDescribe(syslog.value());
		}

		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		sysLog.setMethodType(className + "." + methodName + "()");

		//请求的参数
		Object[] args = joinPoint.getArgs();
		try{
			String params = new Gson().toJson(args[0]);
			if (params.length() > 900) {
				params.substring(0, 900);
			}
			sysLog.setRequestParam(params);
		}catch (Exception e){

		}

		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		HttpServletResponse response = HttpContextUtils.getHttpServletResponse();

		//设置IP地址
		sysLog.setIp(IPUtils.getIpAddr(request));
		sysLog.setUrl(request.getRequestURI());
		sysLog.setResponCode(String.valueOf(response.getStatus()));
		Object res = request.getAttribute(Constants.BACKEND_RESPONSE);
		if (Objects.nonNull(res)) {
			sysLog.setResponParam(request.getAttribute(Constants.BACKEND_RESPONSE).toString());
		}

		//用户名
		SysUser user = (SysUser) request.getAttribute(Constants.BACKEND_CURRENT_USER);
		if (Objects.nonNull(user)) {
			sysLog.setCreateName(user.getName());
//			sysLog.setCreateId(user.getId());
		}

		sysLog.setUseTime((int)time);
		sysLog.setCreateAt(LocalDateTime.now());
		//保存系统日志
		operationLogService.save(sysLog);
	}

}
