package com.rzt.cft.admin.controller.cms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.admin.aspect.SysLog;
import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.cms.CmsAffiliationTypeDto;
import com.rzt.cft.dto.cms.CmsTypeItemDto;
import com.rzt.cft.dto.sys.SysAgreementDto;
import com.rzt.cft.dto.sys.SysConfigDto;
import com.rzt.cft.entity.cms.CmsAffiliationType;
import com.rzt.cft.entity.cms.CmsTypeItem;
import com.rzt.cft.entity.sys.SysAgreement;
import com.rzt.cft.entity.sys.SysConfig;
import com.rzt.cft.param.IdParam;
import com.rzt.cft.param.sys.*;
import com.rzt.cft.service.cms.ICmsAffiliationTypeService;
import com.rzt.cft.service.cms.ICmsTypeItemService;
import com.rzt.cft.service.sys.ISysAgreementService;
import com.rzt.cft.service.sys.ISysConfigService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ParseWrapper;
import com.rzt.cft.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 基础-字典类型项 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@RestController
@RequestMapping("/v1/cms-type/")
@Api(tags = "类型字典相关接口")
public class CmsTypeItemController extends BaseController {

    @Autowired
    private ICmsAffiliationTypeService cmsAffiliationTypeService;

    @Autowired
    private ICmsTypeItemService cmsTypeItemService;

    @Autowired
    private ISysConfigService sysConfigService;

    @Autowired
    private ISysAgreementService sysAgreementService;


    @ApiOperation("归属类型列表(分页)")
    @PostMapping("affiliationTypeList")
    public ResponseData<IPage<CmsAffiliationTypeDto>> affiliationTypeList(@RequestBody AffiliationTypeParam para) {
        Page page = getPage(para);
        QueryWrapper<CmsAffiliationType> queryWrapper = ParseWrapper.parseWrapper(para);
        queryWrapper.orderByDesc("create_at");
        IPage<CmsAffiliationType> cmsAffiliationTypeIPage = cmsAffiliationTypeService.page(page, queryWrapper);
        return success(cmsAffiliationTypeIPage.convert(x -> CustomUtil.maping(x, CmsAffiliationTypeDto.class)));
    }

    @ApiOperation("归属类型新增")
    @PostMapping("affiliationTypeAdd")
    @SysLog("归属类型新增")
    public ResponseData<Boolean> affiliationTypeAdd(@RequestBody CmsAffiliationTypeDto model) {
        // 简化序列号必填
        if (StringUtils.isBlank(model.getSimplify())) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        // 简化序列号不可重复
        CmsAffiliationType oldModel = cmsAffiliationTypeService.findOneByKv("simplify", model.getSimplify());
        if (!Objects.isNull(oldModel)) {
            return fail(ServiceStatus.DUPLICATE_SERIAL_NUMBER);
        }
        CmsAffiliationType cmsAffiliationType = CustomUtil.maping(model, CmsAffiliationType.class);
        boolean result = cmsAffiliationTypeService.save(cmsAffiliationType);
        return success(result);
    }

    @ApiOperation("归属类型修改")
    @PostMapping("affiliationTypeUpdate")
    @SysLog("归属类型修改")
    public ResponseData<Boolean> affiliationTypeUpdate(@RequestBody CmsAffiliationTypeDto model) {
        CmsAffiliationType cmsAffiliationType = cmsAffiliationTypeService.getById(model.getId());
        if (Objects.isNull(cmsAffiliationType)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        boolean result = cmsAffiliationTypeService.updateById(CustomUtil.maping(model, CmsAffiliationType.class));
        return success(result);
    }

    @ApiOperation("归属类型删除")
    @PostMapping("affiliationTypeDelete")
    @SysLog("归属类型删除")
    public ResponseData<Boolean> affiliationTypeDelete(@RequestBody String... ids) {
        boolean result = cmsAffiliationTypeService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("类型列表")
    @PostMapping("typeList")
    public ResponseData<List<CmsTypeItemDto>> typeList(@RequestBody IdParam param) {
        QueryWrapper<CmsTypeItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type_id", param.getId());
        queryWrapper.orderByAsc("sort");
        List<CmsTypeItem> cmsTypeItems = cmsTypeItemService.list(queryWrapper);
        return success(CustomUtil.maping(cmsTypeItems, CmsTypeItemDto.class));
    }

    @ApiOperation("类型添加")
    @PostMapping("typeAdd")
    @SysLog("类型添加")
    public ResponseData<Boolean> typeAdd(@RequestBody CmsTypeItemParam param) {
        CmsAffiliationType cmsAffiliationType = cmsAffiliationTypeService.getById(param.getTypeId());
        if (Objects.isNull(cmsAffiliationType)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        // 简化序列号不可重复
        CmsTypeItem oldModel = cmsTypeItemService.findOneByKv("simplify", param.getSimplify());
        if (!Objects.isNull(oldModel)) {
            return fail(ServiceStatus.DUPLICATE_SERIAL_NUMBER);
        }
        CmsTypeItem cmsTypeItems = CustomUtil.maping(param, CmsTypeItem.class);
        Boolean result = cmsTypeItemService.save(cmsTypeItems);
        return success(result);
    }

    @ApiOperation("类型修改")
    @PostMapping("typeUpdate")
    @SysLog("类型修改")
    public ResponseData<Boolean> typeUpdate(@RequestBody CmsTypeItemParam param) {
        CmsTypeItem oldCmsType = cmsTypeItemService.getById(param.getId());
        if (Objects.isNull(oldCmsType)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        CmsTypeItem cmsTypeItems = CustomUtil.maping(param, CmsTypeItem.class);
        Boolean result = cmsTypeItemService.updateById(cmsTypeItems);
        return success(result);
    }

    @ApiOperation("类型删除")
    @PostMapping("typeDelete")
    @SysLog("类型删除")
    public ResponseData<Boolean> typeDelete(@RequestBody String... ids) {
        Boolean result = cmsTypeItemService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("所有类型")
    @PostMapping("typeListAll")
    public ResponseData<List<CmsTypeItemDto>> typeListAll() {
        List<CmsTypeItem> cmsTypeItems = cmsTypeItemService.list();
        List<CmsAffiliationType> cmsAffiliationTypes = cmsAffiliationTypeService.list();
        cmsTypeItems.forEach(m -> {
            CmsAffiliationType cmsAffiliationType = CustomUtil.first(cmsAffiliationTypes, n -> m.getTypeId().equals(n.getId()));
            m.setTypeSimplify(cmsAffiliationType.getSimplify());
        });
        return success(CustomUtil.maping(cmsTypeItems, CmsTypeItemDto.class));
    }

    @ApiOperation("配置中心列表")
    @PostMapping("config/list")
    public ResponseData<IPage<SysConfigDto>> configList(@RequestBody SysConfigPageParam configPageParam) {
        Page page = getPage(configPageParam);
        QueryWrapper<SysConfig> queryWrapper = ParseWrapper.parseWrapper(configPageParam);
        queryWrapper.notIn("config_key", "recharge_qr_code");
        IPage<SysConfig> cmsAffiliationTypeIPage = sysConfigService.page(page, queryWrapper);
        return success(cmsAffiliationTypeIPage.convert(x -> CustomUtil.maping(x, SysConfigDto.class)));
    }

    @ApiOperation("配置添加")
    @PostMapping("config/add")
    @SysLog("配置添加")
    public ResponseData<Boolean> configAdd(@RequestBody AddSysConfig param) {
        ResponseData paramVali = ValidatorUtils.validateEntity(param);
        if (Objects.nonNull(paramVali)) {
            return paramVali;
        }
        List<SysConfig> sysConfigs = sysConfigService.findByKv("config_key", param.getConfigKey());
        if (sysConfigs.size() > 0) {
            return fail(ServiceStatus.CONFIGURATION_KEY_ALREADY_EXISTS);
        }
        SysConfig sysConfig = CustomUtil.maping(param, SysConfig.class);
        Boolean result = sysConfigService.save(sysConfig);
        return success(result);
    }

    @ApiOperation("配置修改")
    @PostMapping("config/update")
    @SysLog("配置修改")
    public ResponseData<Boolean> configUpdate(@RequestBody AddSysConfig param) {
        SysConfig oldSysConfig = sysConfigService.getById(param.getId());
        if (Objects.isNull(oldSysConfig)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        SysConfig sysConfig = CustomUtil.maping(param, SysConfig.class);
        Boolean result = sysConfigService.updateById(sysConfig);
        return success(result);
    }

    @ApiOperation("配置删除")
    @PostMapping("config/delete")
    @SysLog("配置删除")
    public ResponseData<Boolean> configDelete(@RequestBody String... ids) {
        Boolean result = sysConfigService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("配置中心二维码查询")
    @PostMapping("config/qrCodeList")
    public ResponseData<List<SysConfigDto>> qrCodeList() {
        QueryWrapper<SysConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("config_key", "recharge_qr_code");
        List<SysConfig> sysConfigList = sysConfigService.list(queryWrapper);
        return success(CustomUtil.maping(sysConfigList, SysConfigDto.class));
    }

    @ApiOperation("配置中心二维码修改")
    @PostMapping("config/updateQrConfig")
    public ResponseData<Boolean> updateQrConfig(@RequestBody UpdateQrConfig updateQrConfig) {
        QueryWrapper<SysConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("config_key", updateQrConfig.getConfigKey());
        SysConfig sysConfig = sysConfigService.selectOne(queryWrapper);
        if (Objects.isNull(sysConfig)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        sysConfig.setConfigValue(updateQrConfig.getConfigValue());
        Boolean result = sysConfigService.updateById(sysConfig);
        return success(result);
    }

    @ApiOperation("协议配置列表")
    @PostMapping("agreement/list")
    public ResponseData<List<SysAgreementDto>> agreementList() {
        List<SysAgreement> sysAgreements = sysAgreementService.list();
        return success(CustomUtil.maping(sysAgreements, SysAgreementDto.class));
    }

    @ApiOperation("协议配置修改")
    @PostMapping("agreement/update")
    @SysLog("协议配置修改")
    public ResponseData<Boolean> agreementUpdate(@RequestBody UpdateAgreementConfig param) {
        SysAgreement old = sysAgreementService.getById(param.getId());
        if (Objects.isNull(old)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        SysAgreement sysAgreement = CustomUtil.maping(param, SysAgreement.class);
        Boolean result = sysAgreementService.updateById(sysAgreement);
        return success(result);
    }
}
