package com.rzt.cft.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.sys.SysUserDto;
import com.rzt.cft.param.PageParam;
import com.rzt.cft.utils.ShiroUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;


@Slf4j
public abstract class BaseController {

    // 用户信息相关
    public SysUserDto getUser() {
        return ShiroUtils.getSysUser();
    }

    public String getUserId() {
        return getUser().getId();
    }

    public static Subject getSubjct() {
        return SecurityUtils.getSubject();
    }

    public String getUsername() {
        return getUser().getUserName();
    }

    // 响应结果
    public <T> ResponseData<T> fail(){
        return ResponseData.error();
    }

    public <T> ResponseData<T> fail(ServiceStatus serviceStatus){
        return ResponseData.error(serviceStatus);
    }

    /**
     * 错误
     * @return
     */
    public ResponseData fail(ResponseData errResponse) {
        return errResponse;
    }

    public ResponseData<String> success(){
        return ResponseData.ok();
    }

    public <T> ResponseData<T> success(T t){
        return ResponseData.ok(t);
    }

    public <T extends PageParam> Page<T> getPage(T tClass) {
        Integer curPage = tClass.getCurPage() == null ? 1 : tClass.getCurPage();
        Integer pageSize = tClass.getPageSize() == null ? 10 : tClass.getPageSize();
        Page page = new Page<>(curPage, pageSize);
        return page;
    }

}