package com.rzt.cft.admin.controller.cms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.admin.aspect.SysLog;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.constant.StatusConstant;
import com.rzt.cft.constant.TypeConstant;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.cms.CmsBannerDto;
import com.rzt.cft.dto.cms.CmsFeedbackDto;
import com.rzt.cft.dto.cms.HomeStatisticsDto;
import com.rzt.cft.entity.cms.CmsBanner;
import com.rzt.cft.entity.cms.CmsFeedback;
import com.rzt.cft.entity.project.ProjectDetail;
import com.rzt.cft.entity.uc.UcUser;
import com.rzt.cft.param.IdParam;
import com.rzt.cft.param.PageParam;
import com.rzt.cft.param.cms.CmsBannerPageParam;
import com.rzt.cft.service.cms.ICmsBannerService;
import com.rzt.cft.service.cms.ICmsFeedbackService;
import com.rzt.cft.service.project.IProjectDetailService;
import com.rzt.cft.service.uc.IUcRechargeService;
import com.rzt.cft.service.uc.IUcUserService;
import com.rzt.cft.service.uc.IUcWithdrawService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ParseWrapper;
import com.rzt.cft.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.Objects;

/**
 * <p>
 * banner管理
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@RestController
@RequestMapping("/v1/cms-banner/")
@Api(tags = "广告管理相关接口")
public class CmsBannerController extends BaseController {

    @Autowired
    private ICmsBannerService cmsBannerService;

    @Autowired
    private ICmsFeedbackService cmsFeedbackService;

    @Autowired
    private IUcRechargeService ucRechargeService;

    @Autowired
    private IUcWithdrawService ucWithdrawService;

    @Autowired
    private IProjectDetailService projectDetailService;

    @Autowired
    private IUcUserService ucUserService;


    @ApiOperation("广告列表")
    @PostMapping("list")
    public ResponseData<IPage<CmsBannerDto>> list(@RequestBody CmsBannerPageParam para) {
        Page page = getPage(para);
        QueryWrapper<CmsBanner> queryWrapper = ParseWrapper.parseWrapper(para);
        IPage<CmsBanner> templatePage = cmsBannerService.page(page, queryWrapper);
        return success(templatePage.convert(x -> CustomUtil.maping(x, CmsBannerDto.class)));
    }

    @ApiOperation("添加广告")
    @PostMapping("add")
    @SysLog("添加广告")
    public ResponseData<Boolean> add(@RequestBody CmsBannerDto bannerDto) {
        ResponseData paramVali = ValidatorUtils.validateEntity(bannerDto);
        if (Objects.nonNull(paramVali)) {
            return paramVali;
        }
        return success(cmsBannerService.save(CustomUtil.maping(bannerDto, CmsBanner.class)));
    }

    @ApiOperation("修改广告")
    @PostMapping("update")
    @SysLog("修改广告")
    public ResponseData<Boolean> update(@RequestBody CmsBannerDto bannerDto) {
        CmsBanner cmsBanner = cmsBannerService.getById(bannerDto.getId());
        if (Objects.isNull(cmsBanner)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        CmsBanner banner = CustomUtil.maping(bannerDto, CmsBanner.class);
        Boolean result = cmsBannerService.updateById(banner);
        return success(result);
    }

    @ApiOperation("删除广告")
    @PostMapping("delete")
    @SysLog("删除广告")
    public ResponseData<Boolean> delete(@RequestBody String... ids) {
        Boolean result = cmsBannerService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("问题反馈列表")
    @PostMapping("feedback/list")
    public ResponseData<IPage<CmsFeedbackDto>> feedbackList(@RequestBody PageParam para) {
        Page page = getPage(para);
        QueryWrapper<CmsFeedback> queryWrapper = ParseWrapper.parseWrapper(para);
        IPage<CmsFeedback> templatePage = cmsFeedbackService.page(page, queryWrapper);
        return success(templatePage.convert(x -> CustomUtil.maping(x, CmsFeedbackDto.class)));
    }

    @ApiOperation("处理问题反馈")
    @PostMapping("feedback/handle")
    @SysLog("处理问题反馈")
    public ResponseData<Boolean> feedbackHandle(@RequestBody IdParam para) {
        CmsFeedback cmsFeedback = cmsFeedbackService.getById(para.getId());
        if (Objects.isNull(cmsFeedback)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        cmsFeedback.setIsHandle(true);
        Boolean result = cmsFeedbackService.updateById(cmsFeedback);
        return success(result);
    }

    @ApiOperation("首页数据查询")
    @PostMapping("home/statistics")
    public ResponseData<HomeStatisticsDto> homeStatistics() {
        HomeStatisticsDto homeStatisticsDto = new HomeStatisticsDto();
        QueryWrapper rechargeQueryWrapper = new QueryWrapper<>();
        rechargeQueryWrapper.eq("status", StatusConstant.UcRechargeStatus.WAIT_AUDITED);
        Integer waitExamineRecharge = ucRechargeService.count(rechargeQueryWrapper);
        Integer waitExamineWithdraw = ucWithdrawService.count(rechargeQueryWrapper);

        QueryWrapper<ProjectDetail> projectDetailQueryWrapper = new QueryWrapper<>();
        projectDetailQueryWrapper.eq("type", TypeConstant.projectType.OPTIONAL);
        Integer  optionalCount = projectDetailService.count(projectDetailQueryWrapper);
        Integer totalCounr = projectDetailService.count();
        Integer intelligenceCounr = totalCounr - optionalCount;

        QueryWrapper<UcUser> ucUserQueryWrapper = new QueryWrapper<>();
        LocalDate today = LocalDate.now();
        ucUserQueryWrapper.ge("create_at", LocalDateTime.of(today, LocalTime.MIN));
        ucUserQueryWrapper.le("create_at", LocalDateTime.of(today, LocalTime.MAX));
        Integer todayNew = ucUserService.count(ucUserQueryWrapper);
        Integer total = ucUserService.count();
        QueryWrapper<UcUser> yesterdayQueryWrapper = new QueryWrapper<>();
        LocalDate yesterday = today.plusDays(-1);
        yesterdayQueryWrapper.ge("create_at", LocalDateTime.of(yesterday, LocalTime.MIN));
        yesterdayQueryWrapper.le("create_at", LocalDateTime.of(yesterday, LocalTime.MAX));
        Integer yesterdayNew = ucUserService.count(yesterdayQueryWrapper);

        //本月的第一天
        LocalDate firstday = LocalDate.of(today.getYear(),today.getMonth(),1);
        //本月的最后一天
        LocalDate lastDay = today.with(TemporalAdjusters.lastDayOfMonth());
        QueryWrapper<UcUser> monthQueryWrapper = new QueryWrapper<>();
        monthQueryWrapper.ge("create_at", LocalDateTime.of(firstday, LocalTime.MIN));
        monthQueryWrapper.le("create_at", LocalDateTime.of(lastDay, LocalTime.MAX));
        Integer monthNew = ucUserService.count(monthQueryWrapper);
        homeStatisticsDto.setIntelligenceCounr(intelligenceCounr);
        homeStatisticsDto.setMonthNew(monthNew);
        homeStatisticsDto.setOptionalCount(optionalCount);
        homeStatisticsDto.setTodayNew(todayNew);
        homeStatisticsDto.setTotal(total);
        homeStatisticsDto.setWaitExamineRecharge(waitExamineRecharge);
        homeStatisticsDto.setWaitExamineWithdraw(waitExamineWithdraw);
        homeStatisticsDto.setYesterdayNew(yesterdayNew);
        return success(homeStatisticsDto);
    }

}
