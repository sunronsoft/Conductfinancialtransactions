package com.rzt.cft.admin.controller.sys;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.sys.SysMenuAllDto;
import com.rzt.cft.dto.sys.SysMenuDto;
import com.rzt.cft.dto.sys.SysMenuListDto;
import com.rzt.cft.dto.sys.SysUserDto;
import com.rzt.cft.entity.sys.SysMenu;
import com.rzt.cft.param.IdParam;
import com.rzt.cft.po.Tree;
import com.rzt.cft.service.sys.ISysMenuService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ShiroUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 系统-后台菜单 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/v1/sys-menu")
@Api(tags = "菜单功能相关接口")
public class SysMenuController extends BaseController {

    @Autowired
    private ISysMenuService sysMenuService;

    @ApiOperation("获取该用户所有菜单列表")
//    @RequiresPermissions("sys:menu:menu")
    @PostMapping("/list")
    public ResponseData<Tree<SysMenuDto>> list() {
        SysUserDto userDto = ShiroUtils.getSysUser();
        if (userDto.getIsSuper() != null && userDto.getIsSuper()) {
            return success(sysMenuService.tree());
        }
        Tree<SysMenuDto> menus = sysMenuService.treeByUser(userDto.getId());
        return success(menus);
    }

    @ApiOperation("获取该用户所有菜单列表")
//    @RequiresPermissions("sys:menu:menu")
    @PostMapping("/listAll")
    public ResponseData<List<SysMenuAllDto>> listAll() {
        List<SysMenu> sysMenuList = sysMenuService.listAll();

        return success(CustomUtil.maping(sysMenuList, SysMenuAllDto.class));
    }

    @ApiOperation("获取该用户所有菜单列表")
//    @RequiresPermissions("sys:menu:menu")
    @PostMapping("/menuList")
    public ResponseData<SysMenuListDto> menuList() {
        SysUserDto userDto = ShiroUtils.getSysUser();
        if (Objects.isNull(userDto)) {
            return fail(ServiceStatus.TOKEN_INVALID);
        }
        if (userDto != null && userDto.getIsSuper() != null && userDto.getIsSuper()) {
            List<SysMenu> sysMenuList = sysMenuService.listAll();
            return success(handleList(sysMenuList));
        }
        List<SysMenu> sysMenuList = sysMenuService.listByUser(userDto.getId());
        return success(handleList(sysMenuList));
    }

    @ApiOperation("根据父id返回子项")
//    @RequiresPermissions("sys:menu:menu")
    @PostMapping("/menuByParent")
    public ResponseData<List<SysMenuDto>> menuByParent(@RequestBody IdParam param) {
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", param.getId());
        List<SysMenu> sysMenuList = sysMenuService.list(queryWrapper);
        return success(CustomUtil.maping(sysMenuList, SysMenuDto.class));
    }

    @ApiOperation("添加菜单")
//    @RequiresPermissions("sys:menu:add")
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody SysMenuDto param) {
        SysMenu parent = sysMenuService.getById(param.getParentId());
        if (Objects.isNull(parent)) {
            param.setParentId("0");
            param.setLevel(1);
        } else {
            param.setLevel(parent.getLevel() + 1);
        }
        Boolean result = sysMenuService.save(CustomUtil.maping(param, SysMenu.class));
        return success(result);
    }

    @ApiOperation("菜单修改")
//    @RequiresPermissions("sys:menu:update")
    @PostMapping("/update")
    public ResponseData<Boolean> update(@RequestBody SysMenuDto param) {
        // 获取原菜单信息
        SysMenu oldMenu = sysMenuService.getById(param.getId());
        if (Objects.isNull(oldMenu)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }

        SysMenu parent = sysMenuService.getById(param.getParentId());
        if (Objects.isNull(parent)) {
            param.setParentId("0");
            param.setLevel(1);
        } else {
            param.setLevel(parent.getLevel() + 1);
        }
        Boolean result = sysMenuService.updateById(CustomUtil.maping(param, SysMenu.class));
        return success(result);
    }

    @ApiOperation("菜单删除（可批量）")
//    @RequiresPermissions("sys:menu:delete")
    @PostMapping("/delete")
    public ResponseData<Boolean> delete(@RequestBody @ApiParam("标识") String... ids) {
        boolean result = sysMenuService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("菜单详情")
//    @RequiresPermissions("sys:menu:detail")
    @PostMapping("/detail")
    public ResponseData<SysMenuDto> menuDetail(@RequestBody @ApiParam("菜单标识") String id) {
        SysMenu menu = sysMenuService.getById(id);
        if (menu == null) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        return success(CustomUtil.maping(menu, SysMenuDto.class));
    }

    @ApiOperation("查询菜单树形数据")
//    @RequiresPermissions("sys:menu:tree")
    @PostMapping("/tree")
    public ResponseData<Tree<SysMenuDto>> tree() {
        Tree<SysMenuDto> tree = sysMenuService.tree();
        return success(tree);
    }

    private SysMenuListDto handleList(List<SysMenu> sysMenuList) {
        SysMenuListDto sysMenuListDto = new SysMenuListDto();
        List<SysMenuDto> sysMenuDtos = new ArrayList<>();
        List<SysMenuDto> functions = new ArrayList<>();
        sysMenuList.forEach(x -> {
            if (x.getMenuType().equals("M") || x.getMenuType().equals("C")) {
                sysMenuDtos.add(CustomUtil.maping(x, SysMenuDto.class));
            }
            if (x.getMenuType().equals("F")) {
                functions.add(CustomUtil.maping(x, SysMenuDto.class));
            }
        });
        sysMenuListDto.setFunctions(functions);
        sysMenuListDto.setSysMenuDtos(sysMenuDtos);
        return sysMenuListDto;
    }

}
