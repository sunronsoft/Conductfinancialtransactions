package com.rzt.cft.admin.controller.cms;


import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.admin.aspect.SysLog;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.cms.CmsRegionDto;
import com.rzt.cft.entity.cms.CmsRegion;
import com.rzt.cft.service.cms.ICmsRegionService;
import com.rzt.cft.utils.CustomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 基础-地区 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@RestController
@RequestMapping("/v1/cms-region/")
@Api(tags = "地区字典相关接口")
public class CmsRegionController extends BaseController {

    @Autowired
    private ICmsRegionService cmsRegionService;

    @ApiOperation("地区获取地区")
    @PostMapping("list")
    public ResponseData<List<CmsRegionDto>> list(@RequestBody @ApiParam("父级id") String parentId) {
        if (StringUtils.isBlank(parentId)) {
            parentId = "0";
        }
        List<CmsRegion> cmsRegins = cmsRegionService.findByKv("parent_id", parentId);
        return success(CustomUtil.maping(cmsRegins, CmsRegionDto.class));
    }

    @ApiOperation("添加地区")
    @PostMapping("add")
    @SysLog("添加地区")
    public ResponseData<Boolean> add(@RequestBody CmsRegionDto regionDto) {
        CmsRegion parentRegion = cmsRegionService.getById(regionDto.getParentId());
        if (Objects.isNull(parentRegion)) {
            regionDto.setParentId("0");
            regionDto.setLevel(1);
        }
        if (Objects.nonNull(parentRegion)) {
            regionDto.setLevel(parentRegion.getLevel() + 1);
        }
        CmsRegion cmsRegion = CustomUtil.maping(regionDto, CmsRegion.class);
        Boolean result = cmsRegionService.save(cmsRegion);
        return success(result);
    }

    @ApiOperation("修改地区")
    @PostMapping("update")
    @SysLog("修改地区")
    public ResponseData<Boolean> update(@RequestBody CmsRegionDto regionDto) {
        CmsRegion cmsRegion = cmsRegionService.getById(regionDto.getId());
        if (Objects.isNull(cmsRegion)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        CmsRegion region = CustomUtil.maping(regionDto, CmsRegion.class);
        Boolean result = cmsRegionService.updateById(region);
        return success(result);
    }

    @ApiOperation("删除地区")
    @PostMapping("delete")
    @SysLog("删除地区")
    public ResponseData<Boolean> delete(@RequestBody String... ids) {
        Boolean result = cmsRegionService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

}
