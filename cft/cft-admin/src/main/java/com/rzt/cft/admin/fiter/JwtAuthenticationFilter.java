package com.rzt.cft.admin.fiter;

import cn.hutool.json.JSONUtil;
import com.rzt.cft.admin.config.JwtToken;
import com.rzt.cft.constant.Constants;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.entity.sys.SysUser;
import com.rzt.cft.service.sys.ISysUserService;
import com.rzt.cft.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;


/**
 * <pre>
 * </pre>
 * <small> 2018年4月22日 | Aron</small>
 */
@Slf4j
public class JwtAuthenticationFilter extends AuthenticatingFilter {

    private final static String HEADER_AUTHORIZATION = "Authorization";

    private static final int TOKEN_REFRESH_INTERVAL = 300;

    @Autowired
    private ISysUserService sysUserService;

    public JwtAuthenticationFilter(String loginUrl) {
        if (StringUtils.isBlank(loginUrl)) {
            loginUrl = DEFAULT_LOGIN_URL;
        }
        this.setLoginUrl(loginUrl);
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (isLoginRequest(request, response)) {
            return true;
        }

        boolean allowed = false;
        try {
            allowed = executeLogin(request, response);
        } catch (IllegalStateException e) {
            log.error("token无效", e);
        } catch (Exception e) {
            log.error("登录失败", e);
        }
        return allowed || super.isPermissive(mappedValue);

    }

    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        String jwt = WebUtils.toHttp(request).getHeader(HEADER_AUTHORIZATION);
        if (StringUtils.isNotBlank(jwt) && !JwtUtil.isTokenExpired(jwt)) {
            return new JwtToken(jwt);
        }
        return null;
    }


    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
        HttpServletResponse resp = WebUtils.toHttp(response);
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json;charset=UTF-8");
        Writer writer = null;
        try {
            writer = resp.getWriter();
            writer.write(JSONUtil.toJsonStr(ResponseData.error(ServiceStatus.AUTHORIZATION_INVALID)));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(writer)) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    log.error("onAccessDenied ：{}", e);
                }
            }
        }
        fillCorsHeader(WebUtils.toHttp(request), WebUtils.toHttp(response));
        return false;
    }


    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response) {
        HttpServletResponse httpResponse = WebUtils.toHttp(response);
        String newToken = null;
        if (token instanceof JwtToken) {
            JwtToken jwt = (JwtToken) token;
            SysUser user = sysUserService.getById(JwtUtil.getUserId(jwt.getPrincipal()));
            boolean shouldRefresh = shouldTokenRefresh(JwtUtil.getIssuedAt(jwt.getToken()));
            if (shouldRefresh) {
                newToken = JwtUtil.sign(user.getId() + "", user.getUserName() + user.getUserPwd(), Constants.EXPIRE_TIME);
            }
        }
        if (StringUtils.isNotBlank(newToken)) {
            httpResponse.setHeader(HEADER_AUTHORIZATION, newToken);
        }

        return true;
    }

    @Override
    protected void postHandle(ServletRequest request, ServletResponse response){
        this.fillCorsHeader(WebUtils.toHttp(request), WebUtils.toHttp(response));
        request.setAttribute("jwtShiroFilter.FILTERED", true);
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request, ServletResponse response) {
        log.error("Validate token fail, token:{}, error:{}", token.toString(), e.getMessage());
        return false;
    }

    protected boolean shouldTokenRefresh(Date issueAt) {
        LocalDateTime issueTime = LocalDateTime.ofInstant(issueAt.toInstant(), ZoneId.systemDefault());
        return LocalDateTime.now().minusSeconds(TOKEN_REFRESH_INTERVAL).isAfter(issueTime);
    }

    protected void fillCorsHeader(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,HEAD");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
    }
}
