package com.rzt.cft.admin.fiter;

import com.alibaba.fastjson.JSON;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import org.apache.shiro.web.filter.authc.UserFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * @author 钟忠
 * @date 2020/1/5 18:25
 */
public class ShiroUserFilter extends UserFilter {

    @Override
    protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
        response.setContentType("application/json; charset=utf-8");
        response.getWriter().write(JSON.toJSONString(ResponseData.error(ServiceStatus.TOKEN_INVALID)));
    }
}
