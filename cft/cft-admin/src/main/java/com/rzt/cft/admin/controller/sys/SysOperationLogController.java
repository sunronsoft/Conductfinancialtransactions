package com.rzt.cft.admin.controller.sys;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.sys.SysOperationLogDto;
import com.rzt.cft.entity.sys.SysOperationLog;
import com.rzt.cft.param.sys.LogParam;
import com.rzt.cft.service.sys.ISysOperationLogService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ParseWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统-操作日志 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-07
 */
@RestController
@RequestMapping("/v1/sys-log")
@Api(tags = "系统-操作日志")
public class SysOperationLogController extends BaseController {

    @Autowired
    private ISysOperationLogService sysOperationLogService;

    @ApiOperation("系统操作日志 -> 列表(分页)")
    @PostMapping("/list")
    public ResponseData<IPage<SysOperationLogDto>> list(@RequestBody LogParam para) {
        Page<SysOperationLog> page = new Page<>(para.getCurPage(), para.getPageSize());
        QueryWrapper<SysOperationLog> queryWrapper = ParseWrapper.parseWrapper(para);
        IPage<SysOperationLog> operationLogIPage = sysOperationLogService.page(page, queryWrapper);
        return success(operationLogIPage.convert(x -> CustomUtil.maping(x, SysOperationLogDto.class)));
    }
}
