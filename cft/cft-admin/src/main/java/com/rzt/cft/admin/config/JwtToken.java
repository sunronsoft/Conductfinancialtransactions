package com.rzt.cft.admin.config;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * <pre>
 * </pre>
 *
 * <small> 2018年4月27日</small>
 */
@Data
public class JwtToken implements AuthenticationToken {

    private static final long serialVersionUID = 5320481558604267958L;
    /**
     * 密钥
     */
    private String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public String getPrincipal() {
        if (StringUtils.isBlank(token)) {
            return "";
        }
        return token;
    }

    @Override
    public String getCredentials() {
        return token;
    }
}