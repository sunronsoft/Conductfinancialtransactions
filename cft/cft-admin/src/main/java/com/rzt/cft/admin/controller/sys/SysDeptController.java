package com.rzt.cft.admin.controller.sys;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.sys.SysDeptDto;
import com.rzt.cft.dto.sys.SysJobDto;
import com.rzt.cft.dto.sys.SysRoleDto;
import com.rzt.cft.entity.sys.SysDept;
import com.rzt.cft.entity.sys.SysJob;
import com.rzt.cft.entity.sys.SysJobRole;
import com.rzt.cft.entity.sys.SysRole;
import com.rzt.cft.param.IdParam;
import com.rzt.cft.param.sys.AllotJobRoleParam;
import com.rzt.cft.po.Tree;
import com.rzt.cft.service.sys.ISysDeptService;
import com.rzt.cft.service.sys.ISysJobService;
import com.rzt.cft.service.sys.ISysRoleService;
import com.rzt.cft.utils.CustomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 系统-后台机构 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/v1/sys-dept")
@Api(tags = "部门岗位相关接口")
public class SysDeptController extends BaseController {

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private ISysJobService sysJobService;

    @Autowired
    private ISysRoleService sysRoleService;

    @ApiOperation("获取所有部门列表")
//    @RequiresPermissions("sys:dept:tree")
    @PostMapping("/tree")
    public ResponseData<Tree<SysDeptDto>> tree() {
        Tree<SysDeptDto> tree = sysDeptService.tree();
        return success(tree);
    }

    @ApiOperation("部门详情")
//    @RequiresPermissions("sys:dept:detail")
    @PostMapping("/detail")
    public ResponseData<SysDeptDto> detail(@RequestBody @ApiParam("部门标识") String id) {
        SysDept dept = sysDeptService.getById(id);
        if (Objects.isNull(dept)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        return success(CustomUtil.maping(dept, SysDeptDto.class));
    }

    @ApiOperation("部门添加")
//    @RequiresPermissions("sys:dept:add")
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody SysDeptDto param) {
        SysDept parent = sysDeptService.getById(param.getParentId());
        if (Objects.isNull(parent)) {
            param.setParentId(0);
        }

        SysDept sysDept = CustomUtil.maping(param, SysDept.class);
        boolean result = sysDeptService.save(sysDept);
        return success(result);
    }

    @ApiOperation("部门修改")
//    @RequiresPermissions("sys:dept:update")
    @PostMapping("/update")
    public ResponseData<Boolean> update(@RequestBody SysDeptDto param) {
        SysDept oldSysDept = sysDeptService.getById(param.getId());
        if (Objects.isNull(oldSysDept)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }

        SysDept parent = sysDeptService.getById(param.getParentId());
        if (Objects.isNull(parent)) {
            param.setParentId(0);
        }

        SysDept sysDept = CustomUtil.maping(param, SysDept.class);
        boolean result = sysDeptService.updateById(sysDept);
        return success(result);
    }

    @ApiOperation("部门删除")
//    @RequiresPermissions("sys:dept:delete")
    @PostMapping("/delete")
    public ResponseData<Boolean> delete(@RequestBody String... ids) {
        Boolean result = sysDeptService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("岗位列表")
//    @RequiresPermissions("sys:job:list")
    @PostMapping("/job/list")
    public ResponseData<List<SysJobDto>> jobList(@RequestBody @ApiParam("部门标识") String deptId) {
        QueryWrapper<SysJob> queryWrapper = new QueryWrapper();
        queryWrapper.eq("organization_id", deptId);
        queryWrapper.orderByAsc("sort");
        List<SysJob> list = sysJobService.list(queryWrapper);
        return success(CustomUtil.maping(list, SysJobDto.class));
    }

    @ApiOperation("岗位详情")
//    @RequiresPermissions("sys:job:detail")
    @PostMapping("/job/detail")
    public ResponseData<SysJobDto> jobDetail(@RequestBody IdParam param) {
        SysJob job = sysJobService.getById(param.getId());
        if (job == null)
            return fail(ServiceStatus.PARAM_EXCEPTION);
        SysJobDto jobDto = CustomUtil.maping(job, SysJobDto.class);
        return success(jobDto);
    }

    @ApiOperation("岗位添加")
//    @RequiresPermissions("sys:job:add")
    @PostMapping("/job/add")
    public ResponseData<Boolean> jobAdd(@RequestBody SysJobDto job) {
        boolean result = sysJobService.save(CustomUtil.maping(job, SysJob.class));
        return success(result);
    }

    @ApiOperation("岗位修改")
    @RequiresPermissions("sys:job:update")
    @PostMapping("/job/update")
    public ResponseData<Boolean> jobUpdate(@RequestBody SysJobDto job) {
        SysJob oldJob = sysJobService.getById(job.getId());
        if (oldJob == null)
            return fail(ServiceStatus.PARAM_EXCEPTION);
        boolean result = sysJobService.updateById(CustomUtil.maping(job, SysJob.class));
        return success(result);
    }

    @ApiOperation("岗位删除")
//    @RequiresPermissions("sys:job:delete")
    @PostMapping("/job/delete")
    public ResponseData<Boolean> jobDel(@RequestBody String... ids) {
        boolean result = sysJobService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("岗位角色列表")
//    @RequiresPermissions("sys:job:jobRoleList")
    @PostMapping("/job/jobRoleList")
    public ResponseData<List<SysRoleDto>> jobRoleList(@RequestBody IdParam param) {
        List<SysRoleDto> jobs = sysJobService.jobRoleList(param.getId());
        return success(jobs);
    }

    @ApiOperation("岗位分配角色")
//    @RequiresPermissions("sys:job:jobRoleAllot")
    @PostMapping("/job/jobRoleAllot")
    public ResponseData<Boolean> jobRoleAllot(@RequestBody AllotJobRoleParam para) {
        SysJob job = sysJobService.getById(para.getJobId());
        if (Objects.isNull(job)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }

        if (para.getRoleList().size() == 0) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        // 角色列表获取
        List<SysRole> roleList = sysRoleService.listByIds(para.getRoleList());
        if (para.getRoleList().size() != roleList.size()) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        List<SysJobRole> list = new ArrayList<>();
        para.getRoleList().forEach(x -> {
            SysJobRole jobRole = new SysJobRole();
            jobRole.setJobId(job.getId());
            jobRole.setRoleId(x);
            list.add(jobRole);
        });
        Boolean result = sysJobService.allotJobRole(job.getId(), list);
        return success(result);
    }

}
