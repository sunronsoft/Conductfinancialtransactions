package com.rzt.cft.admin.controller.sys;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.sys.AllotRoleMenu;
import com.rzt.cft.dto.sys.SysMenuDto;
import com.rzt.cft.dto.sys.SysRoleDto;
import com.rzt.cft.entity.sys.SysMenu;
import com.rzt.cft.entity.sys.SysRole;
import com.rzt.cft.param.PageParam;
import com.rzt.cft.po.Tree;
import com.rzt.cft.service.sys.ISysMenuService;
import com.rzt.cft.service.sys.ISysRoleMenuService;
import com.rzt.cft.service.sys.ISysRoleService;
import com.rzt.cft.utils.CustomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 系统-后台角色 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/v1/sys-role")
@Api(tags = "后台角色相关接口")
public class SysRoleController extends BaseController {

    @Autowired
    private ISysRoleService sysRoleService;

    @Autowired
    private ISysMenuService sysMenuService;

    @Autowired
    private ISysRoleMenuService sysRoleMenuService;

    @ApiOperation("角色列表(分页)")
//    @RequiresPermissions("sys:role:pageList")
    @PostMapping("/pageList")
    public ResponseData<IPage<SysRoleDto>> pageList(@RequestBody PageParam para) {
        Page page = getPage(para);
        IPage<SysRoleDto> pageResult = sysRoleService.page(page);
        return success(pageResult);
    }

    @ApiOperation("角色列表(all)")
//    @RequiresPermissions("sys:role:list")
    @PostMapping("/list")
    public ResponseData<List<SysRoleDto>> list() {
        List<SysRoleDto> list = CustomUtil.maping(sysRoleService.list(), SysRoleDto.class);
        return success(list);
    }

    @ApiOperation("角色详情")
//    @RequiresPermissions("sys:role:detail")
    @PostMapping("/detail")
    public ResponseData<SysRoleDto> detail(@RequestBody Integer id) {
        SysRoleDto role = CustomUtil.maping(sysRoleService.getById(id), SysRoleDto.class);
        if (role == null) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        return success(role);
    }

    @ApiOperation("角色添加")
//    @RequiresPermissions("sys:role:add")
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody SysRoleDto role) {
        boolean result = sysRoleService.save(CustomUtil.maping(role, SysRole.class));
        return success(result);
    }

    @ApiOperation("角色修改")
//    @RequiresPermissions("sys:role:update")
    @PostMapping("/update")
    public ResponseData<Boolean> update(@RequestBody SysRoleDto role) {
        SysRole oldRole = sysRoleService.getById(role.getId());
        if (Objects.isNull(oldRole)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }

        SysRole newRole = CustomUtil.maping(role, SysRole.class);
        boolean result = sysRoleService.updateById(newRole);
        return success(result);
    }

    @ApiOperation("角色删除")
//    @RequiresPermissions("sys:role:delete")
    @PostMapping("/delete")
    public ResponseData<Boolean> delete(@RequestBody String... ids) {
        boolean result = sysRoleService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("查询菜单树形数据根据权限标识")
//    @RequiresPermissions("sys:role:treeByRole")
    @PostMapping("/treeByRole")
    public ResponseData<Tree<SysMenuDto>> treeByRole(@RequestBody @ApiParam("角色标识") String roleId) {
        Tree<SysMenuDto> tree = sysMenuService.treeByRole(roleId);
        return success(tree);
    }

    @ApiOperation("角色菜单功能权限分配")
//    @RequiresPermissions("sys:role:roleMenuAllot")
    @PostMapping("/roleMenuAllot")
    public ResponseData<Boolean> roleMenuAllot(@RequestBody AllotRoleMenu para) {
        if (para.getMenuIdList().size() == 0) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        // 重置对象
        List<String> fidList = para.getMenuIdList();
        List<SysMenu> menuListList = sysMenuService.listByIds(fidList);
        if (fidList.size() != menuListList.size()) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }

        Boolean result = sysRoleMenuService.allotRoleMenu(para.getRoleId(), menuListList);
        return success(result);
    }
}
