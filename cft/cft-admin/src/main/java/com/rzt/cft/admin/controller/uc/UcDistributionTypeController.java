package com.rzt.cft.admin.controller.uc;

import com.rzt.cft.admin.controller.BaseController;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.uc.UcDistributionTypeDto;
import com.rzt.cft.entity.uc.UcDistributionType;
import com.rzt.cft.param.IdParam;
import com.rzt.cft.param.uc.AddUcDistributionType;
import com.rzt.cft.service.uc.IUcDistributionTypeService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author 钟忠
 * @date 2020/2/5 11:51
 */
@RestController
@RequestMapping("/v1/uc-distribution")
@Api(tags = "会员等级设置相关接口")
public class UcDistributionTypeController extends BaseController {

    @Autowired
    private IUcDistributionTypeService ucDistributionTypeService;


    @ApiOperation("会员等级设置列表")
    @PostMapping("/list")
    public ResponseData<List<UcDistributionTypeDto>> list() {
        List<UcDistributionType> ucDistributionTypes = ucDistributionTypeService.list();
        return success(CustomUtil.maping(ucDistributionTypes, UcDistributionTypeDto.class));
    }

    @ApiOperation("会员等级删除")
    @PostMapping("/delete")
    public ResponseData<Boolean> delete(@RequestBody String... ids) {
         Boolean result = ucDistributionTypeService.removeByIds(Arrays.asList(ids));
        return success(result);
    }

    @ApiOperation("会员等级新增")
    @PostMapping("/add")
    public ResponseData<Boolean> add(@RequestBody AddUcDistributionType addUcDistributionType) {
        ResponseData paramVali = ValidatorUtils.validateEntity(addUcDistributionType);
        if (Objects.nonNull(paramVali)) {
            return paramVali;
        }
        UcDistributionType ucDistributionType = CustomUtil.maping(addUcDistributionType, UcDistributionType.class);
        if (addUcDistributionType.getIsDefault()) {
            List<UcDistributionType> ucDistributionTypes = ucDistributionTypeService.list();
            ucDistributionTypes.forEach(x -> {
                x.setIsDefault(false);
                ucDistributionTypeService.updateById(x);
            });
        }
        Boolean result = ucDistributionTypeService.save(ucDistributionType);
        return success(result);
    }

    @ApiOperation("会员等级修改")
    @PostMapping("/update")
    public ResponseData<Boolean> update(@RequestBody AddUcDistributionType addUcDistributionType) {
        UcDistributionType oldUcDistributionType = ucDistributionTypeService.getById(addUcDistributionType.getId());
        if (Objects.isNull(oldUcDistributionType)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        UcDistributionType ucDistributionType = CustomUtil.maping(addUcDistributionType, UcDistributionType.class);
        if (addUcDistributionType.getIsDefault()) {
            List<UcDistributionType> ucDistributionTypes = ucDistributionTypeService.list();
            ucDistributionTypes.forEach(x -> {
                x.setIsDefault(false);
                ucDistributionTypeService.updateById(x);
            });
        }
        Boolean result = ucDistributionTypeService.updateById(ucDistributionType);
        return success(result);
    }

    @ApiOperation("会员等级根据id查询")
    @PostMapping("/detail")
    public ResponseData<UcDistributionTypeDto> list(@RequestBody IdParam param) {
        UcDistributionType ucDistributionTypes = ucDistributionTypeService.getById(param.getId());
        if (Objects.isNull(ucDistributionTypes)) {
            return fail(ServiceStatus.PARAM_EXCEPTION);
        }
        return success(CustomUtil.maping(ucDistributionTypes, UcDistributionTypeDto.class));
    }

}
