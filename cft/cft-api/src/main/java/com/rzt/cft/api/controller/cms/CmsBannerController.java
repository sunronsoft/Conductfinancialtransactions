package com.rzt.cft.api.controller.cms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.api.controller.BaseController;
import com.rzt.cft.api.fiter.CustomAuthorize;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.cms.CmsBannerDto;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.cms.CmsBanner;
import com.rzt.cft.entity.cms.CmsFeedback;
import com.rzt.cft.param.cms.CmsBannerPageParam;
import com.rzt.cft.param.cms.CmsFeedbackParam;
import com.rzt.cft.service.cms.ICmsBannerService;
import com.rzt.cft.service.cms.ICmsFeedbackService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ParseWrapper;
import com.rzt.cft.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * <p>
 * banner管理
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@RestController
@RequestMapping("/v1/api/cms-banner/")
@Api(tags = "广告管理相关接口")
public class CmsBannerController extends BaseController {

    @Autowired
    private ICmsBannerService cmsBannerService;


    @Autowired
    private ICmsFeedbackService cmsFeedbackService;


    @ApiOperation("广告列表")
    @PostMapping("list")
    @CustomAuthorize(skip = true)
    public ResponseData<IPage<CmsBannerDto>> list(@RequestBody CmsBannerPageParam para) {
        Page page = getPage(para);
        QueryWrapper<CmsBanner> queryWrapper = ParseWrapper.parseWrapper(para);
        IPage<CmsBanner> templatePage = cmsBannerService.page(page, queryWrapper);
        return success(templatePage.convert(x -> CustomUtil.maping(x, CmsBannerDto.class)));
    }

    @ApiOperation("问题反馈")
    @PostMapping("feedback")
    public ResponseData<Boolean> feedback(@RequestBody CmsFeedbackParam para) {
        ResponseData paramVali = ValidatorUtils.validateEntity(para);
        if (Objects.nonNull(paramVali)) {
            return paramVali;
        }
        UcUserDto ucUserDto = getUser();
        CmsFeedback cmsFeedback = new CmsFeedback();
        cmsFeedback.setIsHandle(false);
        cmsFeedback.setContent(para.getContent());
        cmsFeedback.setUserId(ucUserDto.getId());
        cmsFeedback.setUserName(ucUserDto.getNickName());
        Boolean result = cmsFeedbackService.save(cmsFeedback);
        return success(result);
    }

}
