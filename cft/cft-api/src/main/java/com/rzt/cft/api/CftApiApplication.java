package com.rzt.cft.api;

import com.rzt.cft.api.config.SecurityInterceptorConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages="com.rzt.cft")
@Import({SecurityInterceptorConfig.class})
public class CftApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(CftApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CftApiApplication.class);
    }

}
