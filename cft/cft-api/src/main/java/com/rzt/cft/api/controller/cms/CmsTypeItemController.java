package com.rzt.cft.api.controller.cms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.api.controller.BaseController;
import com.rzt.cft.api.fiter.CustomAuthorize;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.cms.CmsAffiliationTypeDto;
import com.rzt.cft.dto.cms.CmsTypeItemDto;
import com.rzt.cft.dto.sys.SysAgreementDto;
import com.rzt.cft.dto.sys.SysConfigDto;
import com.rzt.cft.entity.cms.CmsAffiliationType;
import com.rzt.cft.entity.cms.CmsTypeItem;
import com.rzt.cft.entity.sys.SysAgreement;
import com.rzt.cft.entity.sys.SysConfig;
import com.rzt.cft.param.sys.AffiliationTypeParam;
import com.rzt.cft.param.sys.AppSysConfigParam;
import com.rzt.cft.service.cms.ICmsAffiliationTypeService;
import com.rzt.cft.service.cms.ICmsTypeItemService;
import com.rzt.cft.service.sys.ISysAgreementService;
import com.rzt.cft.service.sys.ISysConfigService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ParseWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 基础-字典类型项 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@RestController
@RequestMapping("/v1/api/cms-type/")
@Api(tags = "类型字典相关接口")
public class CmsTypeItemController extends BaseController {

    @Autowired
    private ICmsAffiliationTypeService cmsAffiliationTypeService;

    @Autowired
    private ICmsTypeItemService cmsTypeItemService;

    @Autowired
    private ISysConfigService sysConfigService;

    @Autowired
    private ISysAgreementService sysAgreementService;


    @ApiOperation("归属类型列表(分页)")
    @PostMapping("affiliationTypeList")
    @CustomAuthorize(skip = true)
    public ResponseData<IPage<CmsAffiliationTypeDto>> affiliationTypeList(@RequestBody AffiliationTypeParam para) {
        Page page = getPage(para);
        QueryWrapper<CmsAffiliationType> queryWrapper = ParseWrapper.parseWrapper(para);
        IPage<CmsAffiliationType> cmsAffiliationTypeIPage = cmsAffiliationTypeService.page(page, queryWrapper);
        return success(cmsAffiliationTypeIPage.convert(x -> CustomUtil.maping(x, CmsAffiliationTypeDto.class)));
    }

    @ApiOperation("类型列表")
    @PostMapping("typeList")
    @CustomAuthorize(skip = true)
    public ResponseData<List<CmsTypeItemDto>> typeList(@RequestBody String typeId) {
        List<CmsTypeItem> cmsTypeItems = cmsTypeItemService.findByKv("type_id", typeId);
        return success(CustomUtil.maping(cmsTypeItems, CmsTypeItemDto.class));
    }

    @ApiOperation("所有类型")
    @PostMapping("typeListAll")
    @CustomAuthorize(skip = true)
    public ResponseData<List<CmsTypeItemDto>> typeListAll() {
        List<CmsTypeItem> cmsTypeItems = cmsTypeItemService.list();
        List<CmsAffiliationType> cmsAffiliationTypes = cmsAffiliationTypeService.list();
        cmsTypeItems.forEach(m -> {
            CmsAffiliationType cmsAffiliationType = CustomUtil.first(cmsAffiliationTypes, n -> m.getTypeId().equals(n.getId()));
            m.setTypeSimplify(cmsAffiliationType.getSimplify());
        });
        return success(CustomUtil.maping(cmsTypeItems, CmsTypeItemDto.class));
    }

    @ApiOperation("配置中心根据key取值")
    @PostMapping("config/byKey")
    @CustomAuthorize(skip = true)
    public ResponseData<SysConfigDto> configByKey(@RequestBody AppSysConfigParam appSysConfigParam) {
        SysConfig sysConfig = sysConfigService.findOneByKv("config_key", appSysConfigParam.getConfigKey());
        return success(CustomUtil.maping(sysConfig, SysConfigDto.class));
    }

    @ApiOperation("查询用户协议")
    @PostMapping("agreement")
    public ResponseData<SysAgreementDto> agreement(@RequestBody AppSysConfigParam appSysConfigParam) {
        QueryWrapper<SysAgreement> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("config_key", appSysConfigParam.getConfigKey());
        SysAgreement sysAgreements = sysAgreementService.getOne(queryWrapper);
        return success(CustomUtil.maping(sysAgreements, SysAgreementDto.class));
    }

    @ApiOperation("配置中心列表")
    @PostMapping("config/list")
    @CustomAuthorize(skip = true)
    public ResponseData<List<SysConfigDto>> configList() {
        List<SysConfig> sysConfig = sysConfigService.list();
        return success(CustomUtil.maping(sysConfig, SysConfigDto.class));
    }
}
