package com.rzt.cft.api.fiter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})//此注解作用于函数上
public @interface CustomAuthorize {

    /**
     * 是否跳过token校验
     *
     * @return
     */
    boolean skip() default false;

}
