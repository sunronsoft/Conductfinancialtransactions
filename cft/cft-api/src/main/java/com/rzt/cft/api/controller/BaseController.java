package com.rzt.cft.api.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.uc.UcUser;
import com.rzt.cft.param.PageParam;
import com.rzt.cft.service.uc.IUcUserService;
import com.rzt.cft.utils.CustomUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;


@Slf4j
public abstract class BaseController {

    @Autowired
    public IUcUserService ucUserService;

    @Autowired
    public HttpServletRequest servletRequest;

    public static String API_CURRENT_USER_ID = "user_id";

    public String getUserId() {
        String userId = (String)servletRequest.getAttribute(API_CURRENT_USER_ID);
        return userId;
    }

    // 用户信息相关
    public UcUserDto getUser() {
        String userId = (String)servletRequest.getAttribute(API_CURRENT_USER_ID);
        if (StringUtils.isBlank(userId)) {
            return null;
        }
        UcUser ucUser = ucUserService.getById(userId);
        if (Objects.isNull(ucUser)) {
            return null;
        }
        return CustomUtil.maping(ucUser, UcUserDto.class);
    }


    public static Subject getSubjct() {
        return SecurityUtils.getSubject();
    }

    // 响应结果
    public <T> ResponseData<T> fail(){
        return ResponseData.error();
    }

    public <T> ResponseData<T> fail(ServiceStatus serviceStatus){
        return ResponseData.error(serviceStatus);
    }

    /**
     * 错误
     * @return
     */
    public ResponseData fail(ResponseData errResponse) {
        return errResponse;
    }

    public ResponseData<String> success(){
        return ResponseData.ok();
    }

    public <T> ResponseData<T> success(T t){
        return ResponseData.ok(t);
    }

    public <T extends PageParam> Page<T> getPage(T tClass) {
        Integer curPage = tClass.getCurPage() == null ? 1 : tClass.getCurPage();
        Integer pageSize = tClass.getPageSize() == null ? 10 : tClass.getPageSize();
        Page page = new Page<>(curPage, pageSize);
        return page;
    }

}