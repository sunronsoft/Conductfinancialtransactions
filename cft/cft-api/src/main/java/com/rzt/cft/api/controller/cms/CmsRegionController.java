package com.rzt.cft.api.controller.cms;


import com.rzt.cft.api.controller.BaseController;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.cms.CmsRegionDto;
import com.rzt.cft.entity.cms.CmsRegion;
import com.rzt.cft.param.IdParam;
import com.rzt.cft.service.cms.ICmsRegionService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.TreeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 基础-地区 前端控制器
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@RestController
@RequestMapping("/v1/api/cms-region/")
@Api(tags = "地区字典相关接口")
public class CmsRegionController extends BaseController {

    @Autowired
    private ICmsRegionService cmsRegionService;


    @ApiOperation("地区获取地区")
    @PostMapping("list")
    public ResponseData<List<CmsRegionDto>> list(@RequestBody @ApiParam("父级id") String parentId) {
        if (StringUtils.isBlank(parentId)) {
            parentId = "0";
        }
        List<CmsRegion> cmsRegins = cmsRegionService.findByKv("parent_id", parentId);
        return success(CustomUtil.maping(cmsRegins, CmsRegionDto.class));
    }

    @ApiOperation("地区获取地区")
    @PostMapping("listByParentId")
    public ResponseData<List<CmsRegionDto>> listByParentId(@RequestBody IdParam param) {
        String parentId = param.getId();
        if (StringUtils.isBlank(parentId)) {
            parentId = "0";
        }
        List<CmsRegion> cmsRegins = cmsRegionService.findByKv("parent_id", parentId);
        return success(CustomUtil.maping(cmsRegins, CmsRegionDto.class));
    }

    @ApiOperation("根据id获取地区信息")
    @PostMapping("detail")
    public ResponseData<CmsRegionDto> detail(@RequestBody @ApiParam("id") String id) {
        CmsRegion cmsRegin = cmsRegionService.getById(id);
        return success(CustomUtil.maping(cmsRegin, CmsRegionDto.class));
    }

    @ApiOperation("地区所有列表")
    @PostMapping("all")
    public ResponseData<List<CmsRegionDto>> all() {
        List<CmsRegion> cmsRegins = cmsRegionService.list();
        return success(CustomUtil.maping(cmsRegins, CmsRegionDto.class));
    }

    @ApiOperation("地区所有列表")
    @PostMapping("treeAll")
    public ResponseData<List<CmsRegionDto>> treeAll() {
        List<CmsRegion> cmsRegins = cmsRegionService.list();
        List<CmsRegionDto> cmsRegionDtoList = CustomUtil.maping(cmsRegins, CmsRegionDto.class);
        cmsRegionDtoList = TreeUtil.recursionList(cmsRegionDtoList, "0");
        return success(cmsRegionDtoList);
    }

    @ApiOperation("根据id获取地区信息")
    @PostMapping("detailId")
    public ResponseData<List<CmsRegionDto>> detailId(@RequestBody @ApiParam("id") String id) {
        List<CmsRegion> cmsRegins = cmsRegionService.list();
        List<CmsRegionDto> cmsRegionDtoList = CustomUtil.maping(cmsRegins, CmsRegionDto.class);
        List<String> ids = new ArrayList<>();
        ids.add(id);
        cmsRegionDtoList = TreeUtil.reversalRecursionList(cmsRegionDtoList, ids);
        return success(cmsRegionDtoList);
    }

}
