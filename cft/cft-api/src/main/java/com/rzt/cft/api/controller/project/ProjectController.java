package com.rzt.cft.api.controller.project;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rzt.cft.api.controller.BaseController;
import com.rzt.cft.api.fiter.CustomAuthorize;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.project.ProjectDetailDto;
import com.rzt.cft.entity.project.ProjectDetail;
import com.rzt.cft.entity.project.ProjectStatistics;
import com.rzt.cft.param.uc.ProjectParam;
import com.rzt.cft.service.project.IProjectDetailService;
import com.rzt.cft.service.project.IProjectStatisticsService;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.utils.ParseWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * @author 钟忠
 * @date 2020/2/5 11:51
 */
@RestController
@RequestMapping("/v1/api/project")
@Api(tags = "项目相关接口")
public class ProjectController extends BaseController {


    @Autowired
    private IProjectDetailService projectDetailService;

    @Autowired
    private IProjectStatisticsService projectStatisticsService;

    @ApiOperation("项目列表(分页)")
    @PostMapping("/list")
    @CustomAuthorize(skip = true)
    public ResponseData<IPage<ProjectDetailDto>> list(@RequestBody ProjectParam para) {
        Page page = getPage(para);
        QueryWrapper<ProjectDetail> queryWrapper =  ParseWrapper.parseWrapper(para);
        queryWrapper.orderByDesc("create_at");
        if (para.getPurchasableGrades() != null && para.getPurchasableGrades().size() > 0) {
            para.getPurchasableGrades().forEach(x -> {
                queryWrapper.like("purchasable_grade", x);
            });
        }
        IPage<ProjectDetail> detailIPage = projectDetailService.page(page, queryWrapper);
        if (detailIPage.getRecords() == null || detailIPage.getRecords().size() == 0) {
            return success(detailIPage.convert(x -> CustomUtil.maping(x, ProjectDetailDto.class)));
        }
        List<String> ids = CustomUtil.select(detailIPage.getRecords(), ProjectDetail::getId);
        QueryWrapper<ProjectStatistics> statisticsQueryWrapper =  new QueryWrapper<>();
        statisticsQueryWrapper.eq("current", true);
        statisticsQueryWrapper.in("project_id", ids);
        List<ProjectStatistics> statistics = projectStatisticsService.list(statisticsQueryWrapper);
        IPage<ProjectDetailDto> projectDtoIPage = detailIPage.convert(x -> CustomUtil.maping(x, ProjectDetailDto.class));
        projectDtoIPage.getRecords().forEach(x -> {
            ProjectStatistics projectStatistics = CustomUtil.first(statistics, m -> m.getProjectId().equals(x.getId()));
            if (Objects.nonNull(projectStatistics)) {
                x.setVirtualProgress(projectStatistics.getVirtualProgress());
                x.setRealProgress(projectStatistics.getRealProgress());
            }
        });
        return success(projectDtoIPage);
    }

    @ApiOperation("首页精选项目")
    @PostMapping("/recommendList")
    @CustomAuthorize(skip = true)
    public ResponseData<List<ProjectDetailDto>> recommendList() {
        QueryWrapper<ProjectDetail> queryWrapper =  new QueryWrapper<>();
        queryWrapper.eq("is_recommend", true);
        List<ProjectDetail> detailList = projectDetailService.list(queryWrapper);
        if (detailList == null || detailList.size() == 0) {
            return success(CustomUtil.maping(detailList, ProjectDetailDto.class));
        }
        List<String> ids = CustomUtil.select(detailList, ProjectDetail::getId);
        QueryWrapper<ProjectStatistics> statisticsQueryWrapper =  new QueryWrapper<>();
        statisticsQueryWrapper.eq("current", true);
        statisticsQueryWrapper.in("project_id", ids);
        List<ProjectStatistics> statistics = projectStatisticsService.list(statisticsQueryWrapper);
        List<ProjectDetailDto> projectDetailDtos = CustomUtil.maping(detailList, ProjectDetailDto.class);
        projectDetailDtos.forEach(x -> {
            ProjectStatistics projectStatistics = CustomUtil.first(statistics, m -> m.getProjectId().equals(x.getId()));
            if (Objects.nonNull(projectStatistics)) {
                x.setVirtualProgress(projectStatistics.getVirtualProgress());
                x.setRealProgress(projectStatistics.getRealProgress());
            }
        });
        return success(projectDetailDtos);
    }

}
