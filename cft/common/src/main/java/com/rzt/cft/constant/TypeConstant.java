package com.rzt.cft.constant;

/**
 * @author 钟忠
 * @date 2020/2/7 14:19
 */
public interface TypeConstant {

    /**
     * 用户类型
     */
    interface UserType {
        //普通会员
        String USER_MEMBER = "uc_user_member";
    }

    /**
     * 余额变动类型
     */
    interface usableChangeType {
        //余额管理
        String BALANCE_MANAGEMENT = "balance_management";
        //充值
        String BALANCE_RECHARGE = "balance_recharge";
        //提现
        String BALANCE_WITHDRAW = "balance_withdraw";
        //提现失败回款
        String BALANCE_WITHDRAW_FAIL = "balance_withdraw_fail";
        //项目注资
        String BALANCE_CAPITAL_INJECTION = "balance_capital_injection";
        //项目返本
        String BALANCE_RETUR_CAPITAL = "balance_retur_capital";
        //项目返利
        String BALANCE_PROJECT_REBATE = "balance_project_rebate";
        //分销返利
        String BALANCE_DISTRIBUTION_REBATE = "balance_distribution_rebate";
        //项目退出
        String BALANCE_PROJECT_EXIT = "balance_project_exit";
        //转账转出
        String BALANCE_TURN_OUT = "balance_turn_out";
        //转账转入
        String BALANCE_TURN_IN = "balance_turn_in";
    }

    /**
     * 积分变动类型
     */
    interface integralChangeType {
        //项目返积分
        String INTEGRAL_PROJECT_REBATE = "integral_project_rebate";
        //转账转出
        String INTEGRAL_TURN_OUT = "integral_turn_out";
        //转账转入
        String INTEGRAL_TURN_IN = "integral_turn_in";
        //项目退出
        String INTEGRAL_PROJECT_EXIT = "integral_project_exit";
        //分销返积分
        String INTEGRAL_DISTRIBUTION_REBATE = "integral_distribution_rebate";
    }

    /**
     * 项目类型
     */
    interface projectType {
        //自选
        String OPTIONAL = "optional";
        //智能
        String INTELLIGENCE = "intelligence";
    }

    /**
     * 项目期限
     */
    interface projectTerm {
        //一年
        String ONE_YEAR = "one_year";
        //半年
        String HALF_YEAR = "half_year";
    }

    /**
     * 返利类型（自选返利，智能返利，分销返利，返回本金）
     */
    interface ucRebateType {
        //自选返利
        String OPTIONAL_REBATE = "optional_rebate";
        //智能返利
        String INTELLIGENCE_REBATE = "intelligence_rebate";
        //分销返利
        String DISTRIBUTION_REBATE = "distribution_rebate";
        //返回本金
        String RETUR_CAPITAL = "retur_capital";
    }

    /**
     * 分销级别类型
     */
    interface distributionLevelType {
        //直推
        String ONE = "one";
        //多级
        String MORE = "more";
    }

}
