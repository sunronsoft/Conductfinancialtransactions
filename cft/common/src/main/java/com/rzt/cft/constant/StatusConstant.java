package com.rzt.cft.constant;

/**
 * @author 钟忠
 * @date 2020/2/7 14:19
 */
public interface StatusConstant {
    /**
     * 用户账号状态（0,启用、1禁用、2冻结、3注销、4待注销）
     */
    interface UcUserStatus {
        //启用
        Integer ENABLE = 0;
        //1禁用
        Integer PROHIBIT = 1;
        //2冻结
        Integer FROZEN = 2;
        //3注销
        Integer CANCELLATION = 3;
        //4待注销
        Integer WAIT_CANCELLATION = 4;
    }

    /**
     * 充值、提现、退出订单状态（0 待审核，1已通过，2已拒绝）
     */
    interface UcRechargeStatus {
        //待审核
        String WAIT_AUDITED = "0";
        //已通过
        String ADOPT = "1";
        //已拒绝
        String REJECTED = "2";
    }

    /**
     * 注资订单状态（0 进行中，1已完成，2已退出）
     */
    interface ProjectOrderStatus {
        //进行中
        String IN_HAND = "0";
        //已完成
        String COMPLETED = "1";
        //已退出
        String QUIT = "2";
    }

    /**
     * 项目状态（0 进行中，1已完成，2已结束）
     */
    interface ProjectStatus {
        //进行中
        String IN_HAND = "0";
        //已完成
        String COMPLETED = "1";
        //已结束
        String QUIT = "2";
    }


}
