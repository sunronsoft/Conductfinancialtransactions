package com.rzt.cft.constant;

/**
 *  正则表达式常量
 */
public class RegexConstant {

    /**
     * 手机号正则校验
     */
    public final static String PHONE_REGEX = "^1[2-9]\\d{9}$";
}
