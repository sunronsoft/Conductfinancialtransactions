package com.rzt.cft.po;

import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/14 10:58
 */
@Data
public class SmsParam {

    //短信通道号
    private String sender;

    //模板ID
    private String templateId;

    //签名名称
    private String signature;

    //短信接收人号码 "+8615123456789,+8615234567890"
    private String receiver;

    //选填,短信状态报告
    private String statusCallBack;

    //模板变量 双变量模板示例:模板内容为"您有${1}件快递请到${2}领取"时,templateParas可填写为"[\"3\",\"人民公园正门\"]"
    private String templateParas;

}
