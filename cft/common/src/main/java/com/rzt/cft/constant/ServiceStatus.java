package com.rzt.cft.constant;


/**
 * @ClassName ResponseData
 * @Description: 响应码。
 * @Author zz
 * @Date 2019/10/16 0016
 * @Version V1.0
 **/
public enum ServiceStatus {
    /**
     * 请求成功
     */
    SUCCESS(200,"操作成功"),

    /**
     * 参数异常
     */
    PARAM_EXCEPTION(400,"参数异常"),

    /**
     * 证书错误
     */
    CERTIFICATE_EXCEPTION(401,"证书错误"),

    /**
     * 无效的token
     */
    TOKEN_INVALID(403,"无效的token"),

    /**
     * 签名错误
     */
    SIGN_ERROR(401,"签名错误"),

    /**
     * 请求超时
     */
    REQUEST_TIMEOUT(500,"请求超时"),

    /**
     * 服务器异常
     */
    SERVICE_EXCEPTION(500,"未知异常，请联系管理员"),

    /**
     * BBS sensitive 敏感词命中错误
     */
    BBS_SENSITIV_EERROR(1001,"用词不当,言论请与学习相关"),

    AUTHORIZATION_SIGN_FAILED(1002,"token生成失败"),

    AUTHORIZATION_INVALID(1003,"token不合法"),

    AUTHORIZATION_EXPIRED(1004,"token已过期"),

    UNAUTHORIZED_EXCEPTION_EXPIRED(1005,"权限不足！"),


    INVALID_PARAM(10003, "非法参数！"),

    NO_USER(10004, "用户不存在！"),

    NO_LOGIN(10005, "尚未登录，请登录！"),

    UN_PW_WRONG(10006, "账户名或者密码输入错误!"),

    PASSWORD_WRONG(10007, "原密码错误!"),

    EQUAL_OLD_PW(10008, "请输入不同的密码!"),

    EQUAL_NEW_PW(10009, "密码不一致!"),

    DATE_FORMAT(10010, "日期格式转换错误，不支持格式！"),

    USER_EXC(10011, "用户名/密码不匹配！！"),

    USER_NOT(10012, "账户已被冻结！！"),

    USER_NULL(10013, "账户不存在！!"),

    USER_ERROR(10014, "额外的错误！!"),

    NOT_REG(10015, "该手机号或账号未注册！!"),

    MSG_ERROR(10016, "验证码错误或失效！!"),

    PHONE_EXIT(10017, "手机号已注册！!"),

    NOT_AUDIT(10018, "该经纪人未审核！!"),

    SH_PASS(10019, "通过审核, 请勿重复操作！!"),

    PAGE_EXC(10020, "分页参数错误!"),

    SEND_SUC(10021, "验证码发送成功！!"),

    LOGIN_SUC(10022, "登陆成功！!"),

    REG_SUC(10023, "注册成功！!"),

    FAIL(10024, "操作失败！"),

    /**
     * 验证码错误
     */
    CODE_ERROR(3001,"验证码错误"),

    /**
     * 账号或密码不存在
     */
    USER_NAMEPWDERROR (3002,"账号或密码不正确"),

    /**
     * 登录类型错误
     */
    LOGINTYPE_ERROR(3003,"登录类型错误"),

    /**
     * 手机号不存在
     */
    ACCOUNT_NOT_EXIST(3004,"手机号不存在"),

    /**
     * 未绑定手机号
     */
    BINDING_MOBILE_PHONE(3005,"未绑定手机号"),

    /**
     * 手机号已绑定其他账号
     */
    PHONE_USED(3006,"手机号已绑定其他账号"),

    /**
     * 原密码错误
     */
    OLD_PWD_ERROR (3007,"原密码错误"),

    /**
     * 非法访问，未登录
     */
    ILLEGAL_ACCESS(3000,"非法访问，未登录"),

    /**
     * 手机号验证
     */
    PHONE_VERIFY(3007,"请输入正确的手机号"),

    /**
     * 密码验证
     */
    PWD_VERIFY(3008,"请输入正确格式"),

    /**
     * 没有设置密码
     */
    PWD_NEVER_SET(3009,"没有设置密码"),

    VERIFICATION_CODE_INVALID(30014,"验证码已失效"),

    REPEAT_OF_USER_NAME(30015, "用户名重复！"),

    REPEAT_OF_USER_PHONE(30016, "手机号重复！"),

    POSITION_DOES_NOT_EXIST(30017, "岗位不存在！"),

    DUPLICATE_SERIAL_NUMBER(30018, "简化序列号重复！"),

    /**
     * 文件不能为空
     */
    THE_FILE_IS_EMPTY(30019,"文件不能为空！"),

    FILE_PATH_ERROR(30020,"文件路径错误！"),

    SIMPLIFY_IS_ERROR(30021,"简化序列号错误！"),

    REGION_ID_IS_ERROR(30022,"地区id错误！"),

    REPEAT_OF_ACCOUNT(30023, "该账号已被注册！"),

    NO_REAL_ESTATE_COMPANY_FOUND(30023, "未找到改房产公司！"),

    NO_STORE(30024, "门店不存在！"),

    DEVELOPERS_COMPANY_NOT_FOUND(30025, "开发商不存在！"),

    PROPERTIES_COMPANY_NOT_FOUND(30026, "楼盘(小区)不存在！"),

    NAME_CANNOT_BE_EMPTY(30027,"名称不能为空！"),

    APARTMENT_NOT_FOUND(30028, "户型不存在！"),

    THERE_ARE_UNAPPROVED_CERTIFICATIONS(30029, "还有未审核的认证，不能重复提交！"),

    CAN_NOT_MODIFY_NON_PERSONAL_PROPERTY(30030, "不能修改非本人房源！"),

    NON_BROKER_CANNOT_OPERATE(30031, "非经纪人不能操作！"),

    ORIGINAL_PASSWORD_ERROR(30032, "原密码错误！"),

    FAILED_OBTAIN_RONGYUN_TOKEN(30033, "获取融云token失败！"),

    PLEASE_SET_YOUR_NICKNAME_AND_AVATAR_FIRST(30033, "请先设置昵称和头像！"),

    HOUSE_NOT_FOUND(30034, "房屋不存在！"),

    NON_CURRENT_BROKER_BOOKING_SERVICE(30035, "非当前经纪人预约服务！"),

    CURRENT_HOUSING_BROKER_INFORMATION_MISMATCH(30036, "当前房屋经纪人信息不匹配！"),

    NO_NORMAL_SELLING_PRICE_FOR_THE_HOUSE(30037, "该房屋没有正常售价！"),

    CONFIGURATION_KEY_ALREADY_EXISTS(30038, "已有该配置key！"),

    NO_WALLET_FOUND(30039, "没有找到钱包！"),

    YOUR_CREDIT_RUNNING_LOW(30039, "余额不足！"),

    NO_RELATED_AUDITS_FOUND(30040, "没有找到相关审核订单！"),

    ORDER_APPROVED(30041, "订单已审核！"),

    PROJECT_DOES_NOT_EXIST(30042, "项目不存在！"),

    AMOUNT_IS_NOT_IN_THE_OPTIONAL(30043, "输入金额不在可选项中！"),

    WRONG_TERM_SELECTION(30044, "期限选择错误！"),

    SURPLUS_DEFICIENCY(30045, "剩余可投不足！"),

    ALREADY_APPLIED_TO_EXIT(30046, "已有申请退出订单！"),

    AUDIT_STATUS_ERROR(30047, "审核状态错误！"),

    WRONG_PROJECT_TYPE(30048, "项目类型错误！"),

    NO_MORE_THAN_5_SELECTED_PROJECTS(30049, "精选项目不能超过5个！"),

    PWD_ERROR (30050,"密码错误"),

    NO_USERS_FOUND_FOR_ODE (30051,"没有找到该推荐码的用户！"),

    YOU_HAVE_BOUND_THE_INVITEE (30052,"你已绑定邀请人！"),

    UNABLE_TO_TRANSFER_ACCOUNTS (30053,"不为推荐关系，不能转账！"),

    INTEGRAL_EFICIENCY(30054, "积分不足！"),

    CAN_NOT_BIND_YOURSELF_INVITING(30055, "不能绑定自己为邀请人！"),

    PRODUCTS_WITH_LESS_THAN_60_DAYS(30056, "有小于60天的产品！"),

    SERVICE_EXPIRED(30057, "服务已过期，请联系开发人员！"),

    HAS_BEEN_APPLIED_FOR_AND_CAPITAL_INJECTION(30058, "已申请退出，不能继续注资！"),

    SEVEN_DAYS_TO_REVIEW(30058, "7天后才能审核！"),


    //客户端提示
    MOBILE_NUMBER_REGISTERED(40001, "手机号已注册！"),

    MOBILE_NUMBER_NOT_REGISTERED(40002, "手机号未注册！"),

    ;
    private int code;
    private String message;

    /**
     * 构造方法
     * @param code
     * @param message
     */
    ServiceStatus(int code, String message) {
        this.message = message;
        this.code = code;
    }
    public int getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
