package com.rzt.cft.constant;

/**
 * 对应config表中的code
 */
public class ConfigType {
    /**
     * 楼盘类型
     */
    public static final String BUILD_TYPE = "T001";

    /**
     * 装修类型
     */
    public static final String DECORATE = "T002";

    /**
     * 标签
     */
    public static final String TAG = "BQ001";

}
