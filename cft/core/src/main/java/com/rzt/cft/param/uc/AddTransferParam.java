package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 转账
 */
@Data
public class AddTransferParam {

    @ApiModelProperty("转入对象用户id")
    private String inUserId;

    @ApiModelProperty("转账金额")
    private BigDecimal amt;

    @ApiModelProperty("转账积分")
    private BigDecimal integral;

    @ApiModelProperty("密码")
    private String pwd;

}
