package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 配置表
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
public class UpdateAgreementConfig {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "配置值")
    private String configValue;

    @ApiModelProperty(value = "配置名称")
    private String name;

}
