package com.rzt.cft.service.goods.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.service.goods.IGoodsCategoryService;
import com.rzt.cft.entity.goods.GoodsCategory;
import com.rzt.cft.mapper.goods.GoodsCategoryMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-类目表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class GoodsCategoryServiceImpl extends ServiceImpl<GoodsCategoryMapper, GoodsCategory> implements IGoodsCategoryService {

}
