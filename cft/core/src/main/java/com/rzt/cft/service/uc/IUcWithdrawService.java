package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.uc.UcWithdraw;
import com.rzt.cft.param.uc.AddUcWithdraw;
import com.rzt.cft.param.uc.ExamineParam;

/**
 * <p>
 * 用户-提现表单 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IUcWithdrawService extends IService<UcWithdraw> {

    ResponseData<Boolean> withdrawAdd(AddUcWithdraw addUcWithdraw, UcUserDto ucUserDto);

    Boolean withdrawExamine(ExamineParam para, UcWithdraw ucWithdraw);
}
