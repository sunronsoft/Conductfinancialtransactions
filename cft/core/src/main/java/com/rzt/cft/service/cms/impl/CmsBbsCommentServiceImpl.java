package com.rzt.cft.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.cms.CmsBbsComment;
import com.rzt.cft.mapper.cms.CmsBbsCommentMapper;
import com.rzt.cft.service.cms.ICmsBbsCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营-论坛评论 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class CmsBbsCommentServiceImpl extends ServiceImpl<CmsBbsCommentMapper, CmsBbsComment> implements ICmsBbsCommentService {

}
