package com.rzt.cft.dto.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 钟忠
 * @date 2020/1/3 11:57
 */
@Data
public class SysMenuListDto {

    @ApiModelProperty("菜单和目录list")
    private List<SysMenuDto> sysMenuDtos;

    @ApiModelProperty("功能list")
    private List<SysMenuDto> functions;

}
