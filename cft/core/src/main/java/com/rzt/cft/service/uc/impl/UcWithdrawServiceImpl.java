package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.constant.ServiceStatus;
import com.rzt.cft.constant.StatusConstant;
import com.rzt.cft.constant.TypeConstant;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.uc.UcUsable;
import com.rzt.cft.entity.uc.UcWallet;
import com.rzt.cft.entity.uc.UcWithdraw;
import com.rzt.cft.mapper.uc.UcWalletMapper;
import com.rzt.cft.mapper.uc.UcWithdrawMapper;
import com.rzt.cft.param.uc.AddUcWithdraw;
import com.rzt.cft.param.uc.ExamineParam;
import com.rzt.cft.param.uc.InsertUsableParam;
import com.rzt.cft.service.uc.IUcAssetService;
import com.rzt.cft.service.uc.IUcWithdrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 用户-提现表单 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcWithdrawServiceImpl extends ServiceImpl<UcWithdrawMapper, UcWithdraw> implements IUcWithdrawService {

    @Autowired
    private UcWalletMapper ucWalletMapper;

    @Autowired
    private IUcAssetService ucAssetService;

    @Override
    @Transactional
    public ResponseData<Boolean> withdrawAdd(AddUcWithdraw addUcWithdraw, UcUserDto ucUserDto) {
        //校验余额是否足够
        UcUsable ucUsable = ucAssetService.queryUsable(ucUserDto.getId());
        if (Objects.isNull(ucUsable) || ucUsable.getUsable().compareTo(addUcWithdraw.getAmount()) < 0) {
            return new ResponseData(ServiceStatus.YOUR_CREDIT_RUNNING_LOW);
        }
        //校验钱包地址是否存在
        UcWallet ucWallet = ucWalletMapper.selectById(addUcWithdraw.getWalletId());
        if (Objects.isNull(ucWallet)) {
            return new ResponseData(ServiceStatus.NO_WALLET_FOUND);
        }

        //插入订单信息
        UcWithdraw ucWithdraw = new UcWithdraw();
        ucWithdraw.setUserId(ucUserDto.getId());
        ucWithdraw.setUserName(ucUserDto.getNickName());
        ucWithdraw.setAmount(addUcWithdraw.getAmount());
        ucWithdraw.setWalletId(ucWallet.getId());
        ucWithdraw.setWalletUrl(ucWallet.getUrl());
        ucWithdraw.setQrCode(ucWallet.getQrCode());
        ucWithdraw.setStatus(StatusConstant.UcRechargeStatus.WAIT_AUDITED);
        ucWithdraw.setTxnSsn(CustomUtil.getSsn());

        int count = baseMapper.insert(ucWithdraw);

        //扣减余额
        InsertUsableParam insertUsableParam = new InsertUsableParam();
        insertUsableParam.setUserId(ucUserDto.getId());
        insertUsableParam.setUserName(ucUserDto.getNickName());
        insertUsableParam.setType(TypeConstant.usableChangeType.BALANCE_WITHDRAW);
        insertUsableParam.setUsable(addUcWithdraw.getAmount().negate());
        insertUsableParam.setRemarks(ucWithdraw.getId());
        ucAssetService.insertUsable(insertUsableParam);

        return new ResponseData(retBool(count));
    }

    @Override
    @Transactional
    public Boolean withdrawExamine(ExamineParam para, UcWithdraw ucWithdraw) {
        //如果是拒绝加余额
        if (para.getStatus().equals(StatusConstant.UcRechargeStatus.REJECTED)) {
            InsertUsableParam insertUsableParam = new InsertUsableParam();
            insertUsableParam.setUserId(ucWithdraw.getUserId());
            insertUsableParam.setUserName(ucWithdraw.getUserName());
            insertUsableParam.setType(TypeConstant.usableChangeType.BALANCE_WITHDRAW_FAIL);
            insertUsableParam.setUsable(ucWithdraw.getAmount());
            insertUsableParam.setRemarks(ucWithdraw.getId());
            ucAssetService.insertUsable(insertUsableParam);
        }
        //修改订单状态
        ucWithdraw.setStatus(para.getStatus());
        ucWithdraw.setRemarks(para.getRemarks());
        ucWithdraw.setAuditTime(LocalDateTime.now());
        int count = baseMapper.updateById(ucWithdraw);
        return retBool(count);
    }
}
