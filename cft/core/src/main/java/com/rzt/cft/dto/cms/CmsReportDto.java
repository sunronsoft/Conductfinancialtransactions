package com.rzt.cft.dto.cms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 运营-房屋举报
 *
 * @author zhongzhong
 * @since 2020-03-7
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsReportDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("是否启用")
    private Boolean isUse;

    @ApiModelProperty("房屋标识")
    private String houseId;

    @ApiModelProperty("举报内容")
    private String content;

    @ApiModelProperty("举报人")
    private String userId;

    @ApiModelProperty("举报人昵称")
    private String userName;

    @ApiModelProperty("是否处理")
    private Boolean isHandle;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("标题")
    private String title;

}
