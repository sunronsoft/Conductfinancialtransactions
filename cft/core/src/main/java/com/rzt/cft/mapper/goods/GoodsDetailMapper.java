package com.rzt.cft.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.goods.GoodsDetail;

/**
 * <p>
 * 商品-详情 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface GoodsDetailMapper extends BaseMapper<GoodsDetail> {

}
