package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcDistributionLog;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户-分销佣金记录表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-22
 */
@Component
public interface UcDistributionLogMapper extends BaseMapper<UcDistributionLog> {

}
