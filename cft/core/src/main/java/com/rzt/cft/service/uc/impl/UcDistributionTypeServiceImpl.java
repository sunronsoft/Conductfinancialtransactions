package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcDistributionType;
import com.rzt.cft.mapper.uc.UcDistributionTypeMapper;
import com.rzt.cft.service.uc.IUcDistributionTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-分销类型 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcDistributionTypeServiceImpl extends ServiceImpl<UcDistributionTypeMapper, UcDistributionType> implements IUcDistributionTypeService {

}
