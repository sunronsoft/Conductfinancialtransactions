package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcUserDistributionLog;
import com.rzt.cft.mapper.uc.UcUserDistributionLogMapper;
import com.rzt.cft.service.uc.IUcUserDistributionLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-分销升级记录 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcUserDistributionLogServiceImpl extends ServiceImpl<UcUserDistributionLogMapper, UcUserDistributionLog> implements IUcUserDistributionLogService {

}
