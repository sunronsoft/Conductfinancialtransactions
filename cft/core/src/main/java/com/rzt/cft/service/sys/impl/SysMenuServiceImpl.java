package com.rzt.cft.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.po.Tree;
import com.rzt.cft.utils.BuildTree;
import com.rzt.cft.dto.sys.SysMenuDto;
import com.rzt.cft.entity.sys.SysMenu;
import com.rzt.cft.mapper.sys.SysMenuMapper;
import com.rzt.cft.mapper.sys.SysRoleMenuMapper;
import com.rzt.cft.service.sys.ISysMenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 系统-后台菜单 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public Set<String> listPerms(String userId) {
        List<String> perms = baseMapper.listUserPerms(userId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms) {
            if (StringUtils.isNotBlank(perm)) {
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
    }

    @Override
    public Tree<SysMenuDto> tree() {
        List<Tree<SysMenuDto>> trees = new ArrayList<Tree<SysMenuDto>>();
        List<SysMenu> menus = baseMapper.selectList(null);
        for (SysMenu sysMenu : menus) {
            Tree<SysMenuDto> tree = new Tree<SysMenuDto>();
            if (sysMenu.getMenuType().equals("F")) {
                continue;
            }
            tree.setId(sysMenu.getId());
            tree.setParentId(sysMenu.getParentId());
            tree.setText(sysMenu.getTitle());
            tree.setType(sysMenu.getMenuType());
            tree.setIco(sysMenu.getIco());
            tree.setPerms(sysMenu.getPerms());
            tree.setUrl(sysMenu.getUrl());
            trees.add(tree);
        }
        // 默认顶级菜单为０，根据数据库实际情况调整
        Tree<SysMenuDto> ts = BuildTree.build(trees);
        return ts;
    }

    @Override
    public Tree<SysMenuDto> treeByRole(String roleId) {
        // 根据roleId查询权限
        List<SysMenu> menus = baseMapper.selectList(null);
        List<Integer> menuIds = sysRoleMenuMapper.listMenuIdByRoleId(roleId);
        List<Integer> temp = menuIds;
        for (SysMenu menu : menus) {
            if (temp.contains(menu.getParentId())) {
                menuIds.remove(menu.getParentId());
            }
        }
        List<Tree<SysMenuDto>> trees = new ArrayList<Tree<SysMenuDto>>();
        for (SysMenu sysMenu : menus) {
            Tree<SysMenuDto> tree = new Tree<SysMenuDto>();
            tree.setId(sysMenu.getId().toString());
            tree.setParentId(sysMenu.getParentId().toString());
            tree.setText(sysMenu.getTitle());
            tree.setType(sysMenu.getMenuType());
            Map<String, Object> state = new HashMap<>(16);
            String menuId = sysMenu.getId();
            if (menuIds.contains(menuId)) {
                state.put("selected", true);
            } else {
                state.put("selected", false);
            }
            tree.setState(state);
            trees.add(tree);
        }
        // 默认顶级菜单为０，根据数据库实际情况调整
        Tree<SysMenuDto> t = BuildTree.build(trees);
        return t;
    }

    @Override
    public Tree<SysMenuDto> treeByUser(String userId) {
        List<Tree<SysMenuDto>> trees = new ArrayList<Tree<SysMenuDto>>();
        List<SysMenu> menus = baseMapper.listMenuByUserId(userId);
        for (SysMenu sysMenu : menus) {
            Tree<SysMenuDto> tree = new Tree<SysMenuDto>();
            tree.setId(sysMenu.getId().toString());
            tree.setParentId(sysMenu.getParentId().toString());
            tree.setText(sysMenu.getTitle());
            tree.setType(sysMenu.getMenuType());
            Map<String, Object> attributes = new HashMap<>(16);
            attributes.put("url", sysMenu.getUrl());
            attributes.put("icon", sysMenu.getIco());
            tree.setAttributes(attributes);
            trees.add(tree);
        }
        // 默认顶级菜单为０，根据数据库实际情况调整
        Tree<SysMenuDto> list = BuildTree.build(trees);
        return list;
    }

    @Override
    public List<SysMenu> listAll() {
        return baseMapper.selectList(null);
    }

    @Override
    public List<SysMenu> listByUser(String userId) {
        return baseMapper.listMenuByUserId(userId);
    }
}
