package com.rzt.cft.dto.cms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营-问题反馈
 *
 * @author zhongzhong
 * @since 2020-03-7
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsFeedbackDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("反馈内容")
    private String content;

    @ApiModelProperty("反馈人")
    private String userId;

    @ApiModelProperty("反馈人昵称")
    private String userName;

    @ApiModelProperty("是否处理")
    private Boolean isHandle;

}
