package com.rzt.cft.dto.uc;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-积分记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class UcIntegralDto extends AbstractBaseDto {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户名字")
    private String userName;

    @ApiModelProperty("变动类型")
    private String type;

    @ApiModelProperty("变动积分")
    private BigDecimal changeIntegral;

    @ApiModelProperty("变动后积分")
    private BigDecimal integral;

    @ApiModelProperty("是否最新")
    private Boolean current;

    @ApiModelProperty("备注")
    private String remarks;

    @ApiModelProperty("项目类型")
    private String projectType;

    @ApiModelProperty("项目名称")
    private String projectName;


}
