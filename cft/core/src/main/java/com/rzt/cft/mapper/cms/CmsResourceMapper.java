package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsResource;

/**
 * <p>
 * 基础-资源信息 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface CmsResourceMapper extends BaseMapper<CmsResource> {

}
