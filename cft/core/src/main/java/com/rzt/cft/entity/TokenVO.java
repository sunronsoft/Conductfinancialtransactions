package com.rzt.cft.entity;

import lombok.Data;

/**
 * <pre>
 * </pre>
 * 
 * 2018年4月27日
 */
@Data
public class TokenVO {
    private String token;
    private Long tokenExpire;
    private String refleshToken;
    private Long refreshTokenExpire;
}
