package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.constant.StatusConstant;
import com.rzt.cft.constant.TypeConstant;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.project.ProjectOrder;
import com.rzt.cft.entity.uc.UcLogout;
import com.rzt.cft.entity.uc.UcUser;
import com.rzt.cft.mapper.project.ProjectOrderMapper;
import com.rzt.cft.mapper.uc.UcLogoutMapper;
import com.rzt.cft.mapper.uc.UcUserMapper;
import com.rzt.cft.param.uc.AddUcLogout;
import com.rzt.cft.param.uc.ExamineParam;
import com.rzt.cft.param.uc.InsertIntegralParam;
import com.rzt.cft.param.uc.InsertUsableParam;
import com.rzt.cft.service.uc.IUcAssetService;
import com.rzt.cft.service.uc.IUcLogoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户-退出审核表单 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
@Service
public class UcLogoutServiceImpl extends ServiceImpl<UcLogoutMapper, UcLogout> implements IUcLogoutService {

    @Autowired
    private ProjectOrderMapper projectOrderMapper;

    @Autowired
    private IUcAssetService ucAssetService;

    @Autowired
    private UcUserMapper ucUserMapper;

    @Override
    public Boolean logoutAdd(AddUcLogout addUcLogout, UcUserDto ucUserDto) {
        UcLogout ucLogout = new UcLogout();
        ucLogout.setProjectType(addUcLogout.getProjectType());
        ucLogout.setUserId(ucUserDto.getId());
        ucLogout.setApplyTime(LocalDateTime.now());
        ucLogout.setTxnSsn(CustomUtil.getSsn());
        ucLogout.setUserCode(ucUserDto.getCode());
        ucLogout.setUserName(ucUserDto.getNickName());
        ucLogout.setStatus(StatusConstant.UcRechargeStatus.WAIT_AUDITED);
        BigDecimal amount = BigDecimal.ZERO;

        QueryWrapper<ProjectOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("project_type", addUcLogout.getProjectType());
        queryWrapper.eq("user_id", ucUserDto.getId());
        queryWrapper.eq("status", StatusConstant.ProjectOrderStatus.IN_HAND);
        List<ProjectOrder> projectOrders = projectOrderMapper.selectList(queryWrapper);
        if (projectOrders != null && projectOrders.size() > 0) {
            for (ProjectOrder projectOrder : projectOrders) {
                amount = amount.add(projectOrder.getAmount());
            }
        }
        ucLogout.setAmount(amount);
        int count = baseMapper.insert(ucLogout);
        return retBool(count);
    }

    @Override
    @Transactional
    public Boolean logoutExamine(ExamineParam para, UcLogout ucLogout) {
        ucLogout.setRemarks(para.getRemarks());
        ucLogout.setAuditTime(LocalDateTime.now());
        ucLogout.setStatus(para.getStatus());
        if (para.getStatus().equals(StatusConstant.UcRechargeStatus.REJECTED)) {
            Integer count = baseMapper.updateById(ucLogout);
            return retBool(count);
        }
        //找出未完成的类型订单，修改注资订单状态，添加余额，修改用户状态，修改申请订单状态
        QueryWrapper<ProjectOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("project_type", ucLogout.getProjectType());
        queryWrapper.eq("user_id", ucLogout.getUserId());
        queryWrapper.eq("status", StatusConstant.ProjectOrderStatus.IN_HAND);
        List<ProjectOrder> projectOrders = projectOrderMapper.selectList(queryWrapper);
        BigDecimal amount = BigDecimal.ZERO;
        if (projectOrders != null && projectOrders.size() > 0) {
            for (ProjectOrder projectOrder : projectOrders) {
                amount = amount.add(projectOrder.getAmount());
                projectOrder.setStatus(StatusConstant.ProjectOrderStatus.QUIT);
                projectOrderMapper.updateById(projectOrder);
            }
            amount = amount.multiply(new BigDecimal(0.6));
            InsertUsableParam insertUsableParam = new InsertUsableParam();
            insertUsableParam.setUsable(amount);
            insertUsableParam.setUserId(ucLogout.getUserId());
            insertUsableParam.setInvestment(amount.negate());
            insertUsableParam.setFreeze(amount.negate());
            insertUsableParam.setType(TypeConstant.usableChangeType.BALANCE_PROJECT_EXIT);
            insertUsableParam.setRemarks(ucLogout.getId());
            ucAssetService.insertUsable(insertUsableParam);

            BigDecimal integral = amount.multiply(new BigDecimal(0.4));
            InsertIntegralParam insertIntegralParam = new InsertIntegralParam();
            insertIntegralParam.setIntegral(integral);
            insertIntegralParam.setUserId(ucLogout.getUserId());
            insertIntegralParam.setType(TypeConstant.integralChangeType.INTEGRAL_PROJECT_EXIT);
            insertIntegralParam.setRemarks(ucLogout.getId());
            ucAssetService.insertIntegral(insertIntegralParam);
        }
        UcUser ucUser = ucUserMapper.selectById(ucLogout.getUserId());
        if (ucUser.getAccountStatus().equals(StatusConstant.UcUserStatus.WAIT_CANCELLATION)) {
            ucUser.setAccountStatus(StatusConstant.UcUserStatus.CANCELLATION);
        }
        if (ucUser.getAccountStatus().equals(StatusConstant.UcUserStatus.ENABLE)) {
            ucUser.setAccountStatus(StatusConstant.UcUserStatus.WAIT_CANCELLATION);
        }
        ucUserMapper.updateById(ucUser);
        Integer count = baseMapper.updateById(ucLogout);
        return retBool(count);
    }
}
