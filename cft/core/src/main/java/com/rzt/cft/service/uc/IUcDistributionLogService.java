package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcDistributionLog;

/**
 * <p>
 * 用户-分销佣金记录表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-22
 */
public interface IUcDistributionLogService extends IService<UcDistributionLog> {

}
