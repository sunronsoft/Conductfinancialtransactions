package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UcUserDetailDto extends UcUserDto{

    @ApiModelProperty("累计返利")
    private BigDecimal accumulationRebate;

    @ApiModelProperty("累加返利积分")
    private BigDecimal accumulationIntegral;

    @ApiModelProperty("累计注资")
    private BigDecimal accumulationInjection;

    @ApiModelProperty("分销等级图标")
    private String icon;

    @ApiModelProperty("收货地址")
    private List<UcReceivingAddressDto> receivingAddressDtos;

    @ApiModelProperty("钱包地址")
    private List<UcWalletDto> ucWalletDtos;
}

