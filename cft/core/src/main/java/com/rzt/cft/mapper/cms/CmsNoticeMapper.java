package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsNotice;

/**
 * <p>
 * 配置表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface CmsNoticeMapper extends BaseMapper<CmsNotice> {

}
