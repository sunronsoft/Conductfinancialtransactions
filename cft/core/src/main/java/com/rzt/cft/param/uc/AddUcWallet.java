package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-钱包地址
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Data
public class AddUcWallet {

    @ApiModelProperty("id 修改时必传")
    private String id;

    @ApiModelProperty("钱包地址名称")
    private String name;

    @ApiModelProperty("地址")
    private String url;

    @ApiModelProperty("二维码")
    private String qrCode;


}
