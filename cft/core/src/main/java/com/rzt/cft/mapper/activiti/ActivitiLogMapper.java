package com.rzt.cft.mapper.activiti;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.activiti.ActivitiLog;

/**
 * <p>
 * 抽奖-抽奖记录 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface ActivitiLogMapper extends BaseMapper<ActivitiLog> {

}
