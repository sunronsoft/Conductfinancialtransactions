package com.rzt.cft.entity.uc;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-提现表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcWithdraw extends AbstractBase {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名字
     */
    private String userName;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 审核状态
     */
    private String status;

    /**
     * 图片地址
     */
    private String picture;

    /**
     * 审核内容
     */
    private String remarks;

    /**
     * 钱包地址id
     */
    private String walletId;

    /**
     * 钱包url
     */
    private String walletUrl;

    /**
     * 钱包二维码
     */
    private String qrCode;

    /**
     * 流订单水号
     */
    private String txnSsn;

    /**
     * 审核时间
     */
    private LocalDateTime auditTime;


}
