package com.rzt.cft.param.sys;

import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
public class LogParam extends PageParam {

    @ApiModelProperty(value = "ip")
    @MpLike
    private String ip;

    @ApiModelProperty(value = "用户名")
    @MpLike
    private String name;

    @ApiModelProperty(value = "操作描述")
    @MpLike
    private String methodDescribe;

    @ApiModelProperty(value = "入参")
    @MpLike
    private String requestParam;

}
