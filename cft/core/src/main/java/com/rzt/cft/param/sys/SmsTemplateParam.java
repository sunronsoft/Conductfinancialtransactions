package com.rzt.cft.param.sys;

import com.rzt.cft.annotation.MpEQ;
import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
public class SmsTemplateParam extends PageParam {

    @ApiModelProperty(value = "标题")
    @MpLike
    private String title;

    @ApiModelProperty(value = "模板类型")
    @MpEQ
    private String templateType;
}
