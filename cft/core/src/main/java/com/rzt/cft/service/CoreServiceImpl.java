package com.rzt.cft.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 通用业务层实现
 * </pre>
 *
 * 2018年1月9日
 *
 * @param <M> mapper
 * @param <T> entity
 */
public abstract class CoreServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements ICoreService<T> {

    @Override
    public List<T> findByKv(Object... params) {
        if (params == null) {
            return new ArrayList<>();
        }
        return baseMapper.selectByMap(convertToMap(params));
    }

    @Override
    public T findOneByKv(Object... params) {
        return selectOne(convertToWrapper(params));
    }

    @Override
    public Map<String, Object> convertToMap(Object... params) {
        Map<String, Object> map = new HashMap<>();
        if (params == null) {
            return map;
        }
        for (int i = 0; i < params.length; i++) {
            if (i % 2 == 1 && params[i] != null && StringUtils.isNotBlank(params[i].toString())) {
                map.put((String) params[i - 1], params[i]);
            }
        }
        return map;
    }

    @Override
    public QueryWrapper<T> convertToWrapper(Object... params) {
        QueryWrapper<T> qw = new QueryWrapper<>();
        if (params == null) {
            return qw;
        }
        for (int i = 0; i < params.length; i++) {
            if (i % 2 == 1 && params[i] != null && StringUtils.isNotBlank(params[i].toString())) {
                qw.eq((String) params[i - 1], params[i]);
            }
        }
        return qw;
    }

    @Override
    public T  selectOne(QueryWrapper<T> t) {
        return baseMapper.selectOne(t);
    }
}
