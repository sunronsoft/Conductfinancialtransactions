package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcIntegral;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户-积分记录 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Component
public interface UcIntegralMapper extends BaseMapper<UcIntegral> {

}
