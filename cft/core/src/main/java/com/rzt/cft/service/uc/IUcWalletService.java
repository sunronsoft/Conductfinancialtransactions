package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcWallet;

/**
 * <p>
 * 用户-钱包地址 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
public interface IUcWalletService extends IService<UcWallet> {

}
