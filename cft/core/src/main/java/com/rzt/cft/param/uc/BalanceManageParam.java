package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 余额管理参数
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class BalanceManageParam {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "变动金额")
    private BigDecimal account;

}
