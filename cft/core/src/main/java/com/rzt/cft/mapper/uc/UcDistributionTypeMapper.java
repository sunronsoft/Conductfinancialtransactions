package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcDistributionType;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户-分销类型 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Component
public interface UcDistributionTypeMapper extends BaseMapper<UcDistributionType> {

}
