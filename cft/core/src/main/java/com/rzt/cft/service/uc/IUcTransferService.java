package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.uc.UcTransfer;
import com.rzt.cft.param.uc.AddTransferParam;

/**
 * <p>
 * 用户-转账记录 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IUcTransferService extends IService<UcTransfer> {

    Boolean transfer(UcUserDto ucUserDto, AddTransferParam param);
}
