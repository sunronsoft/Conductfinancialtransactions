package com.rzt.cft.service.uc;

import com.rzt.cft.entity.uc.UcUser;
import com.rzt.cft.param.sys.RegisterUser;
import com.rzt.cft.param.uc.UpgradeParam;
import com.rzt.cft.service.ICoreService;

/**
 * <p>
 * 普通用户表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface IUcUserService extends ICoreService<UcUser> {

    Boolean registerUser(String userName, String phone, String email, String account, String headPortrait, String pwd);

    Boolean appRegisterUser(RegisterUser registerUser);

    void moneyUpgrade(UpgradeParam param);

    void numUpgrade(UpgradeParam param);
}
