package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.dto.sys.SysUserDto;
import com.rzt.cft.entity.sys.SysUser;
import com.rzt.cft.param.sys.SysUserJobParam;

/**
 * <p>
 * 系统-后台用户 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 添加用户
     * @param user
     * @return
     */
    Boolean addUser(SysUserDto user);

    /**
     * 用户岗位分配
     *
     * @param userJobDto
     * @return
     */
    boolean allotUserJob(SysUserJobParam userJobDto);
}
