package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysAgreement;

/**
 * <p>
 * 系统协议配置 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-17
 */
public interface SysAgreementMapper extends BaseMapper<SysAgreement> {

}
