package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcReceivingAddress;

/**
 * <p>
 * 用户-收货地址列表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface UcReceivingAddressMapper extends BaseMapper<UcReceivingAddress> {

}
