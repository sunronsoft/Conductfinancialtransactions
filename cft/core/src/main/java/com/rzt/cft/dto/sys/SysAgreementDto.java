package com.rzt.cft.dto.sys;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 系统协议配置
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-17
 */
@Data
public class SysAgreementDto extends AbstractBaseDto {

    @ApiModelProperty("协议名称")
    private String name;

    @ApiModelProperty("协议key")
    private String configKey;

    @ApiModelProperty("协议内容")
    private String configValue;

    @ApiModelProperty("描述")
    private String remark;

    @ApiModelProperty("rich_text富文本，text文本框")
    private String type;

}
