package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsFeedback;

/**
 * <p>
 * 运营-问题反馈 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-06
 */
public interface CmsFeedbackMapper extends BaseMapper<CmsFeedback> {

}
