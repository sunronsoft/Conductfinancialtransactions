package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.uc.UcLogout;
import com.rzt.cft.param.uc.AddUcLogout;
import com.rzt.cft.param.uc.ExamineParam;

/**
 * <p>
 * 用户-退出审核表单 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
public interface IUcLogoutService extends IService<UcLogout> {

    Boolean logoutAdd(AddUcLogout addUcLogout, UcUserDto ucUserDto);

    Boolean logoutExamine(ExamineParam para, UcLogout ucLogout);
}
