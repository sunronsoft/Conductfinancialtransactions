package com.rzt.cft.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 通用业务层实现
 * </pre>
 * 
 * 2018年1月9日
 * 
 * @param <T>
 */
public interface ICoreService<T> extends IService<T> {


    List<T> findByKv(Object... param);


    T findOneByKv(Object... param);

    /**
     * <pre>
     * </pre>
     * 2018/6/14 17:32
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */

    Map<String, Object> convertToMap(Object... param);

    /**
     * <pre>
     * </pre>
     * 2018/6/14 17:14
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */

    QueryWrapper<T> convertToWrapper(Object... params);

    /**
     * 根据实体查询
     * @param t
     * @return
     */
    T selectOne(QueryWrapper<T> t);
}
