package com.rzt.cft.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.cms.CmsResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 * 基础-资源信息 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface ICmsResourceService extends IService<CmsResource> {

    String uploadFile(MultipartFile file) throws IOException;
}
