package com.rzt.cft.dto.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ZZ
 */
@Data
@ApiModel(value = "角色功能关联")
public class RoleAndMenuDto {

    @ApiModelProperty(value = "菜单功能标识")
    private Integer menuId;

    @ApiModelProperty(value = "角色标识")
    private Integer roleId;
}
