package com.rzt.cft.param.sys;

import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 配置表
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
public class SysConfigPageParam extends PageParam {

    @ApiModelProperty(value = "配置值")
    @MpLike
    private String configValue;

    @ApiModelProperty(value = "配置名称")
    @MpLike
    private String name;

    @ApiModelProperty(value = "配置key")
    @MpLike
    private String configKey;

}
