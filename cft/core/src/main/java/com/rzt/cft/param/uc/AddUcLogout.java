package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-退出表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class AddUcLogout {

    @ApiModelProperty("项目类型 (自选 optional； 智能 intelligence)")
    private String projectType;

}
