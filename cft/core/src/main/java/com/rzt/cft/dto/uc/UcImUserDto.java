package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UcImUserDto {

    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("头像")
    private String headPortrait;

}

