package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 配置表
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
public class AppSysConfigParam {

    @ApiModelProperty(value = "配置key（用户协议:user_agreement;注资协议:capital_injection_agreement;退出申请说明:exit_application_description）")
    private String configKey;

}
