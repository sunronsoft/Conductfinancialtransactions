package com.rzt.cft.entity.sys;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-岗位
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysJob extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 机构标识
     */
    private String organizationId;

    /**
     * 名称
     */
    private String name;

    /**
     * 是否管理者
     */
    private Boolean isSuper;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * 描述
     */
    private String remark;


}
