package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
public class ImUserIdParam {

    @ApiModelProperty("用户id")
    private String userId;
}
