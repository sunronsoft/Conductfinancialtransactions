package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 配置表
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
public class AddSysConfig {

    @ApiModelProperty(value = "id,添加时不填，修改时才填该参数")
    private String id;

    @ApiModelProperty(value = "配置值")
    @NotNull(message = "配置值不能为空")
    private String configValue;

    @ApiModelProperty(value = "配置名称")
    @NotNull(message = "配置名称不能为空")
    private String name;

    @ApiModelProperty(value = "配置key")
    @NotNull(message = "配置key不能为空")
    private String configKey;

    @ApiModelProperty(value = "描述")
    private String remark;

}
