package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcBank;

/**
 * <p>
 * 用户-钱包地址 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface UcBankMapper extends BaseMapper<UcBank> {

}
