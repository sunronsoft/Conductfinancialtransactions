package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UpdateAgentAddIntegral {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("增加金币数")
    private BigDecimal integral;

    @ApiModelProperty("备注")
    private String remarks;
}

