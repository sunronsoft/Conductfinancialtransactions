package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysUser;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * 系统-后台用户 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Component
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 查询用户的所有权限
     * @param userId  用户ID
     */
    List<String> queryAllPerms(Integer userId);

    /**
     * 查询用户的所有菜单ID
     */
    List<Integer> queryAllMenuId(Integer userId);

}
