package com.rzt.cft.service.cms;

import com.rzt.cft.entity.cms.CmsRegion;
import com.rzt.cft.service.ICoreService;

/**
 * <p>
 * 基础-地区 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface ICmsRegionService extends ICoreService<CmsRegion> {

}
