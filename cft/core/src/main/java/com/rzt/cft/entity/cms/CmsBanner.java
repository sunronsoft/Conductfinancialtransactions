package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 运营-banner
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsBanner extends AbstractBase {
    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subhead;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 显示端
     */
    private String portId;

    /**
     * 显示类型
     */
    private String showType;

    /**
     * 链接
     */
    private String link;

    /**
     * 图片链接
     */
    private String imgLink;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 截止时间
     */
    private LocalDateTime endTime;

    /**
     * 描述
     */
    private String remark;


}
