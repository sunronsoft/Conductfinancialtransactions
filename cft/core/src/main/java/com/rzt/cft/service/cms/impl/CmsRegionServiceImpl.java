package com.rzt.cft.service.cms.impl;

import com.rzt.cft.entity.cms.CmsRegion;
import com.rzt.cft.mapper.cms.CmsRegionMapper;
import com.rzt.cft.service.CoreServiceImpl;
import com.rzt.cft.service.cms.ICmsRegionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 基础-地区 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class CmsRegionServiceImpl extends CoreServiceImpl<CmsRegionMapper, CmsRegion> implements ICmsRegionService {

}
