package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-分销类型
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcDistributionType extends AbstractBase {

    private String name;

    private String showName;

    /**
     * 同级别需要金额
     */
    private BigDecimal howMoney;

    /**
     * 同级别需要人数
     */
    private Integer howNum;

    /**
     * 条件是否都满足
     */
    private Boolean isAnd;

    /**
     * 分销层级
     */
    private String level;

    /**
     * 直推日返佣金比例 
     */
    private BigDecimal directRatio;

    /**
     * 级别佣金比例
     */
    private BigDecimal levelRatio;

    /**
     * 收益上限
     */
    private BigDecimal incomeCeiling;

    /**
     * 用户是否默认该等级
     */
    private Boolean isDefault;

    /**
     * 图标
     */
    private String icon;

}
