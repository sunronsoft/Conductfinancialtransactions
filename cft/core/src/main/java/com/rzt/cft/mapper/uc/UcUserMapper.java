package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcUser;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 普通用户表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Component
public interface UcUserMapper extends BaseMapper<UcUser> {

    Integer countNum();
}
