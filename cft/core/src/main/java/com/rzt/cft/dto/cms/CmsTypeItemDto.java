package com.rzt.cft.dto.cms;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/10 15:38
 */
@Data
public class CmsTypeItemDto {

    private String id;

    private String createId;

    @JsonIgnore
    private String updateId;

    @ApiModelProperty(value = "类型标识")
    private String typeId;

    @ApiModelProperty(value = "类型简化序列号")
    private String typeSimplify;

    @ApiModelProperty(value = "父级标识")
    private String parentId;

    @JsonIgnore
    private String parentSimplify;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "副标题")
    private String subhead;

    @ApiModelProperty(value = "简化序列号")
    private String simplify;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "是否启用")
    private Boolean isUse;

    @ApiModelProperty(value = "图片")
    private String imgLink;

    @ApiModelProperty(value = "扩展字段")
    private String extend;

    @ApiModelProperty(value = "备注")
    private String remark;
}
