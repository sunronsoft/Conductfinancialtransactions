package com.rzt.cft.service.cms.impl;

import com.rzt.cft.service.cms.ICmsAffiliationTypeService;
import com.rzt.cft.entity.cms.CmsAffiliationType;
import com.rzt.cft.mapper.cms.CmsAffiliationTypeMapper;
import com.rzt.cft.service.CoreServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 基础-归属类型 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class CmsAffiliationTypeServiceImpl extends CoreServiceImpl<CmsAffiliationTypeMapper, CmsAffiliationType> implements ICmsAffiliationTypeService {

}
