package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zz
 */
@Data
public class UserEditPwdParam {

    @ApiModelProperty(value = "原密码")
    @NotNull(message = "原密码不能为空")
    private String oldPwd;

    @ApiModelProperty(value = "新密码")
    @NotNull(message = "新密码不能为空")
    private String newPwd;

}
