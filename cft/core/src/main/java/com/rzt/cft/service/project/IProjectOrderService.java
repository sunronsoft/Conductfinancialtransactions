package com.rzt.cft.service.project;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.project.ProjectOrder;

/**
 * <p>
 * 项目-订单表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
public interface IProjectOrderService extends IService<ProjectOrder> {

}
