package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsTypeItem;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 基础-字典类型项 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Component
public interface CmsTypeItemMapper extends BaseMapper<CmsTypeItem> {

}
