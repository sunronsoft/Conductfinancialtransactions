package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-资产信息统计
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcAssetStatistics extends AbstractBase {

    /**
     * 用户标识
     */
    private String userId;

    /**
     * 累计注资
     */
    private BigDecimal accumulationInjection;

    /**
     * 累计返利
     */
    private BigDecimal accumulationRebate;

    /**
     * 累加返利积分
     */
    private BigDecimal accumulationIntegral;

    /**
     * 是否当前
     */
    private Boolean current;


}
