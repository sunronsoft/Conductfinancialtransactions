package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsAffiliationType;

/**
 * <p>
 * 基础-归属类型 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface CmsAffiliationTypeMapper extends BaseMapper<CmsAffiliationType> {

}
