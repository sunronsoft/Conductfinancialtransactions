package com.rzt.cft.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.sys.SysRole;
import com.rzt.cft.mapper.sys.SysRoleMapper;
import com.rzt.cft.service.sys.ISysRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统-后台角色 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
