package com.rzt.cft.service.goods.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.goods.GoodsDetail;
import com.rzt.cft.mapper.goods.GoodsDetailMapper;
import com.rzt.cft.service.goods.IGoodsDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-详情 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class GoodsDetailServiceImpl extends ServiceImpl<GoodsDetailMapper, GoodsDetail> implements IGoodsDetailService {

}
