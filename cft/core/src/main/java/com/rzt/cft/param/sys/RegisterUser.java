package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 钟忠
 * @date 2020/1/13 9:49
 */
@Data
public class RegisterUser {

    @ApiModelProperty("账号")
    @NotNull(message = "账号不能为空")
    private String account;

    @ApiModelProperty("手机号")
    @NotNull(message = "手机号不能为空")
//    @Pattern(regexp = RegexConstant.PHONE_REGEX, message = "手机号格式不正确")
    private String phone;

    @ApiModelProperty("密码")
    @NotNull(message = "密码不能为空")
    private String password;

//    @ApiModelProperty("短信简化序列号")
//    @NotNull(message = "短信简化序列号不能为空")
//    private String simplify;
//
//    @ApiModelProperty("验证码")
//    @NotNull(message = "验证码不能为空")
//    private String code;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("邀请码")
    private String parentId;

}
