package com.rzt.cft.entity.uc;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-钱包地址
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcBank extends AbstractBase {
    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名字
     */
    private String userName;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 卡号
     */
    private String cardNo;

    /**
     * 银行编码
     */
    private String bankCode;

    /**
     * 开户地址
     */
    private String region;

    /**
     * 银行名字
     */
    private String bankName;


}
