package com.rzt.cft.param.uc;

import com.rzt.cft.constant.RegexConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class AddUcStore {

    @ApiModelProperty("门头照片")
    private String logo;

    @ApiModelProperty("房产公司id")
    @NotNull(message = "房产公司id不能为空")
    private String enterpriseId;

    @ApiModelProperty("店主手机号")
    @Pattern(regexp = RegexConstant.PHONE_REGEX, message = "手机号格式不正确")
    private String shopownerPhone;

    @ApiModelProperty("账号")
    @NotNull(message = "账号不能为空")
    private String account;

    @ApiModelProperty("地址")
    @NotNull(message = "地址不能为空")
    private String address;

    @ApiModelProperty("门店名称")
    @NotNull(message = "门店名称不能为空")
    private String name;

    @ApiModelProperty("店主姓名")
    @NotNull(message = "店主姓名不能为空")
    private String shopownerName;

    @ApiModelProperty("营业执照号")
    private String license;

    @ApiModelProperty("服务范围")
    private String serviceScope;

}

