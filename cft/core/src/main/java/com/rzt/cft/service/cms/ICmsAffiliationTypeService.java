package com.rzt.cft.service.cms;

import com.rzt.cft.entity.cms.CmsAffiliationType;
import com.rzt.cft.service.ICoreService;

/**
 * <p>
 * 基础-归属类型 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface ICmsAffiliationTypeService extends ICoreService<CmsAffiliationType> {

}
