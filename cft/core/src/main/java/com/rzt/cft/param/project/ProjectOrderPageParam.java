package com.rzt.cft.param.project;

import com.rzt.cft.annotation.MpEQ;
import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class ProjectOrderPageParam extends PageParam {

    @ApiModelProperty(value = "订单编号")
    @MpLike
    private String orderSn;

    @ApiModelProperty(value = "用户名字")
    @MpLike
    private String userName;

    @ApiModelProperty(value = "订单状态（0 进行中，已完成，已退出）")
    @MpEQ
    private String status;

    @ApiModelProperty("项目类型")
    @MpEQ
    private String projectType;

}
