package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UcDevelopersDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("企业名称")
    @NotNull(message = "企业名称不能为空")
    private String name;

    @ApiModelProperty("企业logo")
    private String logo;

}

