package com.rzt.cft.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.cms.CmsFeedback;

/**
 * <p>
 * 运营-问题反馈 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-06
 */
public interface ICmsFeedbackService extends IService<CmsFeedback> {

}
