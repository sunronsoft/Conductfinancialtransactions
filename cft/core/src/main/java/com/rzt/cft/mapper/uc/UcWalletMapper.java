package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcWallet;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户-钱包地址 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Component
public interface UcWalletMapper extends BaseMapper<UcWallet> {

}
