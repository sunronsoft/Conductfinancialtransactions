package com.rzt.cft.dto.uc;

import lombok.Data;

/**
 * <p>
 * 用户-分销类型
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class DistributionTypeDto {

    private String name;

    private String id;

}
