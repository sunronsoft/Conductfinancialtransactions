package com.rzt.cft.entity.sys;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-后台机构
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDept extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 名称
     */
    private String name;

    /**
     * 备用名称
     */
    private String subhead;

    /**
     * 简化序列号（备用）
     */
    private String simplify;

    /**
     * 父级标识
     */
    private String parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 图标
     */
    private String ico;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * 描述
     */
    private String remark;


}
