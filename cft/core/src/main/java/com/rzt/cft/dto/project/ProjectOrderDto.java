package com.rzt.cft.dto.project;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目-订单表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Data
public class ProjectOrderDto extends AbstractBaseDto {

    @ApiModelProperty("项目id")
    private String projectId;

    @ApiModelProperty("项目名称")
    private String projectName;

    @ApiModelProperty("项目类型(optional 自选； intelligence 智能)")
    private String projectType;

    @ApiModelProperty("购买用户id")
    private String userId;

    @ApiModelProperty("用户账号")
    private String userAccount;

    @ApiModelProperty("订单编号")
    private String orderSn;

    @ApiModelProperty("订单状态")
    private String status;

    @ApiModelProperty("订单金额")
    private BigDecimal amount;

    @ApiModelProperty("日利率")
    private BigDecimal dailyInverse;

    @ApiModelProperty("项目编码")
    private String projectSn;

    @ApiModelProperty("注资时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime injectionTime;

    @ApiModelProperty("订单结束时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime completionTime;

    @ApiModelProperty("期限")
    private String term;

    @ApiModelProperty("项目发布时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime releaseTime;

}
