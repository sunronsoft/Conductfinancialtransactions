package com.rzt.cft.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.sys.SysOperationLog;
import com.rzt.cft.mapper.sys.SysOperationLogMapper;
import com.rzt.cft.service.sys.ISysOperationLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统-操作日志 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-07
 */
@Service
public class SysOperationLogServiceImpl extends ServiceImpl<SysOperationLogMapper, SysOperationLog> implements ISysOperationLogService {

}
