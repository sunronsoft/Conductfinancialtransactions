package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcDistributionType;

/**
 * <p>
 * 用户-分销类型 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IUcDistributionTypeService extends IService<UcDistributionType> {

}
