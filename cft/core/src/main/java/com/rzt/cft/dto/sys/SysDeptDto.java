package com.rzt.cft.dto.sys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/3 15:18
 */
@Data
public class SysDeptDto extends AbstractBaseDto {

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 备用名称
     */
    @ApiModelProperty(value = "备用名称")
    private String subhead;

    /**
     * 简化序列号（备用）
     */
    @JsonIgnore
    private String simplify;

    /**
     * 父级标识
     */
    @ApiModelProperty(value = "父级标识")
    private Integer parentId;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
     * 图标
     */
    @ApiModelProperty(value = "图标")
    private String ico;

    /**
     * 是否启用
     */
    @ApiModelProperty(value = "是否启用")
    private Boolean isUse;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String remark;
}
