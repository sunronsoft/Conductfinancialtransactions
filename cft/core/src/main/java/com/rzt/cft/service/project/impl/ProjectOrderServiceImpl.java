package com.rzt.cft.service.project.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.service.project.IProjectOrderService;
import com.rzt.cft.entity.project.ProjectOrder;
import com.rzt.cft.mapper.project.ProjectOrderMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目-订单表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Service
public class ProjectOrderServiceImpl extends ServiceImpl<ProjectOrderMapper, ProjectOrder> implements IProjectOrderService {

}
