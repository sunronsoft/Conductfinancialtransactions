package com.rzt.cft.dto.uc;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-分销类型
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class UcDistributionTypeDto extends AbstractBaseDto {

    @ApiModelProperty("名字")
    private String name;

    @ApiModelProperty("展示名字")
    private String showName;

    @ApiModelProperty("同级别需要金额")
    private BigDecimal howMoney;

    @ApiModelProperty("同级别需要人数")
    private Integer howNum;

    @ApiModelProperty("人数和金额是否都满足")
    private Boolean isAnd;

    @ApiModelProperty("分销层级")
    private String level;

    @ApiModelProperty("直推日返佣金比例")
    private BigDecimal directRatio;

    @ApiModelProperty("级别佣金比例")
    private BigDecimal levelRatio;

    @ApiModelProperty("收益上限")
    private BigDecimal incomeCeiling;

    @ApiModelProperty("用户是否默认该等级")
    private Boolean isDefault;

    @ApiModelProperty("图标")
    private String icon;

}
