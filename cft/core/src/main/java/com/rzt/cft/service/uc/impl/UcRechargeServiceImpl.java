package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.constant.StatusConstant;
import com.rzt.cft.constant.TypeConstant;
import com.rzt.cft.entity.uc.UcRecharge;
import com.rzt.cft.mapper.uc.UcRechargeMapper;
import com.rzt.cft.param.uc.ExamineParam;
import com.rzt.cft.param.uc.InsertUsableParam;
import com.rzt.cft.service.uc.IUcAssetService;
import com.rzt.cft.service.uc.IUcRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户-充值表单 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcRechargeServiceImpl extends ServiceImpl<UcRechargeMapper, UcRecharge> implements IUcRechargeService {

    @Autowired
    private IUcAssetService ucAssetService;

    @Override
    @Transactional
    public Boolean rechargeExamine(ExamineParam para, UcRecharge ucRecharge) {
        //如果是通过先加余额
        if (para.getStatus().equals(StatusConstant.UcRechargeStatus.ADOPT)) {
            InsertUsableParam insertUsableParam = new InsertUsableParam();
            insertUsableParam.setUserId(ucRecharge.getUserId());
            insertUsableParam.setUserName(ucRecharge.getUserName());
            insertUsableParam.setType(TypeConstant.usableChangeType.BALANCE_RECHARGE);
            insertUsableParam.setUsable(ucRecharge.getAmount());
            insertUsableParam.setRemarks(ucRecharge.getId());
            ucAssetService.insertUsable(insertUsableParam);
        }
        //修改订单状态
        ucRecharge.setStatus(para.getStatus());
        ucRecharge.setRemarks(para.getRemarks());
        ucRecharge.setAuditTime(LocalDateTime.now());
        int count = baseMapper.updateById(ucRecharge);

        return retBool(count);
    }
}
