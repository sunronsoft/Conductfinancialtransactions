package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-返利记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcRebateLog extends AbstractBase {

    /**
     * 流水号
     */
    private String txnSsn;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 项目订单id
     */
    private String projectOrderId;

    /**
     * 返利积分
     */
    private BigDecimal integral;

    /**
     * 返利金额
     */
    private BigDecimal amt;

    /**
     * 类型(自选返利，智能返利，分销返利）
     */
    private String type;

    /**
     * 备注
     */
    private String remarks;


}
