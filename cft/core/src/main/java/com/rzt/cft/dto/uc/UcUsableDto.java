package com.rzt.cft.dto.uc;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-积分记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class UcUsableDto extends AbstractBaseDto {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户名字")
    private String userName;

    @ApiModelProperty("变动类型")
    private String type;

    @ApiModelProperty("变动金额")
    private BigDecimal changeAmount;

    @ApiModelProperty("可提现余额")
    private BigDecimal usable;

    @ApiModelProperty("冻结余额")
    private BigDecimal freeze;

    @ApiModelProperty("是否最新")
    private Boolean current;

    @ApiModelProperty("备注")
    private String remarks;

    @ApiModelProperty("项目类型")
    private String projectType;

    @ApiModelProperty("项目名称")
    private String projectName;

}
