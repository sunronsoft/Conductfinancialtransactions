package com.rzt.cft.dto.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
@ApiModel(value = "岗位角色关联")
public class JobAndRoleDto {

    @ApiModelProperty(value = "岗位标识")
    private Integer jobId;

    @ApiModelProperty(value = "角色标识")
    private Integer roleId;

}
