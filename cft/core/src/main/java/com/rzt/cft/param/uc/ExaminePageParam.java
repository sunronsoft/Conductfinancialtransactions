package com.rzt.cft.param.uc;

import com.rzt.cft.annotation.MpEQ;
import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class ExaminePageParam extends PageParam {

    @ApiModelProperty(value = "订单编号")
    @MpLike
    private String txnSsn;

    @ApiModelProperty(value = "用户名字")
    @MpLike
    private String userName;

    @ApiModelProperty(value = "用户编号")
    @MpLike
    private String userCode;

    @ApiModelProperty(value = "审核状态（0 待审核，1已通过，2已拒绝）")
    @MpEQ
    private String status;

}
