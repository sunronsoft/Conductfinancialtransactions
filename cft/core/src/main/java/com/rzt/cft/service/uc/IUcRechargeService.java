package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcRecharge;
import com.rzt.cft.param.uc.ExamineParam;

/**
 * <p>
 * 用户-充值表单 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IUcRechargeService extends IService<UcRecharge> {

    Boolean rechargeExamine(ExamineParam para, UcRecharge ucRecharge);
}
