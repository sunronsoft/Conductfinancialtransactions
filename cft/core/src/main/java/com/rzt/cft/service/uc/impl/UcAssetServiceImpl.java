package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcAsset;
import com.rzt.cft.entity.uc.UcIntegral;
import com.rzt.cft.entity.uc.UcUsable;
import com.rzt.cft.mapper.uc.UcAssetMapper;
import com.rzt.cft.mapper.uc.UcIntegralMapper;
import com.rzt.cft.mapper.uc.UcUsableMapper;
import com.rzt.cft.param.uc.InsertIntegralParam;
import com.rzt.cft.param.uc.InsertUsableParam;
import com.rzt.cft.service.uc.IUcAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * <p>
 * 用户-资产信息 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Service
public class UcAssetServiceImpl extends ServiceImpl<UcAssetMapper, UcAsset> implements IUcAssetService {

    @Autowired
    private UcUsableMapper ucUsableMapper;

    @Autowired
    private UcIntegralMapper ucIntegralMapper;

    /**
     * 余额变动
     * @return
     */
    @Transactional
    public Boolean insertUsable(InsertUsableParam param) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("current", true);
        queryWrapper.eq("user_id", param.getUserId());
        UcAsset oldUcAsset = baseMapper.selectOne(queryWrapper);
        UcUsable oldUcUsable = ucUsableMapper.selectOne(queryWrapper);
        BigDecimal ucable = BigDecimal.ZERO;
        BigDecimal freeze = BigDecimal.ZERO;
        BigDecimal investment = BigDecimal.ZERO;
        BigDecimal integral = BigDecimal.ZERO;
        if (Objects.nonNull(oldUcUsable)) {
            ucable = oldUcUsable.getUsable();
            freeze = oldUcUsable.getFreeze();
        }
        if (Objects.nonNull(oldUcAsset)) {
            investment = oldUcAsset.getInvestment();
            integral = oldUcAsset.getIntegral();
        }
        UcUsable ucUsable = new UcUsable();
        UcAsset ucAsset = new UcAsset();
        if (param.getUsable() != null) {
            ucable = ucable.add(param.getUsable());
        }
        if (param.getFreeze() != null) {
            freeze = freeze.add(param.getFreeze());
        }
        if (param.getInvestment() != null) {
            investment = investment.add(param.getInvestment());
        }
        ucUsable.setChangeAmount(param.getUsable());
        ucUsable.setCurrent(true);
        ucUsable.setFreeze(freeze);
        ucUsable.setUsable(ucable);
        ucUsable.setUserId(param.getUserId());
        ucUsable.setType(param.getType());
        ucUsable.setUserName(param.getUserName());
        ucUsable.setRemarks(param.getRemarks());
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("current", true);
        updateWrapper.eq("user_id", param.getUserId());
        UcAsset updateUcAsset = new UcAsset();
        updateUcAsset.setCurrent(false);
        UcUsable updateUcUsable = new UcUsable();
        updateUcUsable.setCurrent(false);

        ucAsset.setCurrent(true);
        ucAsset.setFreeze(freeze);
        ucAsset.setIntegral(integral);
        ucAsset.setChangeType(param.getType());
        ucAsset.setInvestment(investment);
        ucAsset.setUsable(ucable);
        ucAsset.setUserId(param.getUserId());

        baseMapper.update(updateUcAsset, updateWrapper);
        ucUsableMapper.update(updateUcUsable, updateWrapper);

        ucUsableMapper.insert(ucUsable);

        ucAsset.setCapitalLogId(ucUsable.getId());
        Integer count = baseMapper.insert(ucAsset);
        return retBool(count);
    }

    /**
     * 积分变动
     * @return
     */
    @Transactional
    public Boolean insertIntegral(InsertIntegralParam param) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("current", true);
        queryWrapper.eq("user_id", param.getUserId());
        UcAsset oldUcAsset = baseMapper.selectOne(queryWrapper);
        UcIntegral oldUcIntegral = ucIntegralMapper.selectOne(queryWrapper);
        BigDecimal integral = param.getIntegral();
        if (Objects.nonNull(oldUcIntegral)) {
            integral = oldUcIntegral.getIntegral().add(param.getIntegral());
        }
        UcIntegral ucIntegral = new UcIntegral();
        UcAsset ucAsset = new UcAsset();
        ucIntegral.setChangeIntegral(param.getIntegral());
        ucIntegral.setCurrent(true);
        ucIntegral.setType(param.getType());
        ucIntegral.setUserId(param.getUserId());
        ucIntegral.setIntegral(integral);
        ucIntegral.setUserName(param.getUserName());
        ucIntegral.setRemarks(param.getRemarks());

        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("current", true);
        updateWrapper.eq("user_id", param.getUserId());
        UcAsset updateUcAsset = new UcAsset();
        updateUcAsset.setCurrent(false);
        UcIntegral updateUcIntegral = new UcIntegral();
        updateUcIntegral.setCurrent(false);

        ucAsset.setCurrent(true);
        ucAsset.setFreeze(oldUcAsset == null ? BigDecimal.ZERO : oldUcAsset.getFreeze());
        ucAsset.setIntegral(integral);
        ucAsset.setChangeType(param.getType());
        ucAsset.setInvestment(oldUcAsset == null ? BigDecimal.ZERO : oldUcAsset.getInvestment());
        ucAsset.setUsable(oldUcAsset == null ? BigDecimal.ZERO : oldUcAsset.getUsable());
        ucAsset.setUserId(param.getUserId());

        baseMapper.update(updateUcAsset, updateWrapper);
        ucIntegralMapper.update(updateUcIntegral, updateWrapper);

        ucIntegralMapper.insert(ucIntegral);

        ucAsset.setCapitalLogId(ucIntegral.getId());
        Integer count = baseMapper.insert(ucAsset);
        return retBool(count);
    }

    @Override
    public UcUsable queryUsable(String userId) {
        QueryWrapper<UcUsable> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        queryWrapper.eq("current", true);
        UcUsable ucUsable = ucUsableMapper.selectOne(queryWrapper);
        return ucUsable;
    }
}
