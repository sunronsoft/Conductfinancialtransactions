package com.rzt.cft.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.service.sys.ISysJobRoleService;
import com.rzt.cft.entity.sys.SysJobRole;
import com.rzt.cft.mapper.sys.SysJobRoleMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统-岗位角色 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Service
public abstract class SysJobRoleServiceImpl extends ServiceImpl<SysJobRoleMapper, SysJobRole> implements ISysJobRoleService {

}
