package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class AgentAuth {

    @ApiModelProperty("门店id")
    @NotNull(message = "门店id不能为空")
    private String storeId;

    @ApiModelProperty("手机")
    private String phone;

    @ApiModelProperty("姓名")
    @NotNull(message = "姓名")
    private String name;

    @ApiModelProperty("身份证正面照片")
    @NotNull(message = "身份证正面照片不能为空")
    private String idcardFront;

    @ApiModelProperty("身份证背面照片")
    @NotNull(message = "身份证背面照片不能为空")
    private String idcardRear;

}

