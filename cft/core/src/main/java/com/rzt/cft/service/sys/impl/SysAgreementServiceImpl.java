package com.rzt.cft.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.sys.SysAgreement;
import com.rzt.cft.mapper.sys.SysAgreementMapper;
import com.rzt.cft.service.sys.ISysAgreementService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统协议配置 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-17
 */
@Service
public class SysAgreementServiceImpl extends ServiceImpl<SysAgreementMapper, SysAgreement> implements ISysAgreementService {

}
