package com.rzt.cft.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.goods.GoodsAttribute;

/**
 * <p>
 * 商品-参数表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IGoodsAttributeService extends IService<GoodsAttribute> {

}
