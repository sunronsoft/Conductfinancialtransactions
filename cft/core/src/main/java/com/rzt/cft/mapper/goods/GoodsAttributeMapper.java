package com.rzt.cft.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.goods.GoodsAttribute;

/**
 * <p>
 * 商品-参数表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface GoodsAttributeMapper extends BaseMapper<GoodsAttribute> {

}
