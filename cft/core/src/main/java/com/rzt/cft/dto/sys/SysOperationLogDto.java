package com.rzt.cft.dto.sys;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/3/7 11:57
 */
@Data
public class SysOperationLogDto extends AbstractBaseDto {

    @ApiModelProperty("操作人名字")
    private String createName;

    @ApiModelProperty("操作url")
    private String url;

    @ApiModelProperty("操作类型")
    private String methodType;

     @ApiModelProperty("操作描述")
    private String methodDescribe;

    @ApiModelProperty("入参（超长做字符串截取）")
    private String requestParam;

//    @ApiModelProperty("出参（超长做字符串截取）")
//    private String responParam;
//
//    @ApiModelProperty("请求内部返回码")
//    private String responCode;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("ip")
    private String ip;

    @ApiModelProperty("用时")
    private Integer useTime;


}
