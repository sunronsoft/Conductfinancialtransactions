package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UpdateUcAgentStatus {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("状态（0,启用【审核通过】、1禁用、2冻结、3待审核、4审核拒绝）")
    private Integer status;

    @ApiModelProperty("审核备注")
    private Integer remark;
}

