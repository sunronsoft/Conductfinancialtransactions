package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UpdateUcAgent {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("经纪人名字")
    private String name;

    @ApiModelProperty("门店id")
    private String storeId;

    @ApiModelProperty("手机")
    private String phone;

    @ApiModelProperty("电子邮件")
    private String email;

    @ApiModelProperty("描述")
    private String aboutUs;

    @ApiModelProperty("入职时间")
    private LocalDateTime entryTime;

    @ApiModelProperty("服务年限")
    private Integer workYear;

    @ApiModelProperty("头像")
    private String headPortrait;

    @ApiModelProperty("个人介绍视频")
    private String introduceUrl;

    @ApiModelProperty("主营小区id")
    private List<String> mainResidentialId;

}

