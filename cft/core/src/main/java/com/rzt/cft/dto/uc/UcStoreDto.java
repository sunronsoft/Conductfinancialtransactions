package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UcStoreDto {

    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("门店名称")
    private String name;

    @ApiModelProperty("公司企业id")
    private String enterpriseId;

    @ApiModelProperty("门店主信息id")
    private String userId;

    @ApiModelProperty("门头图片")
    private String logo;

    @ApiModelProperty("店主姓名")
    private String shopownerName;

    @ApiModelProperty("店主电话")
    private String shopownerPhone;

    @ApiModelProperty("店主身份证")
    private String shopownerIdcard;

    @ApiModelProperty("实际地址")
    private String address;

    @ApiModelProperty("营业执照")
    private String license;

    @ApiModelProperty("公司名称")
    private String enterpriseName;

    @ApiModelProperty("账号标识")
    private String accountId;

    @ApiModelProperty("账号")
    private String account;

}

