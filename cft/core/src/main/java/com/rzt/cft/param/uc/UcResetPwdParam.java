package com.rzt.cft.param.uc;

import com.rzt.cft.constant.RegexConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author zz
 */
@Data
public class UcResetPwdParam {

    @ApiModelProperty("手机号")
    @Pattern(regexp = RegexConstant.PHONE_REGEX, message = "手机号格式不正确")
    private String phone;

    @ApiModelProperty("短信简化序列号")
    @NotNull(message = "短信简化序列号不能为空")
    private String simplify;

    @ApiModelProperty("验证码")
    @NotNull(message = "验证码不能为空")
    private String code;

    @ApiModelProperty(value = "新密码")
    @NotNull(message = "新密码不能为空")
    private String newPwd;

}
