package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.sys.SysMenu;
import com.rzt.cft.entity.sys.SysRoleMenu;

import java.util.List;

/**
 * <p>
 * 系统-后台角色功能 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    Boolean allotRoleMenu(String roleId, List<SysMenu> menuListList);

}
