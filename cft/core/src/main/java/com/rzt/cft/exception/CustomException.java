package com.rzt.cft.exception;


import com.rzt.cft.constant.ServiceStatus;

/**
 * 自定义异常类型，继承运行时异常 RuntimeException
 **/
public class CustomException extends RuntimeException {

    // 错误代码
    ServiceStatus serviceStatus;

    public CustomException(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }
}