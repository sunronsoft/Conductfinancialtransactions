package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
public class ResetPwdParam {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "密码")
    private String pwd;

}
