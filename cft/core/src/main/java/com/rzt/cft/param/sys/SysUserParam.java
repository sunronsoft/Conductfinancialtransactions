package com.rzt.cft.param.sys;

import com.rzt.cft.annotation.MpEQ;
import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class SysUserParam extends PageParam {

    @ApiModelProperty(value = "手机号")
    @MpLike
    private String phone;

    @ApiModelProperty(value = "用户名")
    @MpLike
    private String userName;

    @ApiModelProperty(value = "状态")
    @MpEQ
    private String accountStatus;
}
