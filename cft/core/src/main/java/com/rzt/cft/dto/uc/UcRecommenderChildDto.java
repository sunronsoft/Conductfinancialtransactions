package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UcRecommenderChildDto {

    @ApiModelProperty("推荐人")
    private UcChildDto recommenderDto;

    @ApiModelProperty("伞下")
    private List<UcChildDto> childDto;
}

