package com.rzt.cft.service.sys.impl;

import com.rzt.cft.entity.sys.SysConfig;
import com.rzt.cft.mapper.sys.SysConfigMapper;
import com.rzt.cft.service.CoreServiceImpl;
import com.rzt.cft.service.sys.ISysConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配置表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Service
public class SysConfigServiceImpl extends CoreServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

}
