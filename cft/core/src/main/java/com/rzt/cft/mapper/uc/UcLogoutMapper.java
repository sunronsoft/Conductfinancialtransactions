package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcLogout;

/**
 * <p>
 * 用户-退出审核表单 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
public interface UcLogoutMapper extends BaseMapper<UcLogout> {

}
