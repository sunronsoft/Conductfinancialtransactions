package com.rzt.cft.service.sys;

import com.rzt.cft.entity.sys.SysConfig;
import com.rzt.cft.service.ICoreService;

/**
 * <p>
 * 配置表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysConfigService extends ICoreService<SysConfig> {

}
