package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcAssetStatistics;
import com.rzt.cft.param.uc.UcAssetStatisticsParam;

/**
 * <p>
 * 用户-资产信息统计 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-15
 */
public interface IUcAssetStatisticsService extends IService<UcAssetStatistics> {

    /**
     * 更新个人资产累计统计信息
     * @param param
     * @return
     */
    Boolean addAssetStatistics(UcAssetStatisticsParam param);
}
