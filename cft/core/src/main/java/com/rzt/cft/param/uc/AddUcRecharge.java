package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-充值表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class AddUcRecharge {

    @ApiModelProperty("金额")
    private BigDecimal amount;

    @ApiModelProperty("图片")
    private String picture;

}
