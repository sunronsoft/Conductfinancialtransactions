package com.rzt.cft.service.cms.impl;

import com.rzt.cft.service.cms.ICmsBannerService;
import com.rzt.cft.entity.cms.CmsBanner;
import com.rzt.cft.mapper.cms.CmsBannerMapper;
import com.rzt.cft.service.CoreServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营-banner 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class CmsBannerServiceImpl extends CoreServiceImpl<CmsBannerMapper, CmsBanner> implements ICmsBannerService {

}
