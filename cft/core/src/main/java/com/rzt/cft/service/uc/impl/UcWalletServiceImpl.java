package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcWallet;
import com.rzt.cft.mapper.uc.UcWalletMapper;
import com.rzt.cft.service.uc.IUcWalletService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-钱包地址 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Service
public class UcWalletServiceImpl extends ServiceImpl<UcWalletMapper, UcWallet> implements IUcWalletService {

}
