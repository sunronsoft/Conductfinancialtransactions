package com.rzt.cft.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.goods.GoodsOrder;

/**
 * <p>
 * 商品-订单表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface GoodsOrderMapper extends BaseMapper<GoodsOrder> {

}
