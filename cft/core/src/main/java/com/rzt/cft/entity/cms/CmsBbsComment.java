package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营-论坛评论
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsBbsComment extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 标题
     */
    private String postId;

    /**
     * 评论用户标识
     */
    private String userId;

    /**
     * 用户昵称（冗余）
     */
    private String userName;

    /**
     * 评论用户头像(冗余）
     */
    private String headPortrait;

    /**
     * 内容
     */
    private String content;

    /**
     * 评论楼数
     */
    private Integer num;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * @评论的标识
     */
    private String parentId;

    /**
     * 描述
     */
    private String remark;


}
