package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcReceivingAddress;
import com.rzt.cft.mapper.uc.UcReceivingAddressMapper;
import com.rzt.cft.service.uc.IUcReceivingAddressService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-收货地址列表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcReceivingAddressServiceImpl extends ServiceImpl<UcReceivingAddressMapper, UcReceivingAddress> implements IUcReceivingAddressService {

}
