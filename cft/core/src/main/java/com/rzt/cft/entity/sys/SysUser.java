package com.rzt.cft.entity.sys;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-后台用户
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUser extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 是否超管
     */
    private Boolean isSuper;

    /**
     * 登录名
     */
    private String userName;

    /**
     * 登录密码
     */
    private String userPwd;

    /**
     * 密码盐
     */
    private String salt;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 状态("离职","注销","正常")
     */
    private String status;

    /**
     * 头像图片
     */
    private String headPortrait;


}
