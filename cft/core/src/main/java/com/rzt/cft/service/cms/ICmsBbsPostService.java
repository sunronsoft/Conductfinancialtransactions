package com.rzt.cft.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.cms.CmsBbsPost;

/**
 * <p>
 * 运营-论坛帖子 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface ICmsBbsPostService extends IService<CmsBbsPost> {

}
