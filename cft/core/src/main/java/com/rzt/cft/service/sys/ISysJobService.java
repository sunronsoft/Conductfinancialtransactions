package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.dto.sys.SysRoleDto;
import com.rzt.cft.entity.sys.SysJob;
import com.rzt.cft.entity.sys.SysJobRole;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 系统-岗位 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysJobService extends IService<SysJob> {

    List<SysRoleDto> jobRoleList(String jobId);

    Boolean allotJobRole(String jobId, Collection<SysJobRole> itemList);

}
