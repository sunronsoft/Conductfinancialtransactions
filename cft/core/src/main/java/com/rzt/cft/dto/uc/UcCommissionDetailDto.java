package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 佣金详情
 * </p>
 *
 */
@Data
public class UcCommissionDetailDto {

    @ApiModelProperty("该伞下总积分收益")
    private BigDecimal totalIntegral;

    @ApiModelProperty("该伞下总金额收益")
    private BigDecimal totalAmt;

    @ApiModelProperty("被分销人code")
    private String distributorCode;

    @ApiModelProperty("被分销人昵称")
    private String distributorName;

    @ApiModelProperty("收益列表")
    private List<UcDistributionLogDto> distributionLogDtoList;
}
