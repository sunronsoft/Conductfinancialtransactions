package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 用户-收货地址列表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class AddUcReceivingAddress {

    @ApiModelProperty("id 修改时必传")
    private String id;

    @ApiModelProperty("联系人")
    private String name;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("详细地址")
    private String detail;

    @ApiModelProperty("是否默认")
    private Boolean isDefault;

}
