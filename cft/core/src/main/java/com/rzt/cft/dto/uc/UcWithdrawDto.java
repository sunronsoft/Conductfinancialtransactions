package com.rzt.cft.dto.uc;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户-提现表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcWithdrawDto extends AbstractBaseDto {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户名字")
    private String userName;

    @ApiModelProperty("金额")
    private BigDecimal amount;

    @ApiModelProperty("审核状态（0 待审核，1已通过，2已拒绝）")
    private String status;

    @ApiModelProperty("审核内容")
    private String remarks;

    @ApiModelProperty("钱包地址id")
    private String walletId;

    @ApiModelProperty("钱包url")
    private String walletUrl;

    @ApiModelProperty("钱包二维码")
    private String qrCode;

    @ApiModelProperty("流订单水号")
    private String txnSsn;

    @ApiModelProperty("审核时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime auditTime;

}
