package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-积分记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcIntegral extends AbstractBase {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名字
     */
    private String userName;

    /**
     * 变动类型
     */
    private String type;

    /**
     * 变动积分
     */
    private BigDecimal changeIntegral;

    /**
     * 变动后积分
     */
    private BigDecimal integral;

    /**
     * 是否最新
     */
    private Boolean current;

    /**
     * 备注
     */
    private String remarks;


}
