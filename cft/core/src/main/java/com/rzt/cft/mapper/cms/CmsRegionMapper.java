package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsRegion;

/**
 * <p>
 * 基础-地区 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface CmsRegionMapper extends BaseMapper<CmsRegion> {

}
