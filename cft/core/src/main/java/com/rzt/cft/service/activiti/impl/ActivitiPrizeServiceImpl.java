package com.rzt.cft.service.activiti.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.activiti.ActivitiPrize;
import com.rzt.cft.mapper.activiti.ActivitiPrizeMapper;
import com.rzt.cft.service.activiti.IActivitiPrizeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抽奖-奖品设置 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class ActivitiPrizeServiceImpl extends ServiceImpl<ActivitiPrizeMapper, ActivitiPrize> implements IActivitiPrizeService {

}
