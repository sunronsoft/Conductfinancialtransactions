package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class SetHeadParam {

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("头像")
    private String headPortrait;

    @ApiModelProperty("性别（1男，2女）")
    private Integer sex;

}

