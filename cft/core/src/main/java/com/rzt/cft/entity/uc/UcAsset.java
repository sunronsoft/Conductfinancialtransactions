package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-资产信息
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcAsset extends AbstractBase {

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 用户标识
     */
    private String userId;

    /**
     * 账户余额
     */
    private BigDecimal usable;

    /**
     * 注资金额
     */
    private BigDecimal investment;

    /**
     * 冻结金额
     */
    private BigDecimal freeze;

    /**
     * 可用积分
     */
    private BigDecimal integral;

    /**
     * 变动类型
     */
    private String changeType;

    /**
     * 交易记录id
     */
    private String capitalLogId;

    /**
     * 是否当前
     */
    private Boolean current;


}
