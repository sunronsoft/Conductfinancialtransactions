package com.rzt.cft.service.cms.impl;

import com.rzt.cft.entity.cms.CmsTypeItem;
import com.rzt.cft.mapper.cms.CmsTypeItemMapper;
import com.rzt.cft.service.CoreServiceImpl;
import com.rzt.cft.service.cms.ICmsTypeItemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 基础-字典类型项 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class CmsTypeItemServiceImpl extends CoreServiceImpl<CmsTypeItemMapper, CmsTypeItem> implements ICmsTypeItemService {

}
