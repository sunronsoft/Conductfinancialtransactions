package com.rzt.cft.service.project;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.dto.ResponseData;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.project.ProjectDetail;
import com.rzt.cft.param.project.AddProjectDetailParam;
import com.rzt.cft.param.uc.ProjectInjection;

/**
 * <p>
 * 项目-详情表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IProjectDetailService extends IService<ProjectDetail> {

    //创建项目
    Boolean add(AddProjectDetailParam projectDetail);

    //修改项目
    Boolean update(AddProjectDetailParam projectDetail);

    //项目注资
    ResponseData<String> capitalInjection(ProjectInjection projectInjection, UcUserDto ucUserDto, ProjectDetail projectDetail);
}
