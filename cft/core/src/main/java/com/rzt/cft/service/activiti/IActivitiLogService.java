package com.rzt.cft.service.activiti;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.activiti.ActivitiLog;

/**
 * <p>
 * 抽奖-抽奖记录 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IActivitiLogService extends IService<ActivitiLog> {

}
