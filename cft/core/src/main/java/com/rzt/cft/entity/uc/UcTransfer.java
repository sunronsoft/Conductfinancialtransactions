package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-转账记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcTransfer extends AbstractBase {

    /**
     * 流水号
     */
    private String txnSsn;

    /**
     * 出账用户标识
     */
    private String outUserId;

    /**
     * 入账用户标识
     */
    private String inUserId;

    /**
     * 转账金额
     */
    private BigDecimal amt;

    /**
     * 转账积分
     */
    private BigDecimal integral;

    /**
     * 类型
     */
    private String type;

    /**
     * 备注
     */
    private String remarks;


}
