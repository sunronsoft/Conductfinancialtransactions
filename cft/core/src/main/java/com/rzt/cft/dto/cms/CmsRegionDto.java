package com.rzt.cft.dto.cms;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rzt.cft.dto.AbstractChildrenDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 基础-地区
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsRegionDto extends AbstractChildrenDto {

    /**
     * 创建人
     */
    @JsonIgnore
    private String createId;

    /**
     * 更新人
     */
    @JsonIgnore
    private String updateId;

    /**
     * 标题
     */
    @ApiModelProperty("名字")
    private String title;

    /**
     * 简化序列号
     */
    @ApiModelProperty("简化序列号")
    private String simplify;

    /**
     * 等级
     */
    @ApiModelProperty("等级")
    private Integer level;

    /**
     * 后缀
     */
    @ApiModelProperty("后缀")
    private String suffix;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
