package com.rzt.cft.service.uc.impl;

import com.rzt.cft.entity.uc.UcAccount;
import com.rzt.cft.mapper.uc.UcAccountMapper;
import com.rzt.cft.service.CoreServiceImpl;
import com.rzt.cft.service.uc.IUcAccountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-账号表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class UcAccountServiceImpl extends CoreServiceImpl<UcAccountMapper, UcAccount> implements IUcAccountService {

}
