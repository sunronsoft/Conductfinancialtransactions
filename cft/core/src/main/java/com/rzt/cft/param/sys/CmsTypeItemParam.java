package com.rzt.cft.param.sys;

import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/10 15:38
 */
@Data
public class CmsTypeItemParam {

    private String id;

    /**
     * 类型标识
     */
    private String typeId;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subhead;

    /**
     * 简化序列号
     */
    private String simplify;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 图片
     */
    private String imgLink;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * 备注
     */
    private String remark;
}
