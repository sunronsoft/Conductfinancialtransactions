package com.rzt.cft.dto.project;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目-详情表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProjectDetailDto extends AbstractBaseDto {

    @ApiModelProperty("项目名称")
    private String name;

    @ApiModelProperty("项目编码")
    private String projectSn;

    @ApiModelProperty("副标题")
    private String subheading;

    @ApiModelProperty("项目类型(optional 自选； intelligence 智能)")
    private String type;

    @ApiModelProperty("项目总金额")
    private BigDecimal total;

    @ApiModelProperty("每日返利比")
    private BigDecimal dailyInverse;

    @ApiModelProperty("虚拟进度")
    private BigDecimal virtualProgress;

    @ApiModelProperty("真实进度")
    private BigDecimal realProgress;

    @ApiModelProperty("半年利率")
    private BigDecimal halfYearRate;

    @ApiModelProperty("年利率")
    private BigDecimal yearRate;

    @ApiModelProperty("日现金比例")
    private BigDecimal cashRatio;

    @ApiModelProperty("日积分比例")
    private BigDecimal integralRatio;

    @ApiModelProperty("可购买等级")
    private String purchasableGrade;

    @ApiModelProperty("起购金额")
    private BigDecimal purchaseAmount;

    @ApiModelProperty("返利方式类型")
    private String rebateType;

    @ApiModelProperty("产品介绍")
    private String introduce;

    @ApiModelProperty("是否开启虚拟进度")
    private Boolean isVirtual;

    @ApiModelProperty("是否精选")
    private Boolean isRecommend;

    @ApiModelProperty("可认购金额")
    private String subscriptionAmount;

    @ApiModelProperty("项目进度开关（1显示真实进度、2显示虚拟进度、3虚拟加真实）")
    private Integer progressSwitch;

    @ApiModelProperty("结束时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime endTime;

    @ApiModelProperty("项目状态(0:进行中；1:已完成；2:已结束)")
    private String status;

    @ApiModelProperty("剩余可注入金额")
    private BigDecimal surplus;

}
