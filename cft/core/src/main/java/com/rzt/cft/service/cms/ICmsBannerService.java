package com.rzt.cft.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.cms.CmsBanner;

/**
 * <p>
 * 运营-banner 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface ICmsBannerService extends IService<CmsBanner> {

}
