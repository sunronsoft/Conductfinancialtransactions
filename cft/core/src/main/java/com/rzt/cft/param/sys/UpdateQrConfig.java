package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 配置表
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
public class UpdateQrConfig {

    @ApiModelProperty(value = "配置值")
    @NotNull(message = "配置值不能为空")
    private String configValue;

    @ApiModelProperty(value = "配置key")
    @NotNull(message = "配置key不能为空")
    private String configKey;

}
