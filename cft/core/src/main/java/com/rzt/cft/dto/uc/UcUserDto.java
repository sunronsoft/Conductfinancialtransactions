package com.rzt.cft.dto.uc;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UcUserDto {

    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createAt;

    @ApiModelProperty("用户编码（用户id，用户推广码）")
    private String code;

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("账号状态（0,启用、1禁用、2冻结、3注销、4待注销）")
    private Integer accountStatus;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("头像")
    private String headPortrait;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("用户类型")
    private String type;

    @ApiModelProperty("name")
    private String name;

    @ApiModelProperty("推荐人编码")
    private String parentCode;

    @ApiModelProperty("分销级别id")
    private String distributionId;

    @ApiModelProperty("分销级别名称")
    private String distributionName;

    @ApiModelProperty("推荐人Id")
    private String parentId;

    @ApiModelProperty("注资金额")
    private BigDecimal investment;

    @ApiModelProperty("账户余额")
    private BigDecimal usable;

    @ApiModelProperty("可用积分")
    private BigDecimal integral;

    @ApiModelProperty("性别（1男，2女）")
    private Integer sex;

}

