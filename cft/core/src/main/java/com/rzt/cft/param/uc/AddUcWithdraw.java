package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-提现表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class AddUcWithdraw {

    @ApiModelProperty("金额")
    private BigDecimal amount;

    @ApiModelProperty("钱包地址id")
    private String walletId;

    @ApiModelProperty("密码")
    private String pwd;

}
