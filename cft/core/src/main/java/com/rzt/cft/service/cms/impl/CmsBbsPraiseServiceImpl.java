package com.rzt.cft.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.cms.CmsBbsPraise;
import com.rzt.cft.mapper.cms.CmsBbsPraiseMapper;
import com.rzt.cft.service.cms.ICmsBbsPraiseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营-论坛点赞、踩记录 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class CmsBbsPraiseServiceImpl extends ServiceImpl<CmsBbsPraiseMapper, CmsBbsPraise> implements ICmsBbsPraiseService {

}
