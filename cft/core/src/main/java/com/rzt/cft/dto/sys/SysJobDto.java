package com.rzt.cft.dto.sys;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/3 15:50
 */
@Data
public class SysJobDto extends AbstractBaseDto {

    /**
     * 机构标识
     */
    @ApiModelProperty(value = "机构id")
    private String organizationId;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 是否管理者
     */
    @ApiModelProperty(value = "是否管理者")
    private Boolean isSuper;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
     * 是否启用
     */
    @ApiModelProperty(value = "是否启用")
    private Boolean isUse;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String remark;
}
