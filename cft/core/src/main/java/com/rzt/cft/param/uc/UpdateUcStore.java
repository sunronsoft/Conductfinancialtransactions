package com.rzt.cft.param.uc;

import com.rzt.cft.constant.RegexConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UpdateUcStore {

    @ApiModelProperty("id")
    @NotNull(message = "id不能为空")
    private String id;

    @ApiModelProperty("门头照片")
    private String logo;

    @ApiModelProperty("房产公司id")
    private String enterpriseId;

    @ApiModelProperty("店主手机号")
    @Pattern(regexp = RegexConstant.PHONE_REGEX, message = "手机号格式不正确")
    private String shopownerPhone;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("门店名称")
    private String name;

    @ApiModelProperty("店主姓名")
    private String shopownerName;

    @ApiModelProperty("营业执照号")
    private String license;

    @ApiModelProperty("服务范围")
    private String serviceScope;

}

