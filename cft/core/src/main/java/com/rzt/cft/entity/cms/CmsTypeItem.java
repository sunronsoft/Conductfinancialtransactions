package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 基础-字典类型项
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsTypeItem extends AbstractBase {
    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;
    /**
     * 类型标识
     */
    private String typeId;

    /**
     * 类型简化序列号
     */
    private String typeSimplify;

    /**
     * 父级标识
     */
    private String parentId;

    /**
     * 父级简化序列号
     */
    private String parentSimplify;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subhead;

    /**
     * 简化序列号
     */
    private String simplify;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * 图片
     */
    private String imgLink;

    private String extend;

    /**
     * 备注
     */
    private String remark;


}
