package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 邀请人
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class InviteesParam {

    @ApiModelProperty("邀请人码")
    private String parentCode;

}
