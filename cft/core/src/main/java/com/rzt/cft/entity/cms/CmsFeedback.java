package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营-问题反馈
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsFeedback extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 反馈内容
     */
    private String content;

    /**
     * 反馈人
     */
    private String userId;

    /**
     * 反馈人昵称
     */
    private String userName;

    /**
     * 是否处理
     */
    private Boolean isHandle;


}
