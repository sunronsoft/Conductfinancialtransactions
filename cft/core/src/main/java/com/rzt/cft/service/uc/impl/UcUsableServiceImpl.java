package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcUsable;
import com.rzt.cft.mapper.uc.UcUsableMapper;
import com.rzt.cft.service.uc.IUcUsableService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-积分记录 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcUsableServiceImpl extends ServiceImpl<UcUsableMapper, UcUsable> implements IUcUsableService {

}
