package com.rzt.cft.entity.uc;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-钱包地址
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcWallet extends AbstractBase {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 钱包地址名称
     */
    private String name;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 地址
     */
    private String url;

    /**
     * 二维码
     */
    private String qrCode;


}
