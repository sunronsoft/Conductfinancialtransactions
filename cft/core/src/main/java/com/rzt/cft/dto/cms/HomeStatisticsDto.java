package com.rzt.cft.dto.cms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营-问题反馈
 *
 * @author zhongzhong
 * @since 2020-03-7
 */
@Data
public class HomeStatisticsDto {

    @ApiModelProperty("充值待审核")
    private Integer waitExamineRecharge;

    @ApiModelProperty("提现待审核")
    private Integer waitExamineWithdraw;

    @ApiModelProperty("自选项目")
    private Integer optionalCount;

    @ApiModelProperty("智能项目")
    private Integer intelligenceCounr;

    @ApiModelProperty("今天新增")
    private Integer todayNew;

    @ApiModelProperty("昨天新增")
    private Integer yesterdayNew;

    @ApiModelProperty("本月新增")
    private Integer monthNew;

    @ApiModelProperty("会员总是")
    private Integer total;

}
