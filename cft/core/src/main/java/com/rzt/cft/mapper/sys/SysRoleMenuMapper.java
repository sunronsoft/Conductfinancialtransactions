package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysRoleMenu;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * 系统-后台角色功能 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Component
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    List<Integer> listMenuIdByRoleId(String roleId);

}
