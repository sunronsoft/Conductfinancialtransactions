package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.sys.SysAgreement;

/**
 * <p>
 * 系统协议配置 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-17
 */
public interface ISysAgreementService extends IService<SysAgreement> {

}
