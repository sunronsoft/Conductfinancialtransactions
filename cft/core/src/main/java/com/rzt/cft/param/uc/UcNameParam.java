package com.rzt.cft.param.uc;

import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class UcNameParam extends PageParam {

    @ApiModelProperty(value = "名字")
    @MpLike
    private String name;

}
