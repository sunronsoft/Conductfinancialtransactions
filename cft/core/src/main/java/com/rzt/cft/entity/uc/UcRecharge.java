package com.rzt.cft.entity.uc;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-充值表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcRecharge extends AbstractBase {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名字
     */
    private String userName;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 审核状态（0 待审核，1已通过，2已拒绝）
     */
    private String status;

    /**
     * 图片地址
     */
    private String picture;

    /**
     * 审核内容
     */
    private String remarks;

    /**
     * 订单流水号
     */
    private String txnSsn;

    /**
     * 审核时间
     */
    private LocalDateTime auditTime;


}
