package com.rzt.cft.param.uc;

import com.rzt.cft.entity.uc.UcUser;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 邀请人
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class ChildCountParam {

    private int count;

    private List<UcUser> ucUserList;

}
