package com.rzt.cft.service.project.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.service.project.IProjectStatisticsService;
import com.rzt.cft.entity.project.ProjectStatistics;
import com.rzt.cft.mapper.project.ProjectStatisticsMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目-统计 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Service
public class ProjectStatisticsServiceImpl extends ServiceImpl<ProjectStatisticsMapper, ProjectStatistics> implements IProjectStatisticsService {

}
