package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class AddUcSetConfigure {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("套餐id")
    @NotNull(message = "套餐id不能为空")
    private String setId;

    @ApiModelProperty("增值服务名称")
    @NotNull(message = "增值服务名称不能为空")
    private String name;

    @ApiModelProperty("服务数量")
    @NotNull(message = "服务数量不能为空")
    private Integer num;

    @ApiModelProperty("类型")
    @NotNull(message = "类型不能为空")
    private String type;

    @ApiModelProperty("描述")
    private String remarks;

}

