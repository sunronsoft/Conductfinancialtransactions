package com.rzt.cft.dto.sys;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/3 11:57
 */
@Data
public class SysMenuAllDto extends AbstractBaseDto {

    /**
     * 标题
     */
    @ApiModelProperty("标题")
    private String title;

    /**
     * 副标题
     */
    @ApiModelProperty("副标题")
    private String subhead;

    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String url;

    /**
     * 图标
     */
    @ApiModelProperty("图标")
    private String ico;

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    @ApiModelProperty("菜单类型（M目录 C菜单 F按钮）")
    private String menuType;

    /**
     * 等级
     */
    @ApiModelProperty("等级")
    private Integer level;

    /**
     * 父级标识
     */
    @ApiModelProperty("父级标识")
    private String parentId;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer sort;

    /**
     * 是否显示
     */
    @ApiModelProperty("是否显示")
    private Boolean isShow;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    private String remark;
}
