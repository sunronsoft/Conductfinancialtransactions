package com.rzt.cft.param.uc;

import com.rzt.cft.constant.RegexConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class AddUcUser {

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("手机号")
    @Pattern(regexp = RegexConstant.PHONE_REGEX, message = "手机号格式不正确")
    private String phone;

    @ApiModelProperty("头像")
    private String headPortrait;

    @ApiModelProperty("邮箱")
    private String email;

}

