package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 用户关注表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
public class UcUserFollowDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("经纪人id")
    private String followUserId;

    @ApiModelProperty("经纪人名称")
    private String followUseName;

    @ApiModelProperty("经纪人头像")
    private String followUseHead;

    @ApiModelProperty("经纪人电话")
    private String followUsePhone;

    @ApiModelProperty("门店名称")
    private String followStoreName;


}
