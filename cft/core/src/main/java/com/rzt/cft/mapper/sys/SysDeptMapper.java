package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysDept;

/**
 * <p>
 * 系统-后台机构 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
