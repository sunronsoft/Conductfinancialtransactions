package com.rzt.cft.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.sys.SysUserJob;
import com.rzt.cft.mapper.sys.SysUserJobMapper;
import com.rzt.cft.service.sys.ISysUserJobService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统-后台用户岗位 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Service
public class SysUserJobServiceImpl extends ServiceImpl<SysUserJobMapper, SysUserJob> implements ISysUserJobService {

}
