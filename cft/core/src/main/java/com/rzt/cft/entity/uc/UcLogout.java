package com.rzt.cft.entity.uc;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-退出审核表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcLogout extends AbstractBase {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 用户名字
     */
    private String userName;

    /**
     * 项目类型
     */
    private String projectType;

    /**
     * 订单编号
     */
    private String txnSsn;

    /**
     * 退出金额
     */
    private BigDecimal amount;

    /**
     * 申请时间
     */
    private LocalDateTime applyTime;

    /**
     * 审核状态（0 待审核，1已通过，2已拒绝）
     */
    private String status;

    /**
     * 审核内容
     */
    private String remarks;

    /**
     * 退出积分
     */
    private BigDecimal integral;

    /**
     * 退出本金
     */
    private BigDecimal investmentAmount;

    /**
     * 审核时间
     */
    private LocalDateTime auditTime;


}
