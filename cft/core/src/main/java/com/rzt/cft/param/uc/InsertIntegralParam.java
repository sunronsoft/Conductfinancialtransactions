package com.rzt.cft.param.uc;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 钟忠
 * @date 2020/3/12 15:11
 */
@Data
public class InsertIntegralParam {

    //变动类型
    private String type;

    //用户id
    private String userId;

    //用户名
    private String userName;

    //变动积分
    private BigDecimal integral;

    //备注
    private String remarks;

}
