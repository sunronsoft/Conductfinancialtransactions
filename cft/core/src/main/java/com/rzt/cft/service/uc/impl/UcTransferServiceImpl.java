package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.constant.TypeConstant;
import com.rzt.cft.utils.CustomUtil;
import com.rzt.cft.dto.uc.UcUserDto;
import com.rzt.cft.entity.uc.UcTransfer;
import com.rzt.cft.mapper.uc.UcTransferMapper;
import com.rzt.cft.param.uc.AddTransferParam;
import com.rzt.cft.param.uc.InsertIntegralParam;
import com.rzt.cft.param.uc.InsertUsableParam;
import com.rzt.cft.service.uc.IUcAssetService;
import com.rzt.cft.service.uc.IUcTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-转账记录 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcTransferServiceImpl extends ServiceImpl<UcTransferMapper, UcTransfer> implements IUcTransferService {

    @Autowired
    private IUcAssetService ucAssetService;

    @Override
    @Transactional
    public Boolean transfer(UcUserDto ucUserDto, AddTransferParam param) {
        UcTransfer ucTransfer = new UcTransfer();
        ucTransfer.setTxnSsn(CustomUtil.getSsn());
        ucTransfer.setAmt(param.getAmt());
        ucTransfer.setIntegral(param.getIntegral());
        ucTransfer.setInUserId(param.getInUserId());
        ucTransfer.setOutUserId(ucUserDto.getId());
        int count = baseMapper.insert(ucTransfer);

        if(param.getAmt() != null && param.getAmt().compareTo(BigDecimal.ZERO) > 0) {
            //出账
            InsertUsableParam outUsableParam = new InsertUsableParam();
            outUsableParam.setUserId(ucUserDto.getId());
            outUsableParam.setType(TypeConstant.usableChangeType.BALANCE_TURN_OUT);
            outUsableParam.setUsable(param.getAmt().negate());
            outUsableParam.setRemarks(ucTransfer.getId());
            ucAssetService.insertUsable(outUsableParam);
            //入账
            InsertUsableParam inUsableParam = new InsertUsableParam();
            inUsableParam.setUserId(param.getInUserId());
            inUsableParam.setType(TypeConstant.usableChangeType.BALANCE_TURN_IN);
            inUsableParam.setUsable(param.getAmt());
            inUsableParam.setRemarks(ucTransfer.getId());
            ucAssetService.insertUsable(inUsableParam);
        }

        if(param.getIntegral() != null && param.getIntegral().compareTo(BigDecimal.ZERO) > 0) {
            //出账
            InsertIntegralParam outIntegralParam = new InsertIntegralParam();
            outIntegralParam.setUserId(ucUserDto.getId());
            outIntegralParam.setType(TypeConstant.integralChangeType.INTEGRAL_TURN_OUT);
            outIntegralParam.setIntegral(param.getIntegral().negate());
            outIntegralParam.setRemarks(ucTransfer.getId());
            ucAssetService.insertIntegral(outIntegralParam);

            //入账
            InsertIntegralParam inIntegralParam = new InsertIntegralParam();
            inIntegralParam.setUserId(param.getInUserId());
            inIntegralParam.setType(TypeConstant.integralChangeType.INTEGRAL_TURN_IN);
            inIntegralParam.setIntegral(param.getIntegral());
            inIntegralParam.setRemarks(ucTransfer.getId());
            ucAssetService.insertIntegral(inIntegralParam);
        }

        return retBool(count);
    }
}
