package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysJob;
import com.rzt.cft.entity.sys.SysRole;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * 系统-岗位 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Component
public interface SysJobMapper extends BaseMapper<SysJob> {

    List<SysRole> jobRoleList(String jobId);
}
