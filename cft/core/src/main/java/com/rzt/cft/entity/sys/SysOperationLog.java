package com.rzt.cft.entity.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* <p>
    * 系统-操作日志
    * </p>
*
* @author zhongzhong
* @since 2019-12-07
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_operation_log")
public class SysOperationLog extends AbstractBase {

            /**
            * 操作人名字
            */
    private String createName;

            /**
            * 操作url
            */
    private String url;

            /**
            * 操作类型
            */
    private String methodType;

            /**
            * 操作描述
            */
    private String methodDescribe;

            /**
            * 入参（超长做字符串截取）
            */
    private String requestParam;

            /**
            * 出参（超长做字符串截取）
            */
    private String responParam;

            /**
            * 请求内部返回码
            */
    private String responCode;

            /**
            * 备注
            */
    private String remark;

    /**
     * 备注
     */
    private String ip;

    /**
     * 用时
     */
    private Integer useTime;


}
