package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class ExamineParam {

    @ApiModelProperty(value = "需要审核的id")
    private String id;

    @ApiModelProperty(value = "审核内容")
    private String remarks;

    @ApiModelProperty(value = "审核状态（0 待审核，1通过，2拒绝）")
    private String status;

}
