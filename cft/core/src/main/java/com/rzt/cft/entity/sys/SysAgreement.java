package com.rzt.cft.entity.sys;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统协议配置
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysAgreement extends AbstractBase {

    /**
     * 协议名称
     */
    private String name;

    /**
     * 协议key
     */
    private String configKey;

    /**
     * 协议内容
     */
    private String configValue;

    /**
     * 描述
     */
    private String remark;

    /**
     * rich_text富文本，text文本框
     */
    private String type;


}
