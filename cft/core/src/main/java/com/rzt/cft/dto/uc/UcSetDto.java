package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 经纪人套餐
 * </p>
 *
 * @author zhongzhong
 * @since 2020-02-10
 */
@Data
public class UcSetDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("套餐名称")
    private String name;

    @ApiModelProperty("介绍")
    private String introduce;

    @ApiModelProperty("套餐类型(会员套餐、畅聊套餐）")
    private String type;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("备注")
    private String remarks;

}
