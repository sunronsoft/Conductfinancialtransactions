package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class AddDevelopers {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("企业名称")
    @NotNull(message = "企业名称不能为空")
    private String name;

    @ApiModelProperty("企业logo")
    private String logo;

    @ApiModelProperty("企业类型")
    private String enterpriseType;

}

