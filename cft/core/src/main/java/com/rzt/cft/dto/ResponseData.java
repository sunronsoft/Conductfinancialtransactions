package com.rzt.cft.dto;

/**
 * @author:zz
 * @description(描述): 接口统一返回数据
 * @date 2019/12/12
 **/

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rzt.cft.constant.ServiceStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询列表结果
 */
@Data
@ApiModel(value = "响应类")
public class ResponseData<T> {

    @ApiModelProperty(name = "code", value = "响应码")
    @JsonProperty("code")
    private Integer code;

    @ApiModelProperty(name = "message", value = "响应码描述")
    @JsonProperty("message")
    private String message;

    @ApiModelProperty(name = "data", value = "数据体")
    @JsonProperty("data")
    private T data;

    /**
     * 返回成功结果集
     */
    public ResponseData() {
        this.code = ServiceStatus.SUCCESS.getCode();
        this.message = ServiceStatus.SUCCESS.getMessage();
    }

    /**
     * 返回成功结果集
     * @param data
     */
    public ResponseData(T data) {
        this.code = ServiceStatus.SUCCESS.getCode();
        this.message = ServiceStatus.SUCCESS.getMessage();
        this.data = data;
    }

    /**
     * 返回响应码  不含结果集
     * @param serviceStatus
     */
    public ResponseData(ServiceStatus serviceStatus){
        this.code = serviceStatus.getCode();
        this.message = serviceStatus.getMessage();
    }

    /**
     * 指定响应码  返回结果集
     * @param data
     * @param serviceStatus
     */
    public ResponseData(T data, ServiceStatus serviceStatus){
        this.code = serviceStatus.getCode();
        this.message = serviceStatus.getMessage();
        this.data = data;
    }

    /**
     * 指定响应码  返回结果集
     * @param data
     * @param serviceStatus
     */
    public ResponseData(ServiceStatus serviceStatus, T data){
        this.code = serviceStatus.getCode();
        this.message = serviceStatus.getMessage();
        this.data = data;
    }

    /**
     * 返回指定错误信息
     * @param message
     * @param serviceStatus
     */
    public ResponseData(ServiceStatus serviceStatus, String message){
        this.code = serviceStatus.getCode();
        this.message = message;
    }

    /**
     * 系统错误
     * @return
     */
    public static ResponseData error() {
        return new ResponseData(ServiceStatus.SERVICE_EXCEPTION);
    }

    /**
     * 成功
     * @return
     */
    public static ResponseData ok() {
        return new ResponseData(ServiceStatus.SUCCESS);
    }

    /**
     * 成功
     * @return
     */
    public static <T> ResponseData ok(T data) {
        return new ResponseData(data);
    }

    /**
     * 根据错误码返回错误
     * @return
     */
    public static ResponseData error(ServiceStatus serviceStatus) {
        return new ResponseData(serviceStatus);
    }

    /**
     * 返回错误描述
     * @return
     */
    public static ResponseData error(String message) {
        return new ResponseData(ServiceStatus.SERVICE_EXCEPTION, message);
    }
}
