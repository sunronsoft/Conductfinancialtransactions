package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysOperationLog;

/**
 * <p>
 * 系统-操作日志 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-07
 */

public interface SysOperationLogMapper extends BaseMapper<SysOperationLog> {

}
