package com.rzt.cft.entity.uc;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-收货地址列表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcReceivingAddress extends AbstractBase {

    /**
     * 用户编号
     */
    private String code;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 详细地址
     */
    private String detail;

    /**
     *  是否默认
     */
    private Boolean isDefault;

    /**
     * 地区id
     */
    private String regionId;

    /**
     * 地区名字
     */
    private String regionName;


}
