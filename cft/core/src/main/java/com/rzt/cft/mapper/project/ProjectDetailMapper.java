package com.rzt.cft.mapper.project;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.project.ProjectDetail;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 项目-详情表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Component
public interface ProjectDetailMapper extends BaseMapper<ProjectDetail> {

    Integer countNum();
}
