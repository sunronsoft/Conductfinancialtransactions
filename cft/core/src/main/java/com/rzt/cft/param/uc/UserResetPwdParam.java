package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
public class UserResetPwdParam {

    @ApiModelProperty(value = "用户标识")
    private Integer id;

    @ApiModelProperty(value = "新密码")
    private String newPwd;

}
