package com.rzt.cft.service.goods.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.goods.GoodsOrder;
import com.rzt.cft.mapper.goods.GoodsOrderMapper;
import com.rzt.cft.service.goods.IGoodsOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-订单表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class GoodsOrderServiceImpl extends ServiceImpl<GoodsOrderMapper, GoodsOrder> implements IGoodsOrderService {

}
