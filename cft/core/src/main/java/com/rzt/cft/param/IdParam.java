package com.rzt.cft.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
public class IdParam {

    @ApiModelProperty(value = "id")
    private String id;

}
