package com.rzt.cft.entity.sys;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 配置表
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysConfig extends AbstractBase {

    /**
     * 配置值
     */
    private String configValue;

    /**
     * 配置名称
     */
    private String name;

    /**
     * 配置key
     */
    private String configKey;

    /**
     * 描述
     */
    private String remark;


}
