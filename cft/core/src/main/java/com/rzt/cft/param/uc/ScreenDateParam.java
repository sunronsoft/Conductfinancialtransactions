package com.rzt.cft.param.uc;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class ScreenDateParam {

    @ApiModelProperty(value = "开始日期")
    @JsonFormat(pattern="yyy-MM-dd",timezone = "GMT+8")
    private LocalDate start;

    @ApiModelProperty(value = "结束日期")
    @JsonFormat(pattern="yyy-MM-dd",timezone = "GMT+8")
    private LocalDate end;

}
