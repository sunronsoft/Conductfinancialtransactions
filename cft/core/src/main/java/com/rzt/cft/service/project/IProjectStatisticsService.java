package com.rzt.cft.service.project;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.project.ProjectStatistics;

/**
 * <p>
 * 项目-统计 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
public interface IProjectStatisticsService extends IService<ProjectStatistics> {

}
