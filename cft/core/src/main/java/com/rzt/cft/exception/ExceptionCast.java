package com.rzt.cft.exception;


import com.rzt.cft.constant.ServiceStatus;

/**
 * 自定义抛出异常
 **/
public class ExceptionCast {

    public static void cast(ServiceStatus serviceStatus) {
        throw new CustomException(serviceStatus);
    }
}
