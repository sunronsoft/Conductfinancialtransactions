package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcRecharge;

/**
 * <p>
 * 用户-充值表单 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface UcRechargeMapper extends BaseMapper<UcRecharge> {

}
