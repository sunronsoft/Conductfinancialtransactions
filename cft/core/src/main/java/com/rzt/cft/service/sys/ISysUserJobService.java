package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.sys.SysUserJob;

/**
 * <p>
 * 系统-后台用户岗位 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysUserJobService extends IService<SysUserJob> {

}
