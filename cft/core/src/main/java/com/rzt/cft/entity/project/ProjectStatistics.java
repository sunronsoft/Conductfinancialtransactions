package com.rzt.cft.entity.project;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 项目-统计
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProjectStatistics extends AbstractBase {

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目编号
     */
    private String projectSn;

    /**
     * 项目状态
     */
    private String status;

    /**
     * 项目类型
     */
    private String type;

    /**
     * 总金额
     */
    private BigDecimal total;

    /**
     * 虚拟进度
     */
    private BigDecimal virtualProgress;

    /**
     * 真实进度
     */
    private BigDecimal realProgress;

    /**
     * 已注入金额
     */
    private BigDecimal raisedAmount;

    /**
     * 剩余可注入金额
     */
    private BigDecimal surplus;

    /**
     * 项目订单id
     */
    private String orderId;

    /**
     * 是否最新
     */
    private Boolean current;


}
