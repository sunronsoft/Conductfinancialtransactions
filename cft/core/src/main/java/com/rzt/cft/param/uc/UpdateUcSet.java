package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UpdateUcSet {

    @ApiModelProperty("id")
    @NotNull(message = "id不能为空")
    private String id;

    @ApiModelProperty("套餐名称")
    private String name;

    @ApiModelProperty("介绍")
    private String introduce;

    @ApiModelProperty("套餐类型(会员套餐、畅聊套餐）")
    private String type;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("备注")
    private String remarks;

}

