package com.rzt.cft.dto.sys;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 配置表
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
public class SysConfigDto extends AbstractBaseDto {

    @ApiModelProperty(value = "配置值")
    private String configValue;

    @ApiModelProperty(value = "配置名称")
    private String name;

    @ApiModelProperty(value = "配置key")
    private String configKey;

    @ApiModelProperty(value = "描述")
    private String remark;

}
