package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.sys.SysJobRole;

/**
 * <p>
 * 系统-岗位角色 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysJobRoleService extends IService<SysJobRole> {

}
