package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zz
 */
@Data
public class UserHeadPortraitParam {

    @ApiModelProperty(value = "头像图片")
    @NotNull(message = "头像图片不能为空")
    private String headPortrait;

}
