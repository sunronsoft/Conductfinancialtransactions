package com.rzt.cft.param.uc;

import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-资产信息统计
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-15
 */
@Data
public class UcAssetStatisticsParam {

    /**
     * 用户标识
     */
    private String userId;

    /**
     * 累计注资
     */
    private BigDecimal accumulationInjection;

    /**
     * 累计返利
     */
    private BigDecimal accumulationRebate;

    /**
     * 累加返利积分
     */
    private BigDecimal accumulationIntegral;


}
