package com.rzt.cft.dto.uc;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户-积分记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class UcUsableDetailDto extends UcUsableDto {

    @ApiModelProperty("注资时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime injectionTime;

    @ApiModelProperty("返利积分")
    private BigDecimal integral;

    @ApiModelProperty("充值凭证")
    private String picture;

    @ApiModelProperty("钱包地址")
    private String walletUrl;

    @ApiModelProperty("转账接受人code")
    private String inUserCode;

    @ApiModelProperty("转出人code")
    private String outUserCode;

    @ApiModelProperty("注资本金")
    private BigDecimal investmentAmount;

    @ApiModelProperty("伞下用户编码")
    private String childCode;

    @ApiModelProperty("伞下用户账号")
    private String childAccount;

    @ApiModelProperty("伞下用户昵称")
    private String childNickName;

    @ApiModelProperty("伞下用户用户id")
    private String childUserId;

}
