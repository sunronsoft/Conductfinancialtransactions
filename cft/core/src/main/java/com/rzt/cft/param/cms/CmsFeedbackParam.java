package com.rzt.cft.param.cms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 钟忠
 * @date 2020/2/7 10:19
 */
@Data
public class CmsFeedbackParam {

    @ApiModelProperty("反馈内容")
    @NotNull(message = "反馈内容不能为空")
    private String content;
}
