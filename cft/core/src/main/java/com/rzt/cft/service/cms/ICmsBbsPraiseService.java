package com.rzt.cft.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.cms.CmsBbsPraise;

/**
 * <p>
 * 运营-论坛点赞、踩记录 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface ICmsBbsPraiseService extends IService<CmsBbsPraise> {

}
