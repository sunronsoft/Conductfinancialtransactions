package com.rzt.cft.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.service.cms.ICmsFeedbackService;
import com.rzt.cft.entity.cms.CmsFeedback;
import com.rzt.cft.mapper.cms.CmsFeedbackMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营-问题反馈 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-06
 */
@Service
public class CmsFeedbackServiceImpl extends ServiceImpl<CmsFeedbackMapper, CmsFeedback> implements ICmsFeedbackService {

}
