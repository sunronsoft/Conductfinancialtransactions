package com.rzt.cft.param.uc;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 钟忠
 * @date 2020/3/12 15:11
 */
@Data
public class InsertUsableParam {

    //变动类型
    private String type;

    //用户id
    private String userId;

    //用户名
    private String userName;

    //余额
    private BigDecimal usable;

    //注资金额
    private BigDecimal investment;

    //冻结余额
    private BigDecimal freeze;

    //备注
    private String remarks;

}
