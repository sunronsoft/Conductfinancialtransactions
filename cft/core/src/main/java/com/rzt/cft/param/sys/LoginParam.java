package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/2 11:11
 */
@Data
public class LoginParam {

    @ApiModelProperty(value = "用户名或手机号")
    private String userName;

    @ApiModelProperty(value = "密码")
    private String passWord;

    @ApiModelProperty(value = "验证码")
    private String captcha;

}
