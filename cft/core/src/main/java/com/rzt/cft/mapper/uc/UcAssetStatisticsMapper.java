package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcAssetStatistics;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户-资产信息统计 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-15
 */
@Component
public interface UcAssetStatisticsMapper extends BaseMapper<UcAssetStatistics> {

}
