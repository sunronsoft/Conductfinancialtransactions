package com.rzt.cft.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.cms.CmsNotice;

/**
 * <p>
 * 配置表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface ICmsNoticeService extends IService<CmsNotice> {

}
