package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.project.ProjectOrder;
import com.rzt.cft.entity.uc.UcRebateLog;
import com.rzt.cft.entity.uc.UcUser;

import java.util.List;

/**
 * <p>
 * 用户-返利记录 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
public interface IUcRebateLogService extends IService<UcRebateLog> {

    /**
     * 智能返利
     * @param projectOrder
     * @return
     */
    Boolean intelligenceRebate(ProjectOrder projectOrder);

    /**
     * 自选返利
     * @param projectOrder
     * @return
     */
    Boolean optionalRebate(ProjectOrder projectOrder);

    /**
     * 查询所有下级分销人员
     * @param ucUserList
     * @param idList
     * @return
     */
    List<UcUser> childList(List<UcUser> ucUserList, List<String> idList);

}
