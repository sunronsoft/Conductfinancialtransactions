package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcBank;
import com.rzt.cft.mapper.uc.UcBankMapper;
import com.rzt.cft.service.uc.IUcBankService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-钱包地址 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class UcBankServiceImpl extends ServiceImpl<UcBankMapper, UcBank> implements IUcBankService {

}
