package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zz
 */
@Data
public class ModifyLevelParam {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "会员等级id")
    private String typeId;

}
