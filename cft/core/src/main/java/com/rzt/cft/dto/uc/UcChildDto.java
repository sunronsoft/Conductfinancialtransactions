package com.rzt.cft.dto.uc;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class UcChildDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createAt;

    @ApiModelProperty("用户编码（用户id，用户推广码）")
    private String code;

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("头像")
    private String headPortrait;

    @ApiModelProperty("name")
    private String name;

    @ApiModelProperty("累计返利")
    private BigDecimal accumulationRebate;

    @ApiModelProperty("累加返利积分")
    private BigDecimal accumulationIntegral;

}

