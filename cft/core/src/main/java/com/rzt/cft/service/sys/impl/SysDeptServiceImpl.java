package com.rzt.cft.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.po.Tree;
import com.rzt.cft.utils.BuildTree;
import com.rzt.cft.dto.sys.SysDeptDto;
import com.rzt.cft.entity.sys.SysDept;
import com.rzt.cft.mapper.sys.SysDeptMapper;
import com.rzt.cft.service.sys.ISysDeptService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统-后台机构 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Override
    public Tree<SysDeptDto> tree() {
        List<Tree<SysDeptDto>> trees = new ArrayList<Tree<SysDeptDto>>();
        List<SysDept> sysDepts = baseMapper.selectList(null);
        for (SysDept sysDept : sysDepts) {
            Tree<SysDeptDto> tree = new Tree<SysDeptDto>();
            tree.setId(sysDept.getId().toString());
            tree.setParentId(sysDept.getParentId().toString());
            tree.setText(sysDept.getName());
            Map<String, Object> state = new HashMap<>(16);
            state.put("opened", true);
            tree.setState(state);
            trees.add(tree);
        }
        // 默认顶级菜单为０，根据数据库实际情况调整
        Tree<SysDeptDto> tree = BuildTree.build(trees);
        return tree;
    }
}
