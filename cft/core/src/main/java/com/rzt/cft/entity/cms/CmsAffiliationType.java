package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 基础-归属类型
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsAffiliationType extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 标题
     */
    private String title;

    /**
     * 简化序列号
     */
    private String simplify;

    /**
     * 归属类型
     */
    private Integer affiliationType;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;


}
