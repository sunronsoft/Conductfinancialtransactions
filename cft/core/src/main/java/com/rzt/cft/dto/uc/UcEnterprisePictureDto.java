package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 公司或门店图片信息
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcEnterprisePictureDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("数据类型（0：公司；1：门店）")
    private Integer dataType;

    @ApiModelProperty("图片类型（公司风采、营业执照、公司介绍）")
    private String type;

    @ApiModelProperty("图片url")
    private String pictureUrl;

}
