package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-分销佣金记录表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcDistributionLog extends AbstractBase {

    /**
     * 收到佣金用户id
     */
    private String userId;

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 项目订单id
     */
    private String projectOrderId;

    private String projectType;

    /**
     * 分销佣金积分
     */
    private BigDecimal integral;

    /**
     * 分销佣金金额
     */
    private BigDecimal amt;

    /**
     * 分销类型（一级还是多级）
     */
    private String distributionType;

    /**
     * 被分销人id
     */
    private String distributorId;

    /**
     * 被分销人code
     */
    private String distributorCode;

    /**
     * 被分销人昵称
     */
    private String distributorName;

    /**
     * 直销人id
     */
    private String directSellingId;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 项目名称
     */
    private String projectName;

}
