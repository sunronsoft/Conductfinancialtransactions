package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsBbsPraise;

/**
 * <p>
 * 运营-论坛点赞、踩记录 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface CmsBbsPraiseMapper extends BaseMapper<CmsBbsPraise> {

}
