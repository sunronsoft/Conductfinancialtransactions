package com.rzt.cft.service.uc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.uc.UcDistributionLog;
import com.rzt.cft.mapper.uc.UcDistributionLogMapper;
import com.rzt.cft.service.uc.IUcDistributionLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-分销佣金记录表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-22
 */
@Service
public class UcDistributionLogServiceImpl extends ServiceImpl<UcDistributionLogMapper, UcDistributionLog> implements IUcDistributionLogService {

}
