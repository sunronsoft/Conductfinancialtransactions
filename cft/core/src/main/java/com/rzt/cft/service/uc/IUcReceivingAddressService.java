package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcReceivingAddress;

/**
 * <p>
 * 用户-收货地址列表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IUcReceivingAddressService extends IService<UcReceivingAddress> {

}
