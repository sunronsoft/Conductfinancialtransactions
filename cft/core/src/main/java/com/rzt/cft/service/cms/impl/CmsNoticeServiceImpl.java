package com.rzt.cft.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.cms.CmsNotice;
import com.rzt.cft.mapper.cms.CmsNoticeMapper;
import com.rzt.cft.service.cms.ICmsNoticeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配置表 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class CmsNoticeServiceImpl extends ServiceImpl<CmsNoticeMapper, CmsNotice> implements ICmsNoticeService {

}
