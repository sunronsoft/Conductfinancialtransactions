package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcAsset;
import com.rzt.cft.entity.uc.UcUsable;
import com.rzt.cft.param.uc.InsertIntegralParam;
import com.rzt.cft.param.uc.InsertUsableParam;

/**
 * <p>
 * 用户-资产信息 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
public interface IUcAssetService extends IService<UcAsset> {

    Boolean insertUsable(InsertUsableParam param);

    Boolean insertIntegral(InsertIntegralParam param);

    UcUsable queryUsable(String userId);

}
