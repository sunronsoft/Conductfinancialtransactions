package com.rzt.cft.param.uc;

import com.rzt.cft.annotation.MpEQ;
import com.rzt.cft.annotation.MpLike;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class UcSetParam {

    @ApiModelProperty(value = "类型")
    @MpEQ
    private String type;

    @ApiModelProperty(value = "名字")
    @MpLike
    private String name;
}
