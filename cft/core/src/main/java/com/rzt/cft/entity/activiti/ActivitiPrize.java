package com.rzt.cft.entity.activiti;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 抽奖-奖品设置
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ActivitiPrize extends AbstractBase {

    /**
     * 奖品名称
     */
    private String name;

    /**
     * 级别
     */
    private String level;

    /**
     * 抽中概率
     */
    private BigDecimal probability;

    /**
     * 是否是积分
     */
    private Boolean isIntegral;

    /**
     * 数量（积分数量或者奖品数量）
     */
    private Integer num;

    /**
     * 奖品名称
     */
    private String prizeName;

    /**
     * 图片地址
     */
    private String picture;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 地址id
     */
    private String addressId;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 收货人姓名
     */
    private String receivingName;

    /**
     * 收货人手机号
     */
    private String receivingPhone;


}
