package com.rzt.cft.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.cms.CmsBbsPost;
import com.rzt.cft.mapper.cms.CmsBbsPostMapper;
import com.rzt.cft.service.cms.ICmsBbsPostService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营-论坛帖子 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Service
public class CmsBbsPostServiceImpl extends ServiceImpl<CmsBbsPostMapper, CmsBbsPost> implements ICmsBbsPostService {

}
