package com.rzt.cft.service.cms;

import com.rzt.cft.entity.cms.CmsTypeItem;
import com.rzt.cft.service.ICoreService;

/**
 * <p>
 * 基础-字典类型项 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface ICmsTypeItemService extends ICoreService<CmsTypeItem> {

}
