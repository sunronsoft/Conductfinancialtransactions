package com.rzt.cft.dto.cms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 基础-Banner
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsBannerDto {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("标题")
    @NotNull(message = "标题不能为空")
    private String title;

    @ApiModelProperty("副标题")
    private String subhead;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("显示端")
    private String portId;

    @ApiModelProperty("显示类型（位置）")
    private String showType;

    @ApiModelProperty("链接")
    private String link;

    @ApiModelProperty("图片链接")
    private String imgLink;

    @ApiModelProperty("是否启用")
    private Boolean isUse;

}
