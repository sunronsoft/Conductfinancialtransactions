package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 用户-充值表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
public class ProjectInjection {

    @ApiModelProperty("项目id")
    private String id;

//    @ApiModelProperty("项目类型")
//    private String type;

    @ApiModelProperty("金额")
    private BigDecimal amount;

    @ApiModelProperty("密码")
    private String pwd;

    @ApiModelProperty("期限（如果是智能订单需要：one_year 一年、half_year 半年）")
    private String term;

}
