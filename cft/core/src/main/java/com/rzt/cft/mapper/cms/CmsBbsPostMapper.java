package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsBbsPost;

/**
 * <p>
 * 运营-论坛帖子 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface CmsBbsPostMapper extends BaseMapper<CmsBbsPost> {

}
