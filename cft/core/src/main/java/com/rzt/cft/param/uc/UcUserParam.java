package com.rzt.cft.param.uc;

import com.rzt.cft.annotation.MpEQ;
import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class UcUserParam extends PageParam {

    @ApiModelProperty(value = "用户id")
    @MpLike
    private String code;

    @ApiModelProperty(value = "账户")
    @MpLike
    private String account;

    @ApiModelProperty(value = "昵称")
    @MpLike
    private String nickName;

    @ApiModelProperty(value = "状态")
    @MpEQ
    private String accountStatus;
}
