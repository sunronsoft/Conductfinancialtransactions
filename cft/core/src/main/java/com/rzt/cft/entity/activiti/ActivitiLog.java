package com.rzt.cft.entity.activiti;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 抽奖-抽奖记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ActivitiLog extends AbstractBase {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 奖品id
     */
    private String prizeId;

    /**
     * 抽中级别
     */
    private String level;

    /**
     * 兑奖状态
     */
    private String status;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 数量（积分数量或者奖品数量）
     */
    private Integer num;

    /**
     * 奖品名称
     */
    private String prizeName;


}
