package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 基础-地区
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsRegion extends AbstractBase {
    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 父级标识
     */
    private String parentId;

    /**
     * 标题
     */

    private String title;

    /**
     * 简化序列号
     */

    private String simplify;

    /**
     * 排序
     */

    private Integer sort;

    /**
     * 等级
     */
    private Integer level;

    /**
     * 后缀
     */
    @ApiModelProperty("后缀")
    private String suffix;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否启用
     */
    private Boolean isUse;


}
