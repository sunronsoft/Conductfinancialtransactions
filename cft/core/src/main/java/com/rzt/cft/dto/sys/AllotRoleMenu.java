package com.rzt.cft.dto.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "分配角色功能参数")
public class AllotRoleMenu {

    @ApiModelProperty(value = "角色标识")
    private String roleId;

    @ApiModelProperty(value = "菜单功能标识集合")
    private List<String> menuIdList;

}
