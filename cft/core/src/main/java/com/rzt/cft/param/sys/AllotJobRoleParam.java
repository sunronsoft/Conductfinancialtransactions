package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "分配岗位角色参数")
public class AllotJobRoleParam {

    @ApiModelProperty(value = "岗位标识")
    private Integer jobId;

    @ApiModelProperty(value = "岗位角色集合")
    private List<String> roleList;

}
