package com.rzt.cft.param.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 钟忠
 * @date 2020/2/5 11:54
 */
@Data
public class AddUcAgent {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("经纪人名字")
    @NotNull(message = "经纪人名字不能为空")
    private String name;

    @ApiModelProperty("账号")
    @NotNull(message = "账号不能为空")
    private String account;

    @ApiModelProperty("门店id")
    @NotNull(message = "门店id不能为空")
    private String storeId;

    @ApiModelProperty("手机")
    private String phone;

    @ApiModelProperty("电子邮件")
    private String email;

    @ApiModelProperty("描述")
    private String aboutUs;

    @ApiModelProperty("入职时间")
    private LocalDateTime entryTime;

    @ApiModelProperty("服务年限")
    private Integer workYear;

    @ApiModelProperty("头像")
    private String headPortrait;

    @ApiModelProperty("个人介绍视频")
    private String introduceUrl;

    @ApiModelProperty("主营小区id")
    private List<String> mainResidentialId;

}

