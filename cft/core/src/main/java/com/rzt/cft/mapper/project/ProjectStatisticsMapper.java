package com.rzt.cft.mapper.project;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.project.ProjectStatistics;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 项目-统计 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Component
public interface ProjectStatisticsMapper extends BaseMapper<ProjectStatistics> {

}
