package com.rzt.cft.dto.uc;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/2/15 10:52
 */
@Data
public class UcLogin {

    @ApiModelProperty("token")
    private String token;

    @ApiModelProperty("用户信息")
    private UcUserDto ucUserDto;
}
