package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.po.Tree;
import com.rzt.cft.dto.sys.SysDeptDto;
import com.rzt.cft.entity.sys.SysDept;

/**
 * <p>
 * 系统-后台机构 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysDeptService extends IService<SysDept> {

    Tree<SysDeptDto> tree();
}
