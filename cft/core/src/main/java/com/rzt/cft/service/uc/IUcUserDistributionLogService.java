package com.rzt.cft.service.uc;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.uc.UcUserDistributionLog;

/**
 * <p>
 * 用户-分销升级记录 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IUcUserDistributionLogService extends IService<UcUserDistributionLog> {

}
