package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.sys.SysOperationLog;

/**
 * <p>
 * 系统-操作日志 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-07
 */
public interface ISysOperationLogService extends IService<SysOperationLog> {

}
