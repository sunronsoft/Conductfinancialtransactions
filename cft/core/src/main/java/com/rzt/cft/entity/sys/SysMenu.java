package com.rzt.cft.entity.sys;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-后台菜单
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysMenu extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 标题
     */
    private String title;

    /**
     * 副标题
     */
    private String subhead;

    /**
     * 地址
     */
    private String url;

    /**
     * 图标
     */
    private String ico;

    /**
     * 权限标识
     */
    private String perms;

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    private String menuType;

    /**
     * 等级
     */
    private Integer level;

    /**
     * 父级标识
     */
    private String parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否显示
     */
    private Boolean isShow;

    /**
     * 描述
     */
    private String remark;


}
