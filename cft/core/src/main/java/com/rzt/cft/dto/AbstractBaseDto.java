package com.rzt.cft.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 钟忠
 * @date 2020/1/3 15:19
 */
@Data
public abstract class AbstractBaseDto {

    private String id;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @JsonIgnore
    private LocalDateTime updateAt;

    /**
     * 删除标识
     */
    @JsonIgnore
    private Boolean delFlag;

    /**
     * 创建人
     */
    @JsonIgnore
    private String createId;

    /**
     * 更新人
     */
    @JsonIgnore
    private String updateId;
}
