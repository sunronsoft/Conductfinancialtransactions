package com.rzt.cft.param.cms;

import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 钟忠
 * @date 2020/2/7 10:19
 */
@Data
public class CmsBannerPageParam extends PageParam {

    @ApiModelProperty(value = "显示端")
    private String portId;

    @ApiModelProperty(value = "显示位置（类型）")
    private String showType;

    @ApiModelProperty("标题")
    private String title;
}
