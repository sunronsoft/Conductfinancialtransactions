package com.rzt.cft.service.uc;

import com.rzt.cft.entity.uc.UcAccount;
import com.rzt.cft.service.ICoreService;

/**
 * <p>
 * 用户-账号表 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface IUcAccountService extends ICoreService<UcAccount> {

}
