package com.rzt.cft.entity.uc;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 普通用户表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcUser extends AbstractBase {

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 用户编号
     */
    private String code;

    /**
     * 账号
     */
    private String account;

    /**
     * 账号状态（0,启用、1禁用、2冻结、3注销、4待注销）
     */
    private Integer accountStatus;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 自我介绍
     */
    private String aboutme;

    /**
     * 头像图片
     */
    private String headPortrait;

    /**
     * 性别（1男，2女）
     */
    private Integer sex;

    /**
     * 用户类型
     */
    private String type;

    /**
     * 融云token
     */
    private String imToken;

    /**
     * 上次登录地区
     */
    private String lastRegion;

    /**
     * 出生年份
     */
    private Integer userYear;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 推荐人编码
     */
    private String parentCode;

    /**
     * 分销级别id
     */
    private String distributionId;

    /**
     * 分销级别名称
     */
    private String distributionName;

    /**
     * 推荐人id
     */
    private String parentId;


}
