package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcTransfer;

/**
 * <p>
 * 用户-转账记录 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface UcTransferMapper extends BaseMapper<UcTransfer> {

}
