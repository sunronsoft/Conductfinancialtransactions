package com.rzt.cft.param.sys;

import com.rzt.cft.constant.RegexConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author 钟忠
 * @date 2020/1/13 9:49
 */
@Data
public class PhoneLogin {

    @ApiModelProperty("手机号")
    @Pattern(regexp = RegexConstant.PHONE_REGEX, message = "手机号格式不正确")
    private String phone;

    @ApiModelProperty("短信简化序列号")
    @NotNull(message = "短信简化序列号不能为空")
    private String simplify;

    @ApiModelProperty("验证码")
    @NotNull(message = "验证码不能为空")
    private String code;
}
