package com.rzt.cft.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Objects;

/**
 * @author zz
 */
@Data
public class PageParam {

    @ApiModelProperty(value = "页码")
    private Integer curPage;

    @ApiModelProperty(value = "每页条数")
    private Integer pageSize;

    public Integer getCurPage() {
        if (Objects.isNull(curPage) || Objects.equals(0, curPage)) {
            curPage = 1;
        }
        return curPage;
    }

    public Integer getPageSize() {
        if (Objects.isNull(pageSize) || Objects.equals(0, pageSize)) {
            pageSize = 10;
        }
        return pageSize;
    }

}
