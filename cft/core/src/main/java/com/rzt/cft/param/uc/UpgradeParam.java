package com.rzt.cft.param.uc;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class UpgradeParam {

    //用户id
    private String id;

    //人数
    private int num;

    //金额
    private BigDecimal amount;

}
