package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcAsset;

/**
 * <p>
 * 用户-资产信息 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
public interface UcAssetMapper extends BaseMapper<UcAsset> {

}
