package com.rzt.cft.dto.uc;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 佣金详情
 * </p>
 *
 */
@Data
public class UcDistributionLogDto {


    private String id;

    @ApiModelProperty("该伞下总积分收益")
    private BigDecimal totalIntegral;

    @ApiModelProperty("该伞下总金额收益")
    private BigDecimal totalAmt;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createAt;

    @ApiModelProperty("项目id")
    private String projectId;

    @ApiModelProperty("项目订单id")
    private String projectOrderId;

    @ApiModelProperty("项目类型")
    private String projectType;

    @ApiModelProperty("项目名称")
    private String projectName;

    @ApiModelProperty("分销佣金积分")
    private BigDecimal integral;

    @ApiModelProperty("分销佣金金额")
    private BigDecimal amt;

    @ApiModelProperty("被分销人code")
    private String distributorCode;

    @ApiModelProperty("被分销人昵称")
    private String distributorName;

    @ApiModelProperty("直销人id")
    private String directSellingId;

    @ApiModelProperty("备注")
    private String remarks;

}
