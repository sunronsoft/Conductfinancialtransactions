package com.rzt.cft.mapper.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.cms.CmsBbsComment;

/**
 * <p>
 * 运营-论坛评论 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
public interface CmsBbsCommentMapper extends BaseMapper<CmsBbsComment> {

}
