package com.rzt.cft.entity.project;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 项目-订单表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProjectOrder extends AbstractBase {

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目类型
     */
    private String projectType;

    /**
     * 购买用户id
     */
    private String userId;

    /**
     * 用户账号
     */
    private String userAccount;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 订单状态
     */
    private String status;

    /**
     * 订单金额
     */
    private BigDecimal amount;

    /**
     * 日利率
     */
    private BigDecimal dailyInverse;

    /**
     * 项目编码
     */
    private String projectSn;

    /**
     * 注资时间
     */
    private LocalDateTime injectionTime;

    /**
     * 订单结束时间
     */
    private LocalDateTime completionTime;

    /**
     * 期限
     */
    private String term;

    /**
     * 项目发布时间
     */
    private LocalDateTime releaseTime;

}
