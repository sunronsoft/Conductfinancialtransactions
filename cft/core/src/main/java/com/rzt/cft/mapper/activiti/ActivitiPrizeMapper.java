package com.rzt.cft.mapper.activiti;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.activiti.ActivitiPrize;

/**
 * <p>
 * 抽奖-奖品设置 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface ActivitiPrizeMapper extends BaseMapper<ActivitiPrize> {

}
