package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysMenu;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * 系统-后台菜单 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Component
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<String> listUserPerms(String id);

    List<SysMenu> listMenuByUserId(String userId);
}
