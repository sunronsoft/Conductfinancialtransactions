package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysConfig;

/**
 * <p>
 * 配置表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
