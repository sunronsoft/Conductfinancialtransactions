package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营-论坛帖子
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsBbsPost extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 发帖人标识
     */
    private String userId;

    /**
     * 用户昵称(冗余）
     */
    private String userName;

    /**
     * 头像(冗余）
     */
    private String headPortrait;

    /**
     * 浏览数量
     */
    private Integer browseNum;

    /**
     * 评论数量（冗余）
     */
    private Integer commentNum;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 大类型
     */
    private String type;

    /**
     * 二级类型
     */
    private String twoLevelType;

    /**
     * 是否启用
     */
    private Boolean isUse;

    /**
     * 描述
     */
    private String remark;


}
