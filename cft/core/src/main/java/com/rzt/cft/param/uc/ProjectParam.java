package com.rzt.cft.param.uc;

import com.rzt.cft.annotation.MpEQ;
import com.rzt.cft.annotation.MpLike;
import com.rzt.cft.param.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 钟忠
 * @date 2020/1/4 9:27
 */
@Data
public class ProjectParam extends PageParam {

    @ApiModelProperty(value = "type")
    @MpEQ
    private String type;

    @ApiModelProperty(value = "项目名称")
    @MpLike
    private String name;

    @ApiModelProperty(value = "项目编号")
    @MpLike
    private String projectSn;

    @ApiModelProperty(value = "是否精选")
    @MpEQ
    private Boolean isRecommend;

    @ApiModelProperty(value = "等级筛选(传等级id)")
    private List<String> purchasableGrades;

}
