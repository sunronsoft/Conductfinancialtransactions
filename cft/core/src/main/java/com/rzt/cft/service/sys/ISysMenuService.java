package com.rzt.cft.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.po.Tree;
import com.rzt.cft.dto.sys.SysMenuDto;
import com.rzt.cft.entity.sys.SysMenu;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统-后台菜单 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
public interface ISysMenuService extends IService<SysMenu> {

    Set<String> listPerms(String userId);

    Tree<SysMenuDto> tree();

    List<SysMenu> listAll();

    List<SysMenu> listByUser(String userId);

    Tree<SysMenuDto> treeByRole(String roleId);

    Tree<SysMenuDto> treeByUser(String userId);

}
