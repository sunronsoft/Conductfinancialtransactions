package com.rzt.cft.service.activiti.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rzt.cft.entity.activiti.ActivitiLog;
import com.rzt.cft.mapper.activiti.ActivitiLogMapper;
import com.rzt.cft.service.activiti.IActivitiLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抽奖-抽奖记录 服务实现类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Service
public class ActivitiLogServiceImpl extends ServiceImpl<ActivitiLogMapper, ActivitiLog> implements IActivitiLogService {

}
