package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcWithdraw;

/**
 * <p>
 * 用户-提现表单 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface UcWithdrawMapper extends BaseMapper<UcWithdraw> {

}
