package com.rzt.cft.entity.project;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目-详情表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProjectDetail extends AbstractBase {

    /**
     * 项目名称
     */
    private String name;

    /**
     * 项目编码
     */
    private String projectSn;

    /**
     * 副标题
     */
    private String subheading;

    /**
     * 项目类型
     */
    private String type;

    /**
     * 项目总金额
     */
    private BigDecimal total;

    /**
     * 每日返利比
     */
    private BigDecimal dailyInverse;

    /**
     * 虚拟进度
     */
    private BigDecimal virtualProgress;

    /**
     * 真实进度
     */
    private BigDecimal realProgress;

    /**
     * 半年利率
     */
    private BigDecimal halfYearRate;

    /**
     * 年利率
     */
    private BigDecimal yearRate;

    /**
     * 日现金比例
     */
    private BigDecimal cashRatio;

    /**
     * 日积分比例
     */
    private BigDecimal integralRatio;

    /**
     * 可购买等级
     */
    private String purchasableGrade;

    /**
     * 起购金额
     */
    private BigDecimal purchaseAmount;

    /**
     * 返利方式
     */
    private String rebateType;

    /**
     * 产品介绍
     */
    private String introduce;

    /**
     * 是否开启虚拟进度
     */
    private Boolean isVirtual;

    /**
     * 可认购金额
     */
    private String subscriptionAmount;

    /**
     * 项目进度开关（1显示真实进度、2显示虚拟进度、3虚拟加真实）
     */
    private Integer progressSwitch;

    /**
     * 发布时间
     */
    private LocalDateTime releaseTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 项目状态
     */
    private String status;

    /**
     * 是否精选
     */
    private Boolean isRecommend;

}
