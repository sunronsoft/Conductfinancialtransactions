package com.rzt.cft.entity.cms;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营-论坛点赞、踩记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CmsBbsPraise extends AbstractBase {

    /**
     * 创建人
     */
    private String createId;

    /**
     * 更新人
     */
    private String updateId;

    /**
     * 帖子或评论标识
     */
    private String dataId;

    /**
     * 点赞（1）踩（0）
     */
    private Boolean type;

    /**
     * 发帖人（评论人）标识
     */
    private String userId;

    /**
     * 用户昵称(冗余）
     */
    private String userName;

    /**
     * 头像(冗余）
     */
    private String headPortrait;


}
