package com.rzt.cft.entity.uc;

import java.math.BigDecimal;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-分销升级记录
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcUserDistributionLog extends AbstractBase {

    /**
     * 创建人标识
     */
    private String createId;

    /**
     * 修改人标识
     */
    private String updateId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 分销类型id
     */
    private String distributionId;

    /**
     * 投了多少钱
     */
    private BigDecimal money;

    /**
     * 人数
     */
    private Integer num;

    /**
     * 是否最新
     */
    private Boolean current;


}
