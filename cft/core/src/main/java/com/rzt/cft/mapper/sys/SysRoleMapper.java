package com.rzt.cft.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.sys.SysRole;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统-后台角色 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2019-12-31
 */
@Component
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
