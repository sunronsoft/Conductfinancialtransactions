package com.rzt.cft.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.goods.GoodsDetail;

/**
 * <p>
 * 商品-详情 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IGoodsDetailService extends IService<GoodsDetail> {

}
