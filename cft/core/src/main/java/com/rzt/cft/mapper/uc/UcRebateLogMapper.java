package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcRebateLog;

/**
 * <p>
 * 用户-返利记录 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
public interface UcRebateLogMapper extends BaseMapper<UcRebateLog> {

}
