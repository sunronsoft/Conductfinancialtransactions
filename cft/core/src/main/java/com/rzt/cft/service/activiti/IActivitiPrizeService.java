package com.rzt.cft.service.activiti;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rzt.cft.entity.activiti.ActivitiPrize;

/**
 * <p>
 * 抽奖-奖品设置 服务类
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-10
 */
public interface IActivitiPrizeService extends IService<ActivitiPrize> {

}
