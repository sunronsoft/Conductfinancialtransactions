package com.rzt.cft.dto.uc;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户-退出审核表单
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcLogoutDto extends AbstractBaseDto {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户编码")
    private String userCode;

    @ApiModelProperty("用户名字")
    private String userName;

    @ApiModelProperty("项目类型")
    private String projectType;

    @ApiModelProperty("退出金额")
    private BigDecimal amount;

    @ApiModelProperty("申请时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime applyTime;

    @ApiModelProperty("审核状态（0 待审核，1已通过，2已拒绝）")
    private String status;

    @ApiModelProperty("审核内容")
    private String remarks;

    @ApiModelProperty("审核时间")
    @JsonFormat(pattern="yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime auditTime;

    @ApiModelProperty("订单编号")
    private String txnSsn;


}
