package com.rzt.cft.dto.uc;

import com.rzt.cft.dto.AbstractBaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-钱包地址
 * </p>
 *
 * @author zhongzhong
 * @since 2020-03-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcWalletDto extends AbstractBaseDto {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户昵称")
    private String userName;

    @ApiModelProperty("钱包地址名称")
    private String name;

    @ApiModelProperty("用户手机号")
    private String phone;

    @ApiModelProperty("地址")
    private String url;

    @ApiModelProperty("二维码")
    private String qrCode;


}
