package com.rzt.cft.mapper.uc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rzt.cft.entity.uc.UcAccount;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户-账号表 Mapper 接口
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Component
public interface UcAccountMapper extends BaseMapper<UcAccount> {

}
