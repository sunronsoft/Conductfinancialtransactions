package com.rzt.cft.param.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zz
 */
@Data
public class SendSmsParam {

    @ApiModelProperty(value = "手机号")
    @NotNull
    private String phone;

    @ApiModelProperty(value = "模板简化序列号")
    @NotNull
    private String simplify;
}
