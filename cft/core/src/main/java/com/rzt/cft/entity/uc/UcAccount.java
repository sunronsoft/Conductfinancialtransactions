package com.rzt.cft.entity.uc;

import com.rzt.cft.entity.AbstractBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户-账号表
 * </p>
 *
 * @author zhongzhong
 * @since 2020-01-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcAccount extends AbstractBase {

    /**
     * 创建人标识
     */
    private String createId;

    /**
     * 修改人标识
     */
    private String updateId;

    /**
     * 用户标识
     */
    private String userId;

    /**
     * 登录账号
     */
    private String account;

    /**
     * 用户状态（0,启用、1禁用、2冻结、3注销、4待注销）
     */
    private Integer status;

    /**
     * 加密
     */
    private String salt;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户类型
     */
    private String userType;


}
